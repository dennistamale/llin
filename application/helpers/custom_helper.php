<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This converts the 1000s to Ks
 * converts Millions to Ms
 *
 * */


/**
 * Groups an array by a given key. Any additional keys will be used for grouping
 * the next set of sub-arrays.
 *
 * @author Jake Zatecky
 *
 * @param array $arr The array to have grouping performed on.
 * @param mixed $key The key to group or split by.
 *
 * @return array
 */
if (!function_exists('array_group_by')) {
    function array_group_by($arr, $key)
        {
            if (!is_array($arr)) {
                trigger_error('array_group_by(): The first argument should be an array', E_USER_ERROR);
            }
            if (!is_string($key) && !is_int($key) && !is_float($key)) {
                trigger_error('array_group_by(): The key should be a string or an integer', E_USER_ERROR);
            }

            // Load the new array, splitting by the target key
            $grouped = [];
            foreach ($arr as $value) {
                $grouped[$value[$key]][] = $value;
            }

            // Recursively build a nested grouping if more parameters are supplied
            // Each grouped array value is grouped according to the next sequential key
            if (func_num_args() > 2) {
                $args = func_get_args();

                foreach ($grouped as $key => $value) {
                    $parms = array_merge([$value], array_slice($args, 2, func_num_args()));
                    $grouped[$key] = call_user_func_array('array_group_by', $parms);
                }
            }

            return $grouped;
        }

}

if (!function_exists('number_convert')) {
    function number_convert($number, $decimalPlaces = 2)
        {
            if ($number < 1000) {
                return round($number, $decimalPlaces);
            }
            elseif ($number < 1000000) {
                return round(($number / 1000), $decimalPlaces) . 'K';
            }
            elseif ($number >= 1000000) {
                return round(($number / 1000000), $decimalPlaces) . 'M';
            }
        }
}

if (!function_exists('memory_convert')) {
    function memory_convert($size)
        {
            $unit = array('kb', 'mb', 'gb', 'tb', 'pb');
            return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
        }
}


if (!function_exists('monthName')) {
    function monthName($x)
        {
            $month = Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            return $month[$x];
        }
}


if (!function_exists('monthShortName')) {
    function monthShortName($x)
        {
            $month = Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
            return $month[$x];
        }
}


if (!function_exists('remove_p')) {
    function remove_p($string)
        {


            $string = str_replace('<p>', '', $string);
            $string = str_replace('</p>', '', $string);
            $string = str_replace('<br/>', '', $string);

            return $string;
        }
}
if (!function_exists('textfom')) {
    function textfom($txt)
        {

            $ftext = htmlspecialchars($txt, ENT_QUOTES);
            return str_replace(array("\r\n", "\n"), array("<br />", "<br />"), $ftext);
        }
}

if (!function_exists('br2nl')) {
    function br2nl($string)
        {
            return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
        }
}

if (!function_exists('stringNumbers')) {

    function stringNumbers($x)
        {
            $nwords = array("zero", "one", "two", "three", "four", "five", "six", "seven",
                "eight", "nine", "ten", "eleven", "twelve", "thirteen",
                "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
                "nineteen", "twenty", 30 => "thirty", 40 => "forty",
                50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty",
                90 => "ninety");


            if (!is_numeric($x))
                $w = '#';
            else if (fmod($x, 1) != 0)
                $w = '#';
            else {
                if ($x < 0) {
                    $w = 'minus ';
                    $x = -$x;
                }
                else
                    $w = '';
                // ... now $x is a non-negative integer.

                if ($x < 21)   // 0 to 20
                    $w .= $nwords[$x];
                else if ($x < 100) {   // 21 to 99
                    $w .= $nwords[10 * floor($x / 10)];
                    $r = fmod($x, 10);
                    if ($r > 0)
                        $w .= ' ' . $nwords[$r];
                }
                else if ($x < 1000) {   // 100 to 999
                    $w .= $nwords[floor($x / 100)] . ' hundred';
                    $r = fmod($x, 100);
                    if ($r > 0) {
                        $w .= ' and ';
                        if ($r < 100) {   // 21 to 99
                            $w .= $nwords[10 * floor($r / 10)];
                            $r = fmod($r, 10);
                            if ($r > 0)
                                $w .= ' ' . $nwords[$r];
                        }
                    }
                }
                else if ($x < 1000000) {   // 1000 to 999999
                    $w .= $nwords[floor($x / 1000)] . ' thousand';
                    $r = fmod($x, 1000);
                    if ($r > 0) {
                        $w .= ' ';
                        if ($r < 100)
                            $w .= ' and ';
                        $w .= $nwords[$r];
                    }
                }
                else {    //  millions
                    $w .= stringNumbers(floor($x / 1000000)) . ' million';
                    $r = fmod($x, 1000000);
                    if ($r > 0) {
                        $w .= ' ';
                        if ($r < 100)
                            $w .= ' and ';
                        $w .= int_to_words($r);
                    }
                }
            }
            //echo $w;

            return $w;


        }
}


if (!function_exists('Networks')) {


    function Networks($country = 'UG')
        {

            $country = strtoupper($country);

            switch ($country) {
                case 'UG' :

                    $u[0]['code'] = '25677';
                    $u[0]['title'] = 'MTN_UG';

                    $u[1]['code'] = '25678';
                    $u[1]['title'] = 'MTN_UG';

                    $u[2]['code'] = '25639';
                    $u[2]['title'] = 'MTN_UG';


                    $u[3]['code'] = '25675';
                    $u[3]['title'] = 'AIRTEL_UG';

                    $u[4]['code'] = '25670';
                    $u[4]['title'] = 'AIRTEL_UG';

                    $u[4]['code'] = '25679';
                    $u[4]['title'] = 'ORANGE_UG';

                    $u[5]['code'] = '25671';
                    $u[5]['title'] = 'UTL_UG';
                    break;


                case 'RW':


                    $u[0]['code'] = '25078';
                    $u[0]['title'] = 'MTN_RW';

                    $u[1]['code'] = '25073';
                    $u[1]['title'] = 'AIRTEL_RW';

                    $u[2]['code'] = '25072';
                    $u[2]['title'] = 'TIGO_RW';

                    break;


                default:

                    $u = array();

                    break;


            }
            return $u;
        }
}


if (!function_exists('phone')) {

    function phone($phone, $country_code = 256)
        {
            if (strstr($phone, ',') or strstr($phone, '/') or strstr($phone, '-')) {
                if (strstr($phone, ',')) {
                    $phone = substr($phone, 0, strpos($phone, ','));
                }
                if (strstr($phone, '/')) {
                    $phone = substr($phone, 0, strpos($phone, '/'));
                }
                if (strstr($phone, '-')) {
                    $phone = substr($phone, 0, strpos($phone, '-'));
                }
                $phone = trim($phone);
            }
            if (substr($phone, 0, 1) == '0') {
                if (strlen($phone) == 10) {
                    $phone = $country_code . '' . substr($phone, 1);
                }
            }
            if (strlen($phone) == 9 and substr($phone, 0, 1) !== 0) {
                $phone = $country_code . '' . $phone;
            }
            if (substr($phone, 0, 1) == '+') {
                $phone = substr($phone, 1);
            }
            return $phone;
        }
}


if (!function_exists('phone2')) {
    function phone2($phone)
        {
            if (strstr($phone, '+')) {
//                776716138

                $phone = substr($phone, 1);
                $mobile = strlen($phone) == 10 ? '256' . substr($phone, 1) : $phone;
                return $mobile;
            }
            else {
                $mobile = strlen($phone) == 9 ? '256' . $phone : $phone;
                return $mobile;
            }
        }
}


if (!function_exists('trending_date')) {
    function trending_date($date)
        {
            return date('Y-m-d') == date('Y-m-d', $date) ? date('H:i:s', $date) : date('d-m-Y', $date);
        }
}
if (!function_exists('trending_date_time')) {
    function trending_date_time($date)
        {
            return date('Y-m-d') == date('Y-m-d', $date) ? date('H:i:s', $date) : date('d-m-Y H:i', $date) . 'hrs';
        }
}

if (!function_exists('code')) {
    function code($length = 10)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
}

if (!function_exists('hashValue')) {
    function hashValue($v)
        {
            return sha1(md5($v));
        }
}

if (!function_exists('readJsonFgets')) {
    function readJsonFgets()
        {
            $data = json_decode(file_get_contents('php://input'), true);
            return $data;
        }
}
if (!function_exists('readJson')) {

    function readJson($obj, $type = 'url')
        {
            if ($type == 'url') {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_URL, $obj);
                $result = curl_exec($ch);
                curl_close($ch);
            }
            else {
                $result = $obj;
            }

            return $obj = json_decode($result);

        }
}

if (!function_exists('readJsonAuth')) {

    function readJsonAuth($obj, $username,$password,$type = 'url')
        {
            if ($type == 'url') {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_URL, $obj);
                $result = curl_exec($ch);
                curl_close($ch);
            }
            else {
                $result = $obj;
            }

            return $obj = json_decode($result);

        }
}

if (!function_exists('readJsonFgetsAuth')) {

    function readJsonFgetsAuth($obj, $username,$password,$type = 'url')
        {

            $context = stream_context_create(array(
                'http' => array(
                    'header'  => "Authorization: Basic " . base64_encode("$username:$password")
                )
            ));


// Open the file using the HTTP headers set above
            $result = file_get_contents($obj, false, $context);

            return $obj = json_decode($result);

        }
}


if (!function_exists('nets_popn_agg')) {


    function nets_popn_agg($for_det)
        {



            $pop = 0;
            $hh_net = 0;
            $picked=0;
            foreach ($for_det as $dd) {

                $final_pop=$dd->person_final!=0?$dd->person_final:$dd->person_vht;
                //adding the total house holds
                $pop = $pop + $final_pop;

                $original_num=floor(($final_pop / 2));
                $modulus_num = ($final_pop % 2) == 0 ? 0 : 1;
                $nets = $original_num + $modulus_num;


                //computing the the nets
                $hh_net = $hh_net + $nets;

               isset($dd->no_picked)? $picked=$picked+$dd->no_picked:'';

            }

            //calculating of bails

            $mod_bails = ($hh_net % 40);
            $modulus_bails = $mod_bails == 0 ? 0 : 1;
            $bails = floor($hh_net / 40) + $modulus_bails;

            //extra bails
            $extra_bails = $mod_bails > 0 ? (40 - $mod_bails) : 0;


            return array(
                'population' => $pop,
                'house_hold_nets' => $hh_net,
                'bails' => $bails,
                'extra_bails' => $extra_bails,
                'picked'=>$picked

            );

        }

}


if (!function_exists('nets_agg_only')) {

    /**
     * @param null $pop
     * @return array
     */
    function nets_agg_only($pop = 0)
        {
            if (isset($pop) || $pop!=0) {

                //calculating the the number of nets

                $original_num = floor(($pop / 2));
                $modulus_num = ($pop % 2) == 0 ? 0 : 1;
                $hh_net = $original_num + $modulus_num;


                //calculating of bails

                $mod_bails = ($hh_net % 40);
                $modulus_bails = $mod_bails == 0 ? 0 : 1;
                $bails = floor($hh_net / 40) + $modulus_bails;
                //extra bails
                $extra_bails = $mod_bails > 0 ? (40 - $mod_bails) : 0;

            }else{
                $pop==0;
                $hh_net = 0;
                $bails = 0;
                $extra_bails = 0;
            }

            return array(
                'population' => $pop,
                'house_hold_nets' => $hh_net,
                'bails' => $bails,
                'extra_bails' => $extra_bails,


            );

        }

}




if (!function_exists('local_server')) {
    function local_server(){
        if($_SERVER["SERVER_ADDR"]=="127.0.0.1"||$_SERVER["SERVER_ADDR"]=="localhost"||$_SERVER["SERVER_ADDR"]=="http://localhost/"||$_SERVER["SERVER_ADDR"]=="::1"){
            return  True;
        }else{

            return  False;
        }
    }
}


