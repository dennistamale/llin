<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {

    public $username = "";
    public $password = "";
    public $page_level = "";
    public $page_level2 = "";

    public $footer_news = "";

    public $notification = array();

    public $view_data = array();

    function __construct()
        {
            parent::__construct();
            $this->page_level = $this->uri->slash_segment(1);
            $this->page_level2 = $this->uri->slash_segment(2);
            $this->footer_news = $this->footer_news(3);
        }

    public function index($page = null)
        {
            $data['view'] = 'home';
            $data['sublink'] = '';
            $data['page'] = $page;
            // $this->login();
            $this->home();

            $this->custom_library->site_visits();


        }

    function page()
        {
            $this->load->view($this->page_level . 'header');
            $this->load->view($this->page_level . 'home/slider');
            $this->load->view($this->page_level . 'home/home');
//        $this->load->view($this->page_level.'page');
            $this->load->view($this->page_level . 'footer');
        }


    function module_visit($id)
        {
            $this->load->library('user_agent');

            $values = array(
                'module' => $id,
                'article_id' => $this->uri->segment(4) / date('Y'),
                'created_on' => time(),
                'platform' => $this->agent->platform(),
                'browser' => $this->agent->browser() . '-' . $this->agent->version(),
                'agent_string' => $this->agent->agent_string(),
                'ip' => $this->input->ip_address(),
                'agent_referral' => $this->agent->is_referral() ? $this->agent->referrer() : '',
            );
            $this->db->insert('module_visit', $values);
        }


    function footer_news($limit = 4)
        {

            return $this->db->select()->from('blog')->where(array('status' => 'published'))->order_by('id', 'desc')->limit($limit)->get()->result();
        }

    function test()
        {
            $this->load->view($this->page_level . 'testing_pages/table_managed');
        }


    function adding_subscriber($email)
        {
            $user = $this->db->select()->from('newsletter_emails')->where('email', $email)->get()->row();
            if (count($user) == 0) {
                $values = array(
                    'email' => $email,
                    'created_on' => time(),
                    'email_sent' => 1
                );
                $this->db->insert('newsletter_emails', $values);
            }
        }

    //publications
    public function publications($type = null, $id = null)
        {


            $data = array(
                'title' => $title = 'publications',
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            $data['id'] = $id = isset($id) ? $id / date('Y') : null;


            switch ($type) {

                default:


                    $this->form_validation->set_rules('search', 'Search', 'required|trim');


                    $this->db->select('a.*,c.sector as sector_name,d.title as publication_title,d.title as pub_type')->from('publications a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
                    $this->db->join('publication_types d', 'd.id=a.publication_type');
                    $this->db->where(array('a.status' => 'published'));

                    if ($this->form_validation->run() == true) {

                        $this->db->like('a.title', $this->input->post('search'));

                    }
                    $data['publications'] = $this->db->order_by('a.publication_date', 'desc', 'desc')->get()->result();

                    $this->load->view($root . 'publication_public', $data);

                    break;

                case 'sector':

                    $this->db->select('a.*,c.sector as sector_name,d.title as publication_title,d.title as pub_type')->from('publications a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
                    $this->db->join('publication_types d', 'd.id=a.publication_type');
                    $this->db->where(array('a.status' => 'published'));
                    $this->db->where('a.sector', $id);
                    $data['publications'] = $this->db->order_by('a.publication_date', 'desc', 'desc')->get()->result();

                    $this->load->view($root . 'publication_public', $data);
                    break;

                case 'publication_type':

                    $this->db->select('a.*,c.sector as sector_name,d.title as publication_title,d.title as pub_type')->from('publications a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
                    $this->db->join('publication_types d', 'd.id=a.publication_type');
                    $this->db->where(array('a.status' => 'published'));
                    $this->db->where('a.publication_type', $id);
                    $data['publications'] = $this->db->order_by('a.publication_date', 'desc', 'desc')->get()->result();

                    // $this->load->view($root . $type, $data);
                    $this->load->view($root . 'publication_public', $data);
                    break;

                case 'new':

                    $this->form_validation
                        ->set_rules('full_name', 'Full Name', 'trim|required')
                        ->set_rules('email', 'Email', 'trim|required|valid_email')
                        ->set_rules('publication_type', 'Publication Type', 'trim|required')
                        ->set_rules('title', 'Title Is needed', 'trim|required')
                        ->set_rules('notes', 'Content', 'trim|required')
                        ->set_rules('sector', 'Sector', 'trim|required')
                        ->set_rules('author', 'Author', 'trim|required')
                        ->set_rules('tags', 'Key Tags', 'trim')
                        ->set_rules('sponsor', 'Sponsor', 'trim')
                        ->set_rules('publication_date', 'Publication Date', 'trim|required')
                        ->set_rules('attachment', 'attachment', 'trim');
                    //checking if the form validation is passed

                    $cname = 'authors/' . $this->input->post('author');
                    //managing of the images
                    $path = $config['upload_path'] = './uploads/' . $this->page_level2 . underscore($cname) . '/';
                    $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG|xls|xlsx|doc|docx|pdf';
                    // $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                    $config['max_size'] = '20000';
                    $config['max_width'] = '1920';
                    $config['max_height'] = '1000';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }

                    if ($this->form_validation->run() == true) {

                        $attachment = $this->upload->do_upload('attachment') == true ? $path . $this->upload->file_name : '';

                        if (strlen($this->upload->display_errors())) {

                            $data['message'] = $this->upload->display_errors();

                            $data['alert'] = 'warning';
                            $date['hide'] = '';
                            $this->load->view('alert', $data);
                        }

                        $values = array(

                            'public_full_name' => $submitter = $this->input->post('full_name'),
                            'email' => $email = $this->input->post('email'),
                            'publication_type' => $this->input->post('publication_type'),
                            'title' => $this->input->post('title'),
                            'content' => $this->input->post('notes'),
                            'sector' => $this->input->post('sector'),
                            'author' => $this->input->post('author'),
                            'key_tags' => $this->input->post('tags'),
                            'publication_date' => strtotime($this->input->post('publication_date')),
                            'sponsor' => $this->input->post('sponsor'),
                            'attachment' => $attachment,
                            //'created_by' => $this->session->userdata('id'),
                            'created_on' => time()

                        );

                        //adding emails incase they dont exist
                        // $this->adding_subscriber($email);

                        ///adding publications
                        $this->db->insert('publications', $values);

                        //Dear <submitter>, the Government Evaluation Facility has received your publication. We shall review it and notify you once it is published
                        $data['message'] = 'Publication submitted successfully <br/><b> Please wait for the confirmation message from Government Evaluation Facility</b>';
                        $message = "Dear $submitter,\n\r  The Government Evaluation Facility has received your publication. We shall review it and notify you once it is published";
                        $this->sendHTMLEmail2($email, 'GEF Publication', $message);
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);

                        $this->add_logs('Public', 'publication_add', $this->input->post('title'), ' has added ');
                        // $this->load->view($root . $title, $data);


                    }
                    else {

                        $data['error'] = $this->upload->display_errors();
                        $this->load->view($root . $type, $data);
                    }

                    // $this->load->view($root . 'countries', $data);
                    break;
                case 'view_publication':

                    $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

                    if ($this->form_validation->run() == true) {

                        $values = array(
                            'publication_id' => $id,
                            'comment' => $this->input->post('comment'),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id')

                        );
                        $this->db->insert('pub_evaluation_comments', $values);
                        $data = array(
                            'alert' => 'success',
                            'message' => 'Comment has been posted successfully'
                        );
                        $data['id'] = $id;
                        $this->load->view('alert', $data);
                    }
                    $this->db->select('a.*,c.sector as sector_name,d.title as publication_title,d.title as pub_type')->from('publications a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
                    $this->db->join('publication_types d', 'd.id=a.publication_type');
                    $data['pub'] = $pub = $this->db->where(array('a.id' => $id))->order_by('updated_on', 'desc')->get()->row();

                    $this->module_visit($pub->publication_type);
                    $this->load->view($root . $type, $data);
                    break;

                case 'approve':


                    $this->form_validation->set_rules('action', 'Action', 'trim|required');

                    if ($this->form_validation->run() == true) {


                        $values['status'] = $action = $this->input->post('action');
                        $values[$action . '_on'] = time();
                        $values[$action . '_by'] = $this->session->userdata('id');

                        $this->db->where('id', $id)->update('publications', $values);


                        $data = array(
                            'alert' => 'success',
                            'message' => 'Publication has been ' . $action
                        );
                        $this->load->view('alert', $data);
                    }
                    $data['id'] = $id;
                    $this->load->view($root . 'view_publication', $data);
                    break;


            }


            $this->load->view($this->page_level . 'footer', $data);

        }


    public function recommendations($type = null, $id = null, $action = null)
        {


            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;

            $this->load->view($this->page_level . 'header', $data);

            $data['id'] = $id = isset($id) ? $id / date('Y') : null;


            switch ($type) {

                default:
                    redirect('admin/recommendations');

                    break;


                case 'view':

                    $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

                    if ($this->form_validation->run() == true) {

                        $values = array(
                            'publication_id' => $id,
                            'comment' => $this->input->post('comment'),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id')

                        );
                        $this->db->insert('pub_evaluation_comments', $values);
                        $data = array(
                            'alert' => 'success',
                            'message' => 'Comment has been posted successfully'
                        );
                        $data['id'] = $id;
                        $this->load->view('alert', $data);
                    }

                    $this->load->view($root . $type, $data);
                    break;


                case 'view_kpi':

                    if (strlen($this->session->username) > 0) {
                        $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

                        if ($this->form_validation->run() == true) {

                            $values = array(
                                'publication_id' => $id,
                                'comment' => $this->input->post('comment'),
                                'created_on' => time(),
                                'created_by' => $this->session->userdata('id')

                            );
                            $this->db->insert('pub_evaluation_comments', $values);
                            $data = array(
                                'alert' => 'success',
                                'message' => 'Comment has been posted successfully'
                            );
                            $data['id'] = $id;
                            $this->load->view('alert', $data);
                        }

                        $this->load->view($root . $type, $data);

                    }
                    else {
                        redirect($this->page_level);
                    }
                    break;


            }

            $this->load->view($this->page_level . 'footer', $data);


        }

    function sendHTMLEmail2($to, $subject, $message)
        {


            $mailto = $to;
            // $file="thanks.htm";
            $pcount = 0;
            $gcount = 0;
            $subject = $subject;
            $b = time();
            $pstr = $message;//$this->email_template($message);
            $gstr = $message;//$this->email_template($message);
            $from = "noreply@deltaits.net";


            $headers = sprintf("From: GEF<noreply@opm.co.ug>\r\n");
            $headers .= sprintf("MIME-Version: 1.0\r\n");
            $headers .= sprintf("Content-type: text/html; charset=utf-8\r\n");

            while (list($key, $val) = each($_POST)) {
                $pstr = $pstr . "$key : $val \n ";
                ++$pcount;

            }
            while (list($key, $val) = each($_GET)) {
                $gstr = $gstr . "$key : $val \n ";
                ++$gcount;

            }
            if ($pcount > $gcount) {
                $message_body = $message;//$pstr;
                // $message_body = $pstr;
                mail($mailto, $subject, $message_body, "From:" . $from);
                //$send = @mail($to, $subject, $body, $headers);

                $this->db->insert('outbox', array('to_user' => $mailto, 'subject' => $subject, 'm_type' => 'Email', 'message' => $message_body, 'created_on' => time(),));
                //  include("$file");
                return true;
            }
            else {
                $message_body = $gstr;

                if (!mail($mailto, $subject, $message_body, "From:" . $from)) {
//                die ("Not sent");
                    return false;
                }
                else {
                    // include("$file");
                    // print $b;
                    return true;
                }
            }

        }

    function email_template($body)
        {
            $logo = 'http://citiexpress.net/Ariane/wp-content/uploads/2016/05/Coat_of_arms_of_the_Republic_of_Uganda.svg.png';
            $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
				<html style="-webkit-text-size-adjust: none;-ms-text-size-adjust: none;">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>Government Evaluation Facility | Email</title>
				<style type="text/css">
				html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
				@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
					*[class="table_width_100"] {
						width: 96% !important;
					}
					*[class="border-right_mob"] {
						border-right: 1px solid #dddddd;
					}
					*[class="mob_100"] {
						width: 100% !important;
					}
					*[class="mob_center"] {
						text-align: center !important;
						padding: 0 !important;
					}
					*[class="mob_center_bl"] {
						float: none !important;
						display: block !important;
						margin: 0px auto;
					}
					.iage_footer a {
						text-decoration: none;
						color: #929ca8;
					}
					img.mob_display_none {
						width: 0px !important;
						height: 0px !important;
						display: none !important;
					}
					img.mob_width_50 {
						width: 40% !important;
						height: auto !important;
					}
					img.mob_width_80 {
						width: 80% !important;
						height: auto !important;
					}
					img.mob_width_80_center {
						width: 80% !important;
						height: auto !important;
						margin: 0px auto;
					}
					.img_margin_bottom {
						font-size: 0;
						height: 25px;
						line-height: 25px;
					}
				}
				.table_width_100 {
					width: 680px;
				}
				</style>
				</head>

				<body style="padding: 0px; margin: 0px;">
				<div id="mailsub" class="notification" align="center">

				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">


				<!--[if gte mso 10]>
				<table width="680" border="0" cellspacing="0" cellpadding="0">
				<tr><td>
				<![endif]-->

				<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px;min-width: 300px;width: 680px;">
					<tr><td>
					<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--header -->
					<tr><td align="center" bgcolor="#ffffff">
						<!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left"><!--

								Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
									<table class="mob_center" width="180" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
										<tr><td align="left" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="180" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="left" valign="top" class="mob_center">
													<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
													<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
													<img src="' . $logo . '" alt="MixaKids" border="0" style="display: block;height:100px"></font></a>
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--><!--[if gte mso 10]>
									</td>
									<td align="right">
								<![endif]--><!--

								Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;">
									<table width="88" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
										<tr><td align="right" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="right">
													<!--social -->
													<div class="mob_center_bl" style="width: 88px;">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr><td width="30" align="center" style="line-height: 19px;">
															<a href="https://www.facebook.com/opmuganda/" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/facebook.gif" width="10" height="19" alt="Follow us on Facebook" border="0" style="display: block;"></font></a>
														</td><td width="39" align="center" style="line-height: 19px;">
															<a href="https://www.twitter.com/mixakids" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/twitter.gif" width="19" height="16" alt="Follow us on Twitter" border="0" style="display: block;"></font></a>
														</td><!--<td width="29" align="right" style="line-height: 19px;">
															<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/dribbble.gif" width="19" height="19" alt="Dribbble" border="0" style="display: block;"></font></a>
														</td>--></tr>
													</table>
													</div>
													<!--social END-->
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--></td>
							</tr>
						</table>
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--header END-->

					<!--content 1 -->
					<tr><td align="center" bgcolor="#f8f8f8">
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left">
								<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
								<font face="Arial, Helvetica, sans-serif" size="4" color="#333" style="font-size: 15px;">
									<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #333;">
										' . $body . '
									</span>
								</font>
								<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
							</td></tr>
						</table>
					</td></tr>
					<!--content 1 END-->
					<tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left">
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
						<div style="line-height: 22px;">
							<font face="Arial, Helvetica, sans-serif" size="5" color="#6b6b6b" style="font-size: 20px;">
								<span style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #6b6b6b;">
									Thank you for Subscribing with <strong>Governmnent evaluation Facility</strong>
								</span>
							</font>
						</div>
					<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
							</td></tr>
						</table>
					</td></tr>

					<!--footer -->
					<tr><td align="center" bgcolor="#ffffff">
						<!-- padding --><div style="height: 15px; line-height: 15px; font-size: 10px;">&nbsp;</div>
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left"><!--

								Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
									<table class="mob_center" width="180" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
										<tr><td align="left" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="115" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="left" valign="top" class="mob_center">
													<a href="http://www.opm.go.ug" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
													<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
													<img src="src="' . $logo . '"" alt="Government Evaluation Facility" border="0" style="display: block;width:100%" /><br>
													Support Team</font></a>
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--><!--[if gte mso 10]>
									</td>
									<td align="right">
								<![endif]--><!--

								Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 150px;">
									<table class="mob_center" width="150" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
										<tr><td align="right" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="right">
													<!--social -->
													<div class="mob_center_bl">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr><td align="center" style="line-height: 19px; padding-right: 20px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															FAQ</font></a>
														</td><td align="center" style="line-height: 19px; padding-right: 20px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															Blog</font></a>
														</td><td align="right" style="line-height: 19px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															Contact</font></a>
														</td></tr>
													</table>
													</div>
													<!--social END-->
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--></td>
							</tr>
						</table>
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--footer END-->
					<tr>
						<td>
							<p>&nbsp;</p>
							<p style="background:#e9e9e9;color:#333"><small>This email was sent automatically by <a href="http://opm.go.ug/">opm.go.ug/</a>. Please, do not reply</small></p>
						</td>
					</tr>
					<tr><td>
					<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
					</td></tr>
				</table>
				<!--[if gte mso 10]>
				</td></tr>
				</table>
				<![endif]-->

				</td></tr>
				</table>

				</div>
				</body>
		</html>';
            return $html;
        }

    //resources
    public function resources($type = null, $id = null)
        {


            $data = array(
                'title' => 'resources',
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            $data['id'] = $id = isset($id) ? $id / date('Y') : null;


            switch ($type) {

                default:
                    $this->form_validation->set_rules('search', 'Search', 'required|trim');

                    $this->db->select('a.*,d.title as resource_title,d.title as res_type')
                        ->from('resources a');

                    $this->db->join('resource_types d', 'd.id=a.resource_type');
                    $this->db->where(array('a.status' => 'published'));

                    if ($this->form_validation->run() == true) {

                        $this->db->like('a.title', $this->input->post('search'));

                    }


                    $data['resources'] = $this->db->order_by('a.id', 'desc')->get()->result();


                    $this->load->view($root . 'resources', $data);


                    break;
                case 'sector':

                    $this->db->select('a.*,c.sector as sector_name,d.title as publication_title,d.title as res_type')->from('publications a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
                    $this->db->join('publication_types d', 'd.id=a.publication_type');
                    $this->db->where(array('a.status' => 'published'));
                    $this->db->where('a.sector', $id);
                    $data['publications'] = $this->db->order_by('a.id', 'desc')->get()->result();


                    // $this->load->view($root . $type, $data);
                    $this->load->view($root . $type, $data);
                    break;
                case 'resource_type':

                    $this->db->select('a.*,d.title as resource_title,d.title as res_type')->from('resources a');
                    $this->db->join('resource_types d', 'd.id=a.resource_type');
                    $this->db->where(array('a.status' => 'published'));
                    $this->db->where('a.resource_type', $id);
                    $data['resources'] = $this->db->order_by('a.id', 'desc')->get()->result();


                    // $this->load->view($root . $type, $data);
                    $this->load->view($root . 'resources', $data);
                    break;

                case 'resource_persons':

                    $this->load->view($root . $type, $data);
                    break;

                case 'new':

                    $this->form_validation
                        ->set_rules('publication_type', 'Publication Type', 'trim|required')
                        ->set_rules('title', 'Title Is needed', 'trim|required')
                        ->set_rules('notes', 'Content', 'trim|required')
                        ->set_rules('sector', 'Sector', 'trim|required')
                        ->set_rules('author', 'Author', 'trim|required')
                        ->set_rules('tags', 'Key Tags', 'trim')
                        ->set_rules('sponsor', 'Sponsor', 'trim')
                        ->set_rules('publication_date', 'Publication Date', 'trim|required')
                        ->set_rules('attachment', 'attachment', 'trim');
                    //checking if the form validation is passed

                    $cname = 'authors/' . $this->input->post('author');
                    //managing of the images
                    $path = $config['upload_path'] = './uploads/' . $this->page_level2 . underscore($cname) . '/';
                    $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG|xls|xlsx|doc|docx|pdf';
                    // $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                    $config['max_size'] = '200';
                    $config['max_width'] = '1920';
                    $config['max_height'] = '850';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }

                    if ($this->form_validation->run() == true) {

                        $attachment = $this->upload->do_upload('attachment') == true ? $path . $this->upload->file_name : '';

                        if (strlen($this->upload->display_errors())) {

                            $data['message'] = $this->upload->display_errors();

                            $data['alert'] = 'warning';
                            $date['hide'] = '';
                            $this->load->view('alert', $data);
                        }

                        $values = array(

                            'publication_type' => $this->input->post('publication_type'),
                            'title' => $this->input->post('title'),
                            'content' => $this->input->post('notes'),
                            'sector' => $this->input->post('sector'),
                            'author' => $this->input->post('author'),
                            'key_tags' => $this->input->post('tags'),
                            'publication_date' => strtotime($this->input->post('publication_date')),
                            'sponsor' => $this->input->post('sponsor'),
                            'attachment' => $attachment,
                            'created_by' => $this->session->userdata('id'),
                            'created_on' => time()

                        );
                        $this->db->insert('publications', $values);
                        $data['message'] = 'Publication has been added successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);


                    }
                    else {

                        $data['error'] = $this->upload->display_errors();
                        $this->load->view($root . $type, $data);
                    }

                    // $this->load->view($root . 'countries', $data);
                    break;

                case 'view_resource':

                    $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

                    if ($this->form_validation->run() == true) {

                        $values = array(
                            'publication_id' => $id,
                            'comment' => $this->input->post('comment'),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id')

                        );
                        $this->db->insert('pub_evaluation_comments', $values);
                        $data = array(
                            'alert' => 'success',
                            'message' => 'Comment has been posted successfully'
                        );
                        $data['id'] = $id;
                        $this->load->view('alert', $data);
                    }

                    $this->load->view($root . $type, $data);
                    break;

                case 'approve':


                    $this->form_validation->set_rules('action', 'Action', 'trim|required');

                    if ($this->form_validation->run() == true) {


                        $values['status'] = $action = $this->input->post('action');
                        $values[$action . '_on'] = time();
                        $values[$action . '_by'] = $this->session->userdata('id');

                        $this->db->where('id', $id)->update('publications', $values);


                        $data = array(
                            'alert' => 'success',
                            'message' => 'Publication has been ' . $action
                        );
                        $this->load->view('alert', $data);
                    }
                    $data['id'] = $id;
                    $this->load->view($root . 'view_publication', $data);
                    break;


            }


            $this->load->view($this->page_level . 'footer', $data);

        }
    //this is the products
    // //publications
    public function events($type = null, $id = null)
        {


            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            $data['id'] = $id = isset($id) ? $id / date('Y') : null;


            switch ($type) {

                default:

//                    $this->load->view($root.'events',$data);
                    $this->load->view($root . 'timeline', $data);
//                    //$this->load->view($root . 'events_timeline', $data);
//                   $this->load->view($root . 'events_timeline2', $data);


                    break;

                case 'sector':

                    $this->db->select('a.*,c.sector as sector_name,d.title as publication_title')->from('publications a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
                    $this->db->join('publication_types d', 'd.id=a.publication_type');
                    $this->db->where(array('a.status' => 'published'));
                    $this->db->where('a.sector', $id);
                    $data['publications'] = $this->db->order_by('a.id', 'desc')->get()->result();


                    // $this->load->view($root . $type, $data);
                    $this->load->view($root . 'publication_public', $data);
                    break;


                case 'event_details':

                    $this->load->view($root . $type, $data);
                    break;


            }


            $this->load->view($this->page_level . 'footer', $data);

        }

    function chat($type = null, $id = null)
        {

            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            //$data['id'] = $id = isset($id) ? $id / date('Y') : null;
            $data['chatroom_id'] = $id / date('Y');
            $this->load->view($this->page_level . 'chat/chat', $data);


            $this->load->view($this->page_level . 'footer', $data);


        }

    //this is the products

    function flicker()
        {
            // $json = $this->input->raw_input_stream;
            $url = 'https://www.flickr.com/services/rest/?method=flickr.test.echo&format=json&api_key=18882305e7b4fce9c90a508aae1065b1';
            //$obj=readJsonFgets($url);
            var_dump(readJson($url));
        }

    function home($type = null)
        {

            $this->page_level = 'home/';
            $this->page_level2 = 'home/';

            $data = array(
                'title' => $title = 'home',
                'subtitle' => $type
            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);


            ///this is the query for the recent publications
            $this->db->select('a.*,c.sector as sector_name,d.title as publication_title')->from('publications a');
            $this->db->join('sectors c', 'a.sector=c.id', 'left');
            $this->db->join('publication_types d', 'd.id=a.publication_type');
            $this->db->where(array('a.status' => 'published'));
            $data['publications'] = $this->db->order_by('a.publication_date', 'desc', 'desc')->limit(4)->get()->result();

            //this is the end of the query for the recent publications

            //getting the available recent available jobs

            $this->db->select('a.*,c.sector as sector_name')->from('jobs a');
            $this->db->join('sectors c', 'a.sector=c.id', 'left');
            $this->db->where(array('a.status' => 'published'));
            $data['jobs'] = $this->db->order_by('a.id', 'desc')->limit(10)->get()->result();

            //end of the list of available jobs

            //query for getting the events

            $data['events'] = $this->db->select('a.*,b.title as district')->from('events a')->join('state b', 'b.state=a.location')->where(array('a.status' => 'published', 'a.event_date >=' => strtotime(date('Y-m-d'))))->order_by('a.event_date', 'asc')->limit(3)->get()->result();


            //this is the end of the query for getting the currents events
            // //query for getting the events

            $data['news'] = $this->db->select()->from('blog')->where(array('blog_type' => 'blog', 'status' => 'published'))->order_by('id', 'desc')->limit(3)->get()->result();

            //this is the end of the query for getting the currents events

            //query for getting the events

            $data['jobs'] = $this->db->select()->from('jobs')->where(array('status' => 'published'))->order_by('id', 'desc')->limit(5)->get()->result();


            $this->load->view($root . 'slider', $data);
            $this->load->view($root . 'home', $data);
            //$this->load->view($root. 'events',$data);

            $this->load->view($this->page_level . 'footer');


        }

    function projects($type = null)
        {

            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            $this->load->view($root . $title);

            $this->load->view($this->page_level . 'footer');


        }

    function services($type = null)
        {

            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            $this->load->view($root . $title);

            $this->load->view($this->page_level . 'footer');


        }

    function about($type = null)
        {

            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            $this->load->view($root . $title);

            $this->load->view($this->page_level . 'footer');


        }


    function flickr()
        {
            $this->load->view('home/home/flickr/flickr_api');
        }

    function contact($type = null)
        {

            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type
            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);


            $this->form_validation->set_rules('name', 'Full Name', 'required|trim]');
            $this->form_validation->set_rules('email', 'Email', 'valid_email|trim]');
            $this->form_validation->set_rules('phone', 'Telephone Number', 'required|trim|max_length[12]|min_length[10]');
            $this->form_validation->set_rules('message', 'Your Message', 'required|trim');

            $this->load->view($this->page_level . 'recaptchalib.php');

            if ($this->form_validation->run() == false) {
                $this->load->view($root . $title);
            }
            else {

                $privatekey = "6LfvPgYTAAAAAMOJEeYcDPbI2UjCv4wor5SS9DpJ";
                $resp = recaptcha_check_answer($privatekey,
                    $_SERVER["REMOTE_ADDR"],
                    $this->input->post("recaptcha_challenge_field"),
                    $this->input->post("recaptcha_response_field"));

                if (!$resp->is_valid) {
                    // What happens when the CAPTCHA was entered incorrectly
                    $data['message'] = "The reCAPTCHA wasn't entered correctly. Go back and try it again."; //."(reCAPTCHA said: " . $resp->error . ")";
                    $data['alert'] = 'danger';
                    $this->load->view('alert', $data);
                    $this->load->view($root . $title);
                }
                else {
                    $to = 'tamaledns@gmail.com';
                    $subject = 'Government Evaluation Facility- Contact Message';
                    $message = '<p>Telphone No : ' . $this->input->post('phone') . "</p>";
                    $message .= '<p>Email : ' . $this->input->post('email') . "</p>";
                    $message .= '<p>&nbsp;</p>';
                    $message .= '<p>Message : ' . $this->input->post('message') . '</p>';
                    if ($this->sendHTMLEmail($to, $subject, $message)) {
                        $data['alert'] = 'success';
                        $data['message'] = '<strong>Well done!</strong> The Message has been sent Successfully.';
                        $this->load->view('alert', $data);
                    }
                    else {
                        $data['alert'] = 'danger';
                        $data['message'] = '<strong>Sorry!</strong> Sorry the Message has not been Sent Try Again.';

                        $this->load->view('alert', $data);
                        $this->load->view($root . $title);
                    }
                }

            }


            $this->load->view($this->page_level . 'footer', $data);


        }


    function sendHTMLEmail($to, $subject, $message)
        {
            $headers = sprintf("From: Government Evaluation Facility<noreply@opm.go.ug>\r\n");
            $headers .= sprintf("MIME-Version: 1.0\r\n");
            $headers .= "Reply-To: " . $to . "\r\n";
            $headers .= "Bcc: tamaledns@gmail.com\r\n";
            $headers .= sprintf("Content-type: text/html; charset=utf-8\r\n");
            $send = @mail($to, $subject, $message, $headers);
            if ($send) {
                return true;
            }
            else {
                return false;
            }
        }

    public function jobs($type = null, $id = null)
        {


            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            $data['id'] = $id = isset($id) ? $id / date('Y') : null;


            switch ($type) {

                default:


                    $this->db->select('a.*,c.sector as sector_name,e.country as ctry')->from('jobs a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
                    $this->db->join('country e', 'a.country=e.a2_iso');
                    $this->db->where(array('a.status' => 'published', 'expiry_date >=' => time()));


                    $this->form_validation->set_rules('search', 'Search', 'trim')->set_rules('country', 'country', 'trim');

                    if ($this->form_validation->run() == true) {
                        strlen($this->input->post('search')) > 0 ? $this->db->like('a.title', $this->input->post('search'))->or_like('a.company', $this->input->post('search')) : '';
                        strlen($this->input->post('country')) > 0 ? $this->db->where('a.country', $this->input->post('country')) : '';
                    }


                    $data['jobs'] = $this->db->order_by('a.id', 'desc')->get()->result();

                    $this->load->view($root . 'jobs', $data);


                    break;
                case 'sector':

                    $this->db->select('a.*,c.sector as sector_name,e.country as ctry')->from('jobs a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
//                    $this->db->join('state d','a.location=d.state','left');
                    $this->db->join('country e', 'a.country=e.a2_iso');
                    $this->db->where(array('a.status' => 'published'));
                    $this->db->where('a.sector', $id);

                    $data['jobs'] = $this->db->order_by('a.id', 'desc')->get()->result();


                    // $this->load->view($root . $type, $data);
                    $this->load->view($root . 'jobs', $data);
                    break;


                case 'view_job':

                    $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

                    if ($this->form_validation->run() == true) {

                        $values = array(
                            'publication_id' => $id,
                            'comment' => $this->input->post('comment'),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id')

                        );
                        $this->db->insert('pub_evaluation_comments', $values);
                        $data = array(
                            'alert' => 'success',
                            'message' => 'Comment has been posted successfully'
                        );
                        $data['id'] = $id;
                        $this->load->view('alert', $data);
                    }

                    $this->load->view($root . $type, $data);
                    break;


            }


            $this->load->view($this->page_level . 'footer', $data);

        }

    public function tenders($type = null, $id = null)
        {


            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            $data['id'] = $id = isset($id) ? $id / date('Y') : null;


            switch ($type) {

                default:


                    $this->db->select('a.*,c.sector as sector_name,e.country as ctry')->from('procurement a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
                    $this->db->join('country e', 'a.country=e.a2_iso');
                    $this->db->where(array('a.status' => 'published', 'expiry_date >=' => time()));


                    $this->form_validation->set_rules('search', 'Search', 'trim')->set_rules('country', 'country', 'trim');

                    if ($this->form_validation->run() == true) {
                        strlen($this->input->post('search')) > 0 ? $this->db->like('a.title', $this->input->post('search')) : '';
                        strlen($this->input->post('country')) > 0 ? $this->db->where('a.country', $this->input->post('country')) : '';
                    }


                    $data['jobs'] = $this->db->order_by('a.id', 'desc')->get()->result();

                    $this->load->view($root . $title, $data);


                    break;
                case 'sector':

                    $this->db->select('a.*,c.sector as sector_name,e.country as ctry')->from('procurement a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
//                    $this->db->join('state d','a.location=d.state','left');
                    $this->db->join('country e', 'a.country=e.a2_iso');
                    $this->db->where(array('a.status' => 'published'));
                    $this->db->where('a.sector', $id);

                    $data['jobs'] = $this->db->order_by('a.id', 'desc')->get()->result();

                    // $this->load->view($root . $type, $data);
                    $this->load->view($root . $title, $data);
                    break;


                case 'view_tender':

                    $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

                    if ($this->form_validation->run() == true) {

                        $values = array(
                            'publication_id' => $id,
                            'comment' => $this->input->post('comment'),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id')

                        );
                        $this->db->insert('pub_evaluation_comments', $values);
                        $data = array(
                            'alert' => 'success',
                            'message' => 'Comment has been posted successfully'
                        );
                        $data['id'] = $id;
                        $this->load->view('alert', $data);
                    }

                    $this->load->view($root . $type, $data);
                    break;


            }


            $this->load->view($this->page_level . 'footer', $data);

        }

    public function announcements($type = null, $id = null)
        {


            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            $data['id'] = $id = isset($id) ? $id / date('Y') : null;


            switch ($type) {

                default:


                    $this->db->select('a.*')->from('announcements a');
                    $this->db->where(array('a.status' => 'published'));

                    $this->form_validation->set_rules('search', 'Search', 'trim')->set_rules('country', 'country', 'trim');

                    if ($this->form_validation->run() == true) {
                        strlen($this->input->post('search')) > 0 ? $this->db->like('a.title', $this->input->post('search')) : '';
                        strlen($this->input->post('country')) > 0 ? $this->db->where('a.country', $this->input->post('country')) : '';
                    }

                    $data['jobs'] = $this->db->order_by('a.id', 'desc')->get()->result();

                    $this->load->view($root . $title, $data);


                    break;
                case 'sector':

                    $this->db->select('a.*,c.sector as sector_name,e.country as ctry')->from('announcements a');

                    $this->db->where(array('a.status' => 'published'));
                    $this->db->where('a.sector', $id);

                    $data['jobs'] = $this->db->order_by('a.id', 'desc')->get()->result();

                    // $this->load->view($root . $type, $data);
                    $this->load->view($root . $title, $data);
                    break;


                case 'view':

                    $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

                    if ($this->form_validation->run() == true) {

                        $values = array(
                            'publication_id' => $id,
                            'comment' => $this->input->post('comment'),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id')

                        );
                        $this->db->insert('pub_evaluation_comments', $values);
                        $data = array(
                            'alert' => 'success',
                            'message' => 'Comment has been posted successfully'
                        );
                        $data['id'] = $id;
                        $this->load->view('alert', $data);
                    }

                    $this->load->view($root . $type, $data);
                    break;


            }


            $this->load->view($this->page_level . 'footer', $data);

        }

    public function news($type = null, $id = null)
        {


            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view($this->page_level . 'header', $data);

            $data['id'] = $id = isset($id) ? $id / date('Y') : null;


            switch ($type) {

                default:

                    $this->form_validation->set_rules('search', 'Search', 'trim');

                    $this->db->select('a.*,b.first_name,b.last_name')->from('blog a');
                    $this->db->join('users b', 'b.id=a.created_by', 'left');

                    $this->db->where(array('blog_type' => 'blog', 'a.status' => 'published'));

                    if ($this->form_validation->run() == true) {
                        $this->db->like('topic', $this->input->post('search'));
                    }


                    $data['news'] = $this->db->order_by('a.id', 'desc')->get()->result();

                    $this->load->view($root . $title, $data);


                    break;
                case 'sector':

                    $this->db->select('a.*,c.sector as sector_name')->from('blog a');
                    $this->db->join('sectors c', 'a.sector=c.id', 'left');
                    $this->db->where(array('a.status' => 'published'));
                    $this->db->where('a.sector', $id);
                    $data['jobs'] = $this->db->order_by('a.id', 'desc')->get()->result();


                    // $this->load->view($root . $type, $data);
                    $this->load->view($root . $title, $data);
                    break;

                case 'new':

                    $this->form_validation
                        ->set_rules('publication_type', 'Publication Type', 'trim|required')
                        ->set_rules('title', 'Title Is needed', 'trim|required')
                        ->set_rules('notes', 'Content', 'trim|required')
                        ->set_rules('sector', 'Sector', 'trim|required')
                        ->set_rules('author', 'Author', 'trim|required')
                        ->set_rules('tags', 'Key Tags', 'trim')
                        ->set_rules('sponsor', 'Sponsor', 'trim')
                        ->set_rules('publication_date', 'Publication Date', 'trim|required')
                        ->set_rules('attachment', 'attachment', 'trim');
                    //checking if the form validation is passed

                    $cname = 'authors/' . $this->input->post('author');
                    //managing of the images
                    $path = $config['upload_path'] = './uploads/' . $this->page_level2 . underscore($cname) . '/';
                    $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG|xls|xlsx|doc|docx|pdf';
                    // $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                    $config['max_size'] = '200';
                    $config['max_width'] = '1920';
                    $config['max_height'] = '850';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }

                    if ($this->form_validation->run() == true) {

                        $attachment = $this->upload->do_upload('attachment') == true ? $path . $this->upload->file_name : '';

                        if (strlen($this->upload->display_errors())) {

                            $data['message'] = $this->upload->display_errors();

                            $data['alert'] = 'warning';
                            $date['hide'] = '';
                            $this->load->view('alert', $data);
                        }

                        $values = array(

                            'publication_type' => $this->input->post('publication_type'),
                            'title' => $this->input->post('title'),
                            'content' => $this->input->post('notes'),
                            'sector' => $this->input->post('sector'),
                            'author' => $this->input->post('author'),
                            'key_tags' => $this->input->post('tags'),
                            'publication_date' => strtotime($this->input->post('publication_date')),
                            'sponsor' => $this->input->post('sponsor'),
                            'attachment' => $attachment,
                            'created_by' => $this->session->userdata('id'),
                            'created_on' => time()

                        );
                        $this->db->insert('publications', $values);
                        $data['message'] = 'Publication has been added successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);


                    }
                    else {

                        $data['error'] = $this->upload->display_errors();
                        $this->load->view($root . $type, $data);
                    }

                    // $this->load->view($root . 'countries', $data);
                    break;

                case 'view_news':

                    $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

                    if ($this->form_validation->run() == true) {

                        $values = array(
                            'publication_id' => $id,
                            'comment' => $this->input->post('comment'),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id')

                        );
                        $this->db->insert('pub_evaluation_comments', $values);
                        $data = array(
                            'alert' => 'success',
                            'message' => 'Comment has been posted successfully'
                        );
                        $data['id'] = $id;
                        $this->load->view('alert', $data);
                    }

                    $this->load->view($root . $type, $data);
                    break;

                case 'approve':


                    $this->form_validation->set_rules('action', 'Action', 'trim|required');

                    if ($this->form_validation->run() == true) {


                        $values['status'] = $action = $this->input->post('action');
                        $values[$action . '_on'] = time();
                        $values[$action . '_by'] = $this->session->userdata('id');

                        $this->db->where('id', $id)->update('publications', $values);


                        $data = array(
                            'alert' => 'success',
                            'message' => 'Publication has been ' . $action
                        );
                        $this->load->view('alert', $data);
                    }
                    $data['id'] = $id;
                    $this->load->view($root . 'view_publication', $data);
                    break;


            }


            $this->load->view($this->page_level . 'footer', $data);

        }

    function delete()
        {
            $this->db->where('id', 1);
            $this->db->delete('users');
        }

    public function Ticket($length)
        {
            $token = $this->GetSMSCode($length);
            while (1) {
                if ($this->db->select('id')->from('transactions')->where(array('ticket_number' => $token))->get()->num_rows() == 0) {
                    break;
                }
                else {
                    $token = $this->GetSMSCode($length);
                }

            }

            return $token;
        }


    public function gettoken()
        {
            $token = $this->GetSMSCode(6);
            while (1) {
                if ($this->db->select('id')->from('transactions')->where(array('token' => $token))->get()->num_rows() == 0) {
                    break;
                }
                else {
                    $token = $this->GetSMSCode(6);
                }

            }

            return 'RYLCT' . $token;
        }


    public function verify_sms_code()
        {
            if (strlen($this->session->userdata('id')) > 0) {
                $phno = $this->session->userdata('mobile');
                $_REQUEST['mobile'] = strlen($phno) == 10 ? '256' . substr($phno, 1) : $phno;
                $_REQUEST['code'] = $_REQUEST['verification_code'];

                if ($this->db->select('id')->from('verification_codes')->where(array('mobile' => $_REQUEST['mobile'], 'status' => 0, 'code' => $_REQUEST['code']))) {
                    //$this->db->where('mobile = "'.$_REQUEST['mobile'].'" and code = "'.$_REQUEST['code'].'"');
                    $this->db->update('verification_codes', array('status' => 1), array('mobile' => $_REQUEST['mobile'], 'code' => $_REQUEST['code']));
                    $this->db->update('users', array('username' => $_REQUEST['mobile'], 'mobile_verified' => 1, 'status' => 1), array('mobile' => $_REQUEST['mobile']));

                    $data['mobile_verified'] = 1;

                    $data['SESSION_ID'] = sha1(date('Y-m-d H:i:s') . $_REQUEST['mobile']);
                    $this->session->set_userdata($data);

                    $this->db->insert('sessions',
                        array('session_id' => $data['SESSION_ID'],
                            'created_at' => date('Y-m-d H:i:s'),
                            'mobile' => $_REQUEST['mobile']));


                    $data['status'] = 8;
                    $data['message'] = 'Your account has been verified successfully';
                }
                else {


                    $data['status'] = 9;
                    $data['message'] = 'You have used an unknown code to activate';
                }


                redirect($this->page_level . 'myaccount', 'refresh');

            }

        }


    public function search($date = null, $page = null)
        {

            $data['view'] = 'home';
            $data['sublink'] = 'search';
            $data['message'] = '';
            $this->form_validation->set_rules('search', 'Search', 'trim');
            if ($this->form_validation->run() == true) {
                $data['src'] = $this->input->post('search');
            }
            strlen($date) > 0 ? $data['src'] = $date : '';
            $data['page'] = $page;
            include('cal_pref.php');
            $this->load->library('calendar', $prefs);
            $this->load_home($data);

            //$data['page'] = $page;

        }


    public function request_code()
        {

            if (strlen($this->session->userdata('id')) > 0) {

                $verification_code = $this->GetSMSCode(5);


                $this->db->where('mobile = "' . $this->session->userdata('mobile') . '" and status = 0');
                $this->db->update('verification_codes', array('status' => 2));
                $this->db->insert('verification_codes',
                    array('mobile' => $this->session->userdata('mobile'),
                        'code' => $verification_code,
                        'created_at' => date('Y-m-d H:i:s')));

                $message = 'ePay Verification Code : ' . $verification_code;


                $values = array(
                    'created' => date('Y-m-d H:i:s'),
                    'sender' => 'ePay',
                    'receiver' => $this->session->userdata('mobile'),
                    'message' => $message,
                    'type' => 'VERIFICATION_CODE',
                    'mobile' => $this->session->userdata('mobile'),
                    'transactionid' => 0);

                $this->db->insert('outbox_messages', $values);


                $this->compticketmodel->SendSMS('ePay', $message, $this->session->userdata('mobile'));

                redirect('home', 'refresh');

            }
        }


    public function isloggedin()
        {
            if (strlen($this->session->userdata('id')) > 0) {
                return true;

            }
            else {
                return false;

            }

        }


    //checking wether the account exists
    public function account_exists($username, $password)
        {
            $status = 0;
            return $results = $this->db->select()->from('users')->where(array('username' => $username, 'password' => $this->hashValue($password)))->get()->row();

        }

    public function login()
        {
            $data['title'] = 'login';
            $data['subtitle'] = 'login';

            if (strlen($this->session->userdata('username')) > 0) {
                $user_type = $this->session->userdata('user_type');

                if ($user_type == 1) {
                    redirect('admin', 'refresh');

                }
                elseif ($user_type == 2) {
                    redirect('evaluators', 'refresh');
                }
                elseif ($user_type == 3) {
                    redirect('partners', 'refresh');
                }
                elseif ($user_type == 4) {
                    redirect('public', 'refresh');
                }
                else {
                    $this->logout();
                }

            }
            else {

                if (isset($_REQUEST['username'])) {

                    $this->form_validation->set_rules('username', 'Email', 'required|trim');
                    $this->form_validation->set_rules('password', 'Password', 'required|trim');


                    if ($this->form_validation->run() == true) {
                        $this->username = $this->input->post('username', true);
                        $this->password = $this->input->post('password', true);


                        $results = $this->account_exists($this->username, $this->password);


                        if (isset($results->username)) {
                            //this check wetha the accounnt is not disabled
                            if ($results->status == 2) {
                                //echo 'account disabled';
                                $data['message'] = '<br/>Your Account is Currently Suspended <br/> <strong>Contact the Administrator</strong>';
                                $this->load->view('home/login/login', $data);
                                //$this->load->view('locked', $data);
                            }
                            else {
                                //$amount=$this->db->select('current_amount')->from('wallet')->where('user',$results->id)->get()->row();
                                $session_data = array(
                                    'id' => $results->id,
                                    'username' => $results->username,
                                    'first_name' => $results->first_name,
                                    'last_name' => $results->last_name,
                                    'photo' => $results->photo,
                                    'email' => $results->email,
//                                'company'=>$results->company,
                                    'user_type' => $results->user_type,
                                    'sub_type' => $results->sub_type,
                                    //'amount'=>$amount->current_amount,
                                    'phone' => $results->phone,
                                    //'verified'=>$results->verified
                                    'platform' => $this->agent->platform(),
                                    'browser' => $this->agent->browser() . '-' . $this->agent->version(),
                                    'agent_string' => $this->agent->agent_string(),
                                    'agent_referal' => $this->agent->is_referral() ? $this->agent->referrer() : '',

                                );


                                $this->session->set_userdata($session_data);
                                $this->add_logs('', 'login', '', ' Logged in');
                                $this->db->where('id', $this->session->userdata('id'))->update('users', array('status' => '1'));
                                //this code redirects the users of the website
                                if ($results->user_type == 1) {
                                    redirect('admin', 'refresh');

                                }
                                elseif ($results->user_type == 2) {
                                    redirect('evaluators', 'refresh');
                                }
                                elseif ($results->user_type == 3) {
                                    redirect('partners', 'refresh');
                                }
                                elseif ($results->user_type == 4) {
                                    redirect('public', 'refresh');
                                }

                                else {
                                    $this->logout();
                                }
                            }
                            //end of the account status check


                        }
                        else {

                            //$message = 'Testing';

                            $data['message'] = 'Invalid Username or Password';
                            $this->load->view('home/login/login', $data);

                        }


                    }
                    else {

                        $data['message'] = 'All fields are Required';

                        $this->load->view('home/login/login', $data);

                    }


                }
                else {

                    $this->load->view('home/login/login', $data);
                }

            }


            //$this->load->view('login');
        }

    /**
     * @param $trans_type accountModification_accountCreation_accountDeletion_AccountChange
     * @param string $target
     * @param string $desc
     */
    function add_logs($user = null, $trans_type, $target = '', $desc = '')
        {
            $this->load->library('user_agent');
            $user = strlen($user) > 0 ? $user : $this->session->id;

            $values = array(
                'transaction_type' => $trans_type,
                'target' => character_limiter($target, 100),
                'details' => $desc,

                'created_by' => $user,
                'created_on' => time(),
                'platform' => $this->agent->platform(),
                'browser' => $this->agent->browser() . '-' . $this->agent->version(),
                'agent_string' => $this->agent->agent_string(),
                'ip' => $this->input->ip_address(),
                'agent_referral' => $this->agent->is_referral() ? $this->agent->referrer() : '',
            );
            $this->db->insert('logs', $values);
        }


    function SendSMS($sender, $destination, $message, $type = '')
        {
            $email = 'denis@timesolut.com';
            $password = 'shawnluvu';
            $url = 'http://caltonmobile.com/calton/api.php?';
            $parameters = 'username=[EMAIL]&password=[PASSWORD]&contacts=[DESTINATION]&message=[MESSAGE]&sender=[SENDERID]';
            $parameters = str_replace('[EMAIL]', $email, $parameters);
            $parameters = str_replace('[PASSWORD]', urlencode($password), $parameters);
            $parameters = str_replace('[DESTINATION]', $destination, $parameters);
            $parameters = str_replace('[MESSAGE]', urlencode($message), $parameters);
            $parameters = str_replace('[SENDERID]', urlencode($sender), $parameters);
            $post_url = $url . $parameters;
            $response = file($post_url);
            $this->db->insert('outbox_messages', array(
                'created' => time(),
                'sender' => 'Virtual Wallet',
                'receiver' => $destination,
                'message' => $message,
                'type' => $type,
                'mobile' => $sender
            ));
            return $response[0];
        }

    //function sending an email


    function test_ip()
        {
            $ip = $this->input->ip_address();

            //$ip='154.73.12.119';

            $this->load->library('Custom_library');

            $r = $this->custom_library->site_visits();

            //  print_r($r);

            echo($r->statusCode);


        }


    //this of the password change
    function password_change($id, $phone)
        {


            $user = $data['user'] = $this->db->select()->from('users')->where(array('id' => $id / date('Y'), 'mobile' => $this->phone($phone)))->get()->row();
            if (isset($user->id)) {
                $data = array('view' => 'password_change', 'sublink' => 'password_change');
                $this->form_validation->set_rules('password', 'Password', 'trim|matches[passconf]|required|xss_clean');
                $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|xss_clean');
                if ($this->form_validation->run() == false) {

                }
                else {
                    $this->db->where(array('mobile' => $phone, 'id' => $id / date('Y')))->update('users', array('password' => $this->hashValue($this->input->post('password'))));

                    $data['message'] = 'Your Password Has Been Updated Successfully. ' . anchor($this->page_level . 'login', 'Click Here to Login');
                    $data['alert'] = 'success';
                    $msg = 'Your account  Password Has been Changed ( ' . base_url() . ' )';
                    //$this->SendSMS('Virtual Wallet',$phone,$msg);
                    $this->sendemail($user->email, 'Password Reset', $msg);

                }
            }
            else {
                $data = array('view' => 'reset_password', 'sublink' => 'reset_password');
                $data['message'] = 'An error has Occurred Please Try again';
                $data['alert'] = 'warning';
            }

            $this->load_home($data);

        }

    // this is the function for the reseting the password
    function reset_password()
        {
            $data['view'] = 'reset_password';
            $data['sublink'] = 'reset_password';
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback_email_exist');
            if ($this->form_validation->run() == false) {
                //$this->load->view('account_reset');
            }
            else {
                $this->load->helper('text');
                $user = $this->db->select()->from('users')->where('email', $this->input->post('email'))->get()->row();
                $data['message'] = 'Check Your email to complete account Reset ';
                $data['alert'] = 'success';
                $email_msg = "Dear " . $user->fname . " " . $user->lname . " \n\r\n We Thank you for using Our Services Please follow the link below to reset the account \n\r\n" . site_url($this->page_level . 'password_change/' . $user->id * date('Y') . '/' . $user->mobile) . "'>Follow This Link'";
                //$this->load->view('account_reset',$data);
                $this->sendemail($user->email, 'Virtual Wallet Account Reset', $email_msg);
            }
            $this->load_home($data);

        }

    //this is the function
    function email_exist($str)
        {
            $user = $this->db->select('email')->from('users')->where('email', $str)->get()->row();
            if (isset($user->email)) {
                return true;
            }
            else {
                $this->form_validation->set_message('email_exist', 'The %s you entered is wrong Please Try Again');
                return false;
            }
        }

    //this is the function for email verification
    function email_verification()
        {
            if (strlen($this->session->userdata('username')) > 0) {
                $data['title'] = 'Email Verification';
                $data['group'] = 'profile';
                $data['desc'] = 'Send an Admin a Message for any inquiry'; // the is the page description
                $this->load->view('header', $data);
                $this->load->helper('text');
                $user = $this->Octopusmodel->select_user($this->input->post('mobile'));
                $data['msg'] = 'Please Check Your email to complete account Verificaton ' . anchor('octopus', 'Go Back') . ' ' . $this->session->userdata('email');
                $email_msg = "Dear " . $this->session->userdata('fname') . " " . $this->session->userdata('lname') . " \n\r\n We Thank you for using Our Services Please follow the link below to Verify your email account \n\r\n" . site_url('/octopus/reg_complete/' . $this->session->userdata('id'));
                $this->sendemail($this->session->userdata('email'), 'Octopus Account Verification', $email_msg);
                //this shows the message alert
                $data['alert_type'] = 'info';
                $this->load->view('admin/alert', $data);
                $this->load->view('footer', $data);
            }
            else {
                $this->login();
            }
        }

    //this is the function for the has value
    public function hashValue($v)
        {
            return sha1(md5($v));
        }
    //this checks for the email weather it exist when signing up an account
    //checking for the valid number
    //checking for the username weather it exists in the database
    public function username_check($str)
        {
            $results = $this->db->select('username')->from('users')->where('email', $str)->get()->row();

            if (isset($results->username)) {
                $this->form_validation->set_message('username_check', 'The %s already Exists');
                return false;
            }
            else {
                return true;
            }
        }

    // this is the email check for the user
    public function email_check($str)
        {
            $results = $this->db->select('email')->from('users')->where('email', $str)->get()->row();

            if (isset($results->email)) {
                $this->form_validation->set_message('email_check', 'The %s already Exists');
                return false;
            }
            else {
                return true;
            }
        }

    function phone($phone)
        {
            if (strstr($phone, '+')) {
                $phone = substr($phone, 1);
                $mobile = strlen($phone) == 10 ? '256' . substr($phone, 1) : $phone;
                return $mobile;
            }
            else {
                $mobile = strlen($phone) == 10 ? '256' . substr($phone, 1) : $phone;
                return $mobile;
            }
        }

    function sendemail($reciever, $subject, $message)
        {

            $this->load->library('email');

            $config['protocol'] = 'smtp';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['useragent'] = 'Virtual Wallet';
            $config['smtp_host'] = 'www.timesolut.com';
            $config['smtp_user'] = 'no-reply@timesolut.com';
            $config['smtp_pass'] = 'b],PLIa~1y7h';
            $this->email->initialize($config);
            $this->email->from('no-reply@virtualwallet.com', 'Virtual Wallet');
            $this->email->to($reciever);
            $this->email->subject('Virtual Wallet | ' . $subject . ' Time >> ' . strftime("%T", time()));
            $this->email->message($message);
            if ($this->email->send()) {
                return true;
            }


        }

    // this is where sign up is suppossed to take place
    function signup()
        {
            $data['title'] = 'register';
            $data['subtitle'] = 'signup';

            //$data['code']=$this->Octopusmodel->country_code();
            $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|min_length[3]xss_clean');
            //$this->form_validation->set_rules('lastname', 'Last Name','trim|required|min_length[3]|max_length[20]|xss_clean');
            $this->form_validation->set_rules('email', 'Email Address', 'callback_email_check|trim|valid_email|required');
            $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[10]|max_length[13]|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]');
            $this->form_validation->set_rules('username', 'Username', 'trim|is_unique[users.username]');
            $this->form_validation->set_rules('gender', 'Gender', 'trim|max_length[1]|required');
            $this->form_validation->set_rules('city', 'City', 'trim');
            //$this->form_validation->set_rules('dob','Date of Birth','trim|required');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|trim|required');

            if ($this->form_validation->run() == false) {
                //$this->load->view('register',$data);

            }
            else {
                $data['msg'] = 'The Registration Process is completed Successfully <br/> <p> An Email Has been Sent For Verification to ' . $this->input->post('email') . ' </p>';
                //$this->load->view('loginmsg', $data);
                $v['full_name'] = $this->input->post('fullname');
                $username = $v['username'] = $this->input->post('username');
                $email = $v['email'] = strtolower($this->input->post('email'));
                $v['city'] = $this->input->post('city');
                $v['gender'] = $this->input->post('gender');
                $v['country'] = $this->input->post('country');
                $v['password'] = $this->hashValue($password = $this->input->post('password'));
                $v['phone'] = $this->phone($this->input->post('phone'));
                //$v['date_of_birth'] = $this->input->post('dob');
                $v['created_on'] = time();

                $this->db->insert('users', $v);


                $msg = $this->input->post('fullname') . ' Your account ' . $username . ' Has been created with us, Thank You for Registering ' . site_url();
                //$this->notification($username,$msg,$this->input->post('fname').' '.$this->input->post('lname'),$this->input->post('email'));
                //$this->sendemail($email,' Account Creation',$msg);
                $this->session_assign($username, $password);
                header('Refresh: 1; url=' . base_url('index.php/home/login/'));

            }
            $this->load->view($this->page_level . 'login/register', $data);
        }

    //this is session is assigned to the user after signing up an account , it is a one time function
    function session_assign($u, $p)
        {
            $results = $this->account_exists($u, $p);
            if (isset($results->username)) {
                //$this->db->insert('wallet',array('user'=>$results->id,'start_amount'=>0,'current_amount'=>0,'created_on'=>time(),'created_by'=>$results->id));

                $session_data = array(
                    'id' => $results->id,
                    'username' => $results->username,
                    'fullname' => $results->full_name,
                    'photo' => $results->photo,
                    'email' => $results->email,
                    'user_type' => $results->user_type,
                    'sub_type' => $results->sub_type,
                    //'amount'=>$amount->current_amount,
                    'phone' => $results->phone,
                    //'verified'=>$results->verified

                );

                $signin = $this->session->set_userdata($session_data);
                $this->db->where('id', $this->session->userdata('id'))->update('users', array('status' => '1'));
                $code = $this->GetCode(10);
                $msg = "Dear " . $results->full_name . ", \n\r\n  Your account Verificacion Code is
             \n\r\n  Verification Code :  " . $code . " \n\r\n
             , Please follow the link and fill in your Code " . base_url("index.php/myportal/email_verification");
                //$this->sendemail($results->email,' Account Creation',$msg);
                $this->db->insert('account_verification', array('user_id' => $results->id, 'code' => $code, 'created_on' => time()));
                // header('Refresh: 3; url=' . base_url('index.php/myportal'));
                $signin == true ? redirect('/customer/', 'location') : $this->login();


            }
            else {
                $this->login();
            }

        }

    // This is the function getting code
    public function GetCode($length)
        {

            $codes = array();
            $chars = "01a2B3c4D5EF6G7H8I9jK";
            srand((double)microtime() * 1000000);
            $i = 0;
            $code = '';
            $serial = '';

            $i = 0;

            while ($i < $length) {
                $num = rand() % 10;
                $tmp = substr($chars, $num, 1);
                $serial = $serial . $tmp;
                $i++;
            }

            return $serial;

        }

    //sending a notification
    function notification($no, $msg, $name, $reciever = null)
        {
            //$newphrase = str_replace($healthy, $yummy, $phrase);
            $fno = '256' . substr($no, 1);
            //sending the message using the api first message is to the subscriber
            $sent = $this->SendSMS('Newspaper Management System', $fno, $msg);
            $dest = strlen($reciever) > 0 ? $reciever : $this->session->userdata('email');
            $this->sendemail($dest, '', $msg);//reciever, subject, message

            $msg2 = ucfirst($name) . " account " . $no . ' Has been Created on ' . date('d-m-Y H:i:s');
            //this is the message to the director
            $this->sendemail('info@octopus.ug', '', $msg2);
            //$sent= $this->SendSMS('Octopus',256776997711,$msg2);
            //$sent_admin=$this->SendSMS('Octopus',256703970431,$msg2);

            $msg = array(
                'dest_id' => $fno,
                'message' => $msg,
                'status' => 'user:' . $sent . ' admin1:' . $sent_admin,
                'no_of_msg' => '1',
                'type' => 'text'
            );
            $this->Octopusmodel->save_msg($msg);
        }
    // this is the funtion for the about us

    // this is the function for contact us


    public function logout()
        {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }

    //this is the error page
    public function error()
        {
            $this->load->view('404');
        }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */