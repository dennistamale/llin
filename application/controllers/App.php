<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class app extends CI_Controller {
    public $username = "";
    public $password = "";
    public $page_level = "";
    public $page_level2 = "";
    public $page_level3 = "";

    public $notification = array();
    public $view_data = array();


    function __construct()
        {
            parent::__construct();

            $this->isloggedin() == true ? '' : $this->logout();
            $this->page_level = $this->uri->slash_segment(1);
            $this->page_level2 = $this->uri->slash_segment(2);
            $this->page_level3 = $this->uri->slash_segment(3);
            // $this->load->library('cart');
//            $this->output->enable_profiler(TRUE);
        }

    public function isloggedin()
        {
            return $this->session->userdata('user_type') == 1 || $this->session->userdata('user_type') == 2 || $this->session->userdata('user_type') == 3 || $this->session->userdata('user_type') == 4 || $this->session->userdata('user_type') == 5 || $this->session->userdata('user_type') == 6 ? true : false;

        }


    public function logout()
        {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
            //	echo '<META http-equiv=refresh content=0;URL='.base_url().'index.php/login>';
        }

    public function index()
        {

            if ($this->custom_library->role_exist('dashboard', 'group')) {
                $this->dashboard();
            }
            elseif ($this->custom_library->role_exist('Form Management', 'group')) {
                $this->page_level2 = '/house_hold_reg/';
                $this->house_hold_reg();
            }
            elseif ($this->custom_library->role_exist('Search Forms', 'group')) {
                $this->page_level2 = '/house_hold_reg/';
                $this->house_hold_reg('search_form');
            }
            else {
                $this->page_level2 = '/house_hold_reg/';
                $this->house_hold_reg('search_form');
            }

        }


    // this is the function for billing and shipping setup

    public function dashboard()
        {

            $data = array(
                'title' => 'dashboard',
                'subtitle' => 'Welcome',
                'link_details' => 'Account overview'
            );
            $page_level = $this->page_level;

            // $data['dennis']=$this->dennis();

            $this->form_validation->set_rules('filter', 'Filter', 'trim');
            $days = $this->input->post('filter');

            switch ($days) {

                case 1:
                    $data['subtitle'] = 'This Month';
                    $data['fdate'] = date('Y-m-01');
                    break;

                case 2:
                    $data['subtitle'] = 'Last 30 Days';
                    $day = date('Y-m-d');
                    $date = date("Y-m-d", strtotime($day . " -1 month "));
                    $data['fdate'] = $date;
                    break;

                case 3:
                    //this is the representation for the last 60 days
                    $data['subtitle'] = 'Last 60 days';
                    $day = date('Y-m-d');
                    $date = date("Y-m-d", strtotime($day . " -2 month "));
                    $data['fdate'] = $date;
                    break;

                case 4:
                    $data['subtitle'] = 'Last 90 days';
                    //this is the representation for the last 90 days
                    $day = date('Y-m-d');
                    $date = date("Y-m-d", strtotime($day . " -3 month "));
                    $data['fdate'] = $date;

                    break;
                case 5:
                    //this is a representation for the last month
                    $data['subtitle'] = 'Last Month';
                    $day = date('Y-m');
                    $date = date("Y-m-01", strtotime($day . " last month "));
                    $data['last_day'] = date('Y-m-d', strtotime(' last day of this month', strtotime($date)));
                    $data['fdate'] = $date;
                    break;
                case 6:
                    $data['subtitle'] = 'Last Year';
                    //this is a representation for last year
                    $day = date('Y');
                    $date = date("Y-01-01", strtotime($day . " last year "));
                    $data['last_day'] = date('Y-m-d', strtotime(' last day of December', strtotime($date)));
                    $data['fdate'] = $date;

                    break;
                case 7:
                    $data['subtitle'] = 'This Week';
                    $date = strtotime('monday this week');
                    $last_day = strtotime('sunday this week');

                    $data['fdate'] = date('Y-m-d', $date);
                    $data['last_day'] = date('Y-m-d 24:59:59', $last_day);


                    break;

                default;
                case 8:
                    $data['subtitle'] = 'This Year';
                    $data['fdate'] = date('Y-01-01');
                    $data['last_day'] = date('Y-m-d 24:59:59');


                    break;

            }


            $this->load->view('static/header', $data);

            $this->custom_library->any_role() ? $this->load->view($page_level . 'dashboard/dashboard') : $this->not_authorised();
            $this->load->view($page_level . 'dashboard/dashboard_footer');
//            $this->load->view('static/footer', $data);

        }

    function not_authorised()
        {

            $data = array(
                'alert' => 'danger',
                'message' => 'Not Authorised to access this service',
                'hide' => 1
            );
            $this->load->view('alert', $data);
        }

    function house_hold_reg($type = null, $id = null)
        {
            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;

            $this->load->view('static/header', $data);

            $data = array('tree' => $this);

            if ($this->custom_library->any_role()) {

                switch ($type) {

                    default;
                    case 'list_forms';


                    //this is the export action of the report
                    if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                        $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                            if (strlen($village = $this->input->post('village')) > 0 || strlen($village = $this->input->post('parish')) > 0 || strlen($village = $this->input->post('sub_county')) > 0) {

                                $data = array(
                                    'alert' => 'success',
                                    'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                    'hide' => 1
                                );
                                $this->load->view('alert', $data);
                                redirect($controller . $type . '/' . $village);
                            }
                            else {
                                $data = array(
                                    'alert' => 'success',
                                    'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                    'hide' => 1
                                );
                                $this->load->view('alert', $data);
                                redirect($controller . $type);
                            }
                        }


                        $this->custom_library->role_exist('View form list') ? $this->load->view($root . 'list_forms', $data) : $this->load->view($root . 'search_form', $data);
                        break;

                    case 'new':

                        $form = $this->db
                            ->select('a.phone,a.form_num,b.first_name,b.last_name,,b.code,b.village,status,date_created')
                            ->from('forms a')
                            ->join('vht b', 'a.phone=b.phone')
                            ->where(array('form_num' => $f_id = $id))->get()->row();

                        if (count($form) == 0) {

                            $data['alert'] = $f_id;
                        }
                        $data['form'] = $form;


                        $this->form_validation
                            //this the details of the current form
                            ->set_rules('district', 'District', 'trim')
                            ->set_rules('district_id', 'District', 'trim')
                            ->set_rules('sub_county_id', 'Sub County', 'trim')
                            ->set_rules('parish_id', 'Parish Id', 'trim')
                            ->set_rules('village_id', 'Village ID', 'trim')
                            ->set_rules('subcounty', 'Subcounty', 'trim')
                            ->set_rules('parish', 'Parish', 'trim')
                            ->set_rules('village', 'Village', 'trim')
                            ->set_rules('vht_name', 'VHT Name', 'trim')
                            ->set_rules('vht_code', 'VHT Code', 'trim')
                            ->set_rules('lc_chairperson', 'LC Chair Person', 'trim')
                            ->set_rules('is_signed', 'Signed ?', 'trim')
                            ->set_rules('sign_date', 'sign_date', 'trim')
                            ->set_rules('collection_date', 'Collection Date', 'trim')
                            //->set_rules('status', 'Status', 'trim')
                            ///this the number of selected households
                            ->set_rules('is_signed', 'Is Signed', 'trim|required')
                            ->set_rules('sir_names[][sir_name]', 'Sir Name', 'trim')
                            ->set_rules('first_names[][first_name]', 'First Name', 'trim')
                            ->set_rules('phones[][phone]', 'Phone', 'trim')
                            ->set_rules('national_ids[][national_id]', 'National ID', 'trim')
                            ->set_rules('persons[][total_persons]', 'Total Persons', 'trim');

                        if ($this->form_validation->run() == true) {


                            $form_data = array(
                                'form_num' =>$form_num=$this->input->post('form_num'),
                                'district' => $this->input->post('district_id'),
                                'sub_county' => $this->input->post('sub_county_id'),
                                'parish' => $this->input->post('parish_id'),
                                'village' => $this->input->post('village_id'),
                                'vhtname' => $this->input->post('vht_name'),
                                'vhtphone' => $this->input->post('vht_phone'),
                                'vhtcode' => $vht_code=$this->input->post('vht_code'),
                                'lc1' => $this->input->post('lc_chairperson'),
                                'signed' => $is_signed = $this->input->post('is_signed'),
                                'lc_date' => $this->input->post('sign_date'),
                                //'status' => $this->input->post('status'),
                                //'entered_by' => $this->session->userdata('id'),
                                'created_by' => $this->session->userdata('id'),
                                'created_on' => $time = time(),
                                'form_code' => $form_code = $this->form_code(),
                                'collection_date' => strtotime($this->input->post('collection_date')),

                                'entry_times' => $entry_times = $this->input->post('entry_times'),
                                'confirm' => $entry_times == 1 ? 1 : 0

                            );

                            $this->db->insert('registration', $form_data);

                            $this->add_logs($this->session->id, 'form_registration', $this->input->post('form_num'), ' has registered a form');


                            //getting the id of the form entered

                            $values = array(
                                'form_num' => $this->input->post('form_num'),
                                'form_code' => $form_code
                            );
                            $form_id = $this->db->select('id')->from('registration')->where($values)->order_by('id', 'desc')->get()->row();


                            if (count($form_id) == 1) {

//                                ID, DataID, Head, Tel, NID, Person

                                $form_id = $form_id->id;
                                $no = 0;
                                foreach ($this->input->post('sir_names') as $reg) {

                                    $sir_names = $this->input->post('sir_names');
                                    $first_names = $this->input->post('first_names');
                                    $phones = $this->input->post('phones');
                                    $national_ids = $this->input->post('national_ids');
                                    $chalk_ids = $this->input->post('chalk_ids');
                                    $persons_vht = $this->input->post('persons_vht');
                                    $persons_sc = $this->input->post('persons_sc');
                                    $persons_final = $this->input->post('persons_final');

                                    $house_hold_details = array(

                                        'row_no' => $no,
                                        'data_id' => trim($form_id),
                                        'last_name' => trim($sir_names[$no]['sir_name']),
                                        'first_name' => trim($first_names[$no]['first_name']),
                                        'tel' => trim($phones[$no]['phone']),
                                        'national_id' => trim($national_ids[$no]['national_id']),
                                        'chalk_id' => trim($vht_code.'/'.$chalk_ids[$no]['chalk_id']),
                                        'person_vht' => trim($persons_vht[$no]['total_persons']),
                                        'person_sc' => trim($persons_sc[$no]['total_persons']),
                                        'person_final' => trim($persons_final[$no]['total_persons']),
                                        'entry_times' => $entry_times,

                                        'created_by' => $this->session->userdata('id'),
                                        'created_on' => time()
                                    );

                                    if ($sir_names[$no]['sir_name'] != '' && $phones[$no]['phone'] != '') {
                                        $this->db->insert('reg_detail', $house_hold_details);
                                    }
                                    //array_push($tt,$form_data);
                                    $no++;

                                }

                                $data['alert'] = 'success';
                                $data['message'] = 'You have successfully filled the form';
                                $this->load->view('alert', $data);

                                ///$this->session->user_type == 1 ? $this->load->view($root . 'list_forms', $data) : $this->load->view($root . 'search_form', $data);

                                $this->add_logs($this->session->id, 'form_registration', $this->input->post('form_num'), ' has filled for form ');
                                redirect("app/house_hold_reg/view/$form_num/success");
                            }
                            else {
                                $this->load->view($root . 'new', $data);
                            }


                        }
                        else {
                            $this->load->view($root . 'new', $data);
                        }


                        break;

                    case 'view':

                        $this->form_validation
                            ->set_rules('reg_id', 'Reg ID', 'trim')
                            ->set_rules('sir_names[][sir_name]', 'Sir Name', 'trim')
                            ->set_rules('first_names[][first_name]', 'First Name', 'trim')
                            ->set_rules('phones[][phone]', 'Phone', 'trim')
                            ->set_rules('national_ids[][national_id]', 'National ID', 'trim')
                            ->set_rules('chalk_ids[][chalk_id]', 'Chalk ID', 'trim')
                            ->set_rules('persons_vht[][total_persons]', 'Total Persons', 'trim')
                            ->set_rules('persons_sc[][total_persons]', 'Total Persons', 'trim')
                            ->set_rules('persons_final[][total_persons]', 'Total Persons', 'trim')
                            ->set_rules('id[]', 'id', 'trim')
                        ;


                        if ($this->form_validation->run() == true) {

                            switch ($this->input->post('submit')) {

                                case 'update':
                                case 'save_final':


                                    $confirmed = $this->db->where(array('form_num' => $this->input->post('form_num'), 'confirm' => 1, 'entry_times' => 3))->from('registration')->count_all_results();


                                    if ($confirmed == 0) {

                                        $time = time();
                                        $created_by = $this->session->id;
                                        $reg_id = $this->input->post('reg_id');

                                        $form_code = $this->form_code();
                                        $confirmed_by = $this->session->id;

                                        $sql = "INSERT INTO registration (form_num, district,sub_county,parish, village, vhtname, vhtphone, vhtcode, date, point, lc1, signed, lc_date,  confirm,created_by,created_on,form_code,confirmed_by,confirmed_on,entry_times)
 SELECT form_num, district,sub_county,parish, village, vhtname, vhtphone, vhtcode, date, point, lc1, signed, lc_date, 1, $created_by,$time,'$form_code',$confirmed_by,$time,3 FROM registration WHERE id=$reg_id;";

                                        if ($this->db->query($sql)) {


                                            //updating the current final document to normal document then exchaing it with the final
                                            $this->db->where(array('id' => $reg_id))->update('registration', array('confirm' => 0));


                                            $values = array(
                                                'form_num' => $this->input->post('form_num'),
                                                'form_code' => $form_code,
                                                'confirm' => 1,
                                                'entry_times' => 3
                                            );
                                            $form_id = $this->db->select('id')->from('registration')->where($values)->order_by('id', 'desc')->get()->row();


                                            if (count($form_id) == 1) {

                                                $form_id = $form_id->id;
                                                $no = 0;
                                                foreach ($this->input->post('sir_names') as $reg) {

                                                    $sir_names = $this->input->post('sir_names');
                                                    $first_names = $this->input->post('first_names');
                                                    $phones = $this->input->post('phones');
                                                    $national_ids = $this->input->post('national_ids');
                                                    $chalk_ids =$this->input->post('chalk_ids');
                                                    $persons_vht = $this->input->post('persons_vht');
                                                    $persons_sc = $this->input->post('persons_sc');
                                                    $persons_final = $this->input->post('persons_final');

                                                    $house_hold_details = array(

                                                        'row_no' => $no,
                                                        'data_id' => trim($form_id),
                                                        'last_name' => trim($sir_names[$no]['sir_name']),
                                                        'first_name' => trim($first_names[$no]['first_name']),
                                                        'tel' => trim($phones[$no]['phone']),
                                                        'national_id' => trim($national_ids[$no]['national_id']),
                                                        'chalk_id' => trim($chalk_ids[$no]['chalk_id']),
                                                        'person_vht' => trim($persons_vht[$no]['total_persons']),
                                                        'person_sc' => trim($persons_sc[$no]['total_persons']),
                                                        'person_final' => trim($persons_final[$no]['total_persons']),
                                                        'entry_times' => 3,
                                                        'created_by' => $this->session->userdata('id'),
                                                        'created_on' => time()
                                                    );

                                                    if ($sir_names[$no]['sir_name'] != '' && $phones[$no]['phone'] != '') {
                                                        $this->db->insert('reg_detail', $house_hold_details);
                                                    }
                                                    //array_push($tt,$form_data);
                                                    $no++;

                                                }


                                                $this->add_logs($this->session->id, 'form_registration', $this->input->post('form_num'), ' has ' . $this->input->post('submit') . ' a form');

                                                $data['alert'] = 'success';
                                                $data['message'] = 'You have successfully Saved Final Form';
                                                $data['hide'] = '1';
                                            }
                                        }
                                        else {
                                            $data['alert'] = 'danger';
                                            $data['message'] = '<b>Opps!!</b> Error Occured while saving the form';
                                            $data['hide'] = '1';
                                        }


                                    }
                                    else {
                                        $data['alert'] = 'danger';
                                        $data['message'] = '<b>Opps!!</b> Form Already confirmed';
                                        $data['hide'] = '1';
                                    }
                                    $this->load->view('alert', $data);


                                    break;

                                case 'sign':

                                    $form_num=$this->input->post('id');

                                   if(count($form_num)>0) {
                                       foreach ($form_num as $fn) {


                                           $values = array(
                                               'signed' => $this->input->post('action'),
                                               'picked_on' => time(),
                                               'updated_by' => $this->session->id
                                           );

                                           $this->db->where('id', $fn)->update('reg_detail', $values);




                                       }

                                   }else{


                                       $data['alert']='danger';
                                       $data['message']='Please check the records you want to commit';
                                       $this->load->view('alert',$data);


                                   }
                                    break;

                            }

                        }


                        $form = $this->db->select('a.phone,a.form_num,b.first_name,b.last_name,b.last_name,b.code,b.village,a.status,date_created,c.collection_date')
                            ->from('forms a')
                            ->join('vht b', 'a.phone=b.phone')
                            ->join('registration c','a.form_num=c.form_num','left')
                            ->where(array('a.form_num' => $id))->get()->row();

                        $data['form'] = $form;
                        $this->custom_library->role_exist('View form list') ? $this->load->view($root . $type, $data) : $this->not_authorised();
                        $this->add_logs($this->session->id, 'form_registration', $id, ' has viewed form ');

                        break;

                    case 'distribution_form':

                        $data['id']=$id;

                        $this->form_validation
                            ->set_rules('signed','Signed','trim|required')
                            ->set_rules('distributed_no','No Distributed','trim|required')
                            ->set_rules('sign_date','Sign Date')
                        ;

                        if($this->form_validation->run()==true){

                            $data['alert']='success';
                            $data['message']='Please check the records you want to commit';
                            $this->load->view('alert',$data);

                            $values=array(
                                'signed'=>$this->input->post('signed'),
                                'no_picked'=>$this->input->post('distributed_no'),
                                'picked'=>1,
                                'picked_on'=>strtotime($this->input->post('sign_date')),
                                'updated_on'=>time(),
                                'updated_by'=>$this->session->userdata('id')

                            );

                            if($this->db->where('id',$hh_id=$id/date('Y'))->update('reg_detail',$values)) {

                                $this->add_logs($this->session->id, 'form_registration', $this->input->post('form_num'), ' has added distribution details for household ID='.$hh_id);

                                redirect($this->page_level . $this->page_level2 . 'view/' . $this->input->post('form_num') . '/success');

                            }else{

                                $this->load->view($this->page_level . $this->page_level2 . 'distribution_form', $data);
                            }

                        }else {
                            $this->load->view($this->page_level . $this->page_level2 . 'distribution_form', $data);
                        }

                        break;

                    case 'search_form':


                        $this->form_validation
                            //this the details of the current form
                            ->set_rules('search', 'Search', 'trim|required');
                        if ($this->form_validation->run() == true) {

                            $this->db->select('a.id,a.form_num,a.phone,a.date_created,c.first_name,c.last_name,c.village,a.status,count(a.form_num) as nos')
                                ->from('forms a')
                                ->join('vht c', 'a.phone=c.phone')
                                ->join('registration b', 'a.form_num=b.form_num', 'left');

                            $this->db->where(array(
                                'a.form_num' => $this->input->post('search'),
                                'a.status' => 'released'
                            ));
                            // $this->db->where('count(a.form_num)<2');
                            $this->db->group_by('a.form_num');
                            $form = $this->db->get()->row();
                            $data['form'] = $form;

                            $this->add_logs($this->session->id, 'form_registration', $this->input->post('search'), ' has searched for form ');
                        }

                        $this->load->view($root . 'search_form', $data);
                        break;

                    case 'add_form_num':
                        $this->load->view($root . $type, $data);
                        break;

                }
            }


            $this->load->view('static/footer_table', $data);
        }

    public function form_code()
        {


            $token = code(5);

            while (1) {

                if ($this->db->select('id')->from('registration')->where(array('form_code' => $token))->get()->num_rows() == 0) {
                    break;
                }
                else {
                    $token = code(5);
                }

            }

            return strtoupper($token);
        }

    /**
     * @param null $user accountModification_accountCreation_accountDeletion_AccountChange
     * @param $trans_type
     * @param string $target
     * @param string $desc
     */

    function add_logs($user = null, $trans_type, $target = '', $desc = '')
        {
            $this->load->library('user_agent');
            $user = strlen($user) > 0 ? $user : $this->session->id;

            $values = array(
                'transaction_type' => $trans_type,
                'target' => character_limiter($target, 100),
                'details' => $desc,
                'created_by' => $user,
                'created_on' => time(),
                'platform' => $this->agent->platform(),
                'browser' => $this->agent->browser() . '-' . $this->agent->version(),
                'agent_string' => $this->agent->agent_string(),
                'ip' => $this->input->ip_address(),
                'agent_referral' => $this->agent->is_referral() ? $this->agent->referrer() : '',
            );
            $this->db->insert('logs', $values);
        }

    function variance_reports($type = null, $id = null)
        {
            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;

            $this->load->view('static/header', $data);

            $data = array('tree' => $this);

            $this->add_logs($this->session->id, 'variance_reports', $type, ' has listed ');
            switch ($type) {


                default;
                case 'list_forms';

                    $this->load->view($root . $title, $data);
                    break;

                case 'entry_variance':
                    //this is the export action of the report
                    if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                        $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                        if (strlen($village = $this->input->post('village')) > 0) {

                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller.'variance_reports/' . $type);
                        }
                        else {
                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller.'variance_reports/' . $type);
                        }
                    }


                    $this->load->view($root . $title, $data);
                    break;

                case 'collection_variance':
                    if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                        $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                        if (strlen($village = $this->input->post('village')) > 0) {

                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller.'variance_reports/' . $type);
                        }
                        else {
                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller.'variance_reports/' . $type);
                        }
                    }


                    $this->load->view($root . $title, $data);
                    break;


            }


            $this->load->view('static/footer_table', $data);
        }

    function distribution_reports($type = null, $id = null)
        {
            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;

            $this->load->view('static/header', $data);

            $data = array('tree' => $this);


            $this->add_logs($this->session->id, 'allocation_plan', humanize($type), ' has listed ');
            switch ($type) {

                default;
                case 'list_forms';
                    $this->load->view($this->page_level . 'house_hold_reg/list_forms', $data);
                    break;


                case 'village':


                    //this is the export action of the report
                    if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                        $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                        if (strlen($village = $this->input->post('village')) > 0 || strlen($village = $this->input->post('parish')) > 0 || strlen($village = $this->input->post('sub_county')) > 0) {

                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller. $type . '/' . $village);
                        }
                        else {
                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller. $type);
                        }
                    }


///this is the default view of the report
//                    $this->load->view($root . 'village_original', $data);
                    $this->load->view($root . $type, $data);

                    break;

                case 'parish_report':


                    //this is the export action of the report
                    if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                        $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                        if (strlen($village = $this->input->post('village')) > 0) {

                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller . $type);
                        }
                        else {
                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller . $type);
                        }
                    }


//                    $this->load->view($root . 'village_original', $data);
                    $this->load->view($root . $type, $data);

                    break;

                case 'sub_county_report':


                    //this is the export action of the report
                    if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                        $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                        if (strlen($village = $this->input->post('village')) > 0) {

                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller . $type);
                        }
                        else {
                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller . $type);
                        }
                    }


//                    $this->load->view($root . 'village_original', $data);
                    $this->load->view($root . $type, $data);

                    break;
                case 'district_report':


                    //this is the export action of the report
                    if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                        $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                        if (strlen($village = $this->input->post('village')) > 0) {

                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller . $type);
                        }
                        else {
                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller . $type);
                        }
                    }


//                    $this->load->view($root . 'village_original', $data);
                    $this->load->view($root . $type, $data);

                    break;

                case 'households':

                    //this is the export action of the report
                    if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                        $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                        if ($this->input->post('export') == 'export_pdf' && (strlen($village = $this->input->post('village')) > 0 || strlen($village = $this->input->post('parish')) > 0 || strlen($village = $this->input->post('sub_county')) > 0)) {

                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller . $type . '/' . $village);
                        }
                        else if ($this->input->post('export') == 'distribution_report') {


                            if (strlen($village = $this->input->post('village')) > 0 || strlen($village = $this->input->post('parish')) > 0 || strlen($village = $this->input->post('sub_county')) > 0) {
                                redirect($controller.'istribution_report/' . $village);
                            }
                            else {
                                redirect($controller.'distribution_report/');
                            }


                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully ' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);


                        }
                        elseif ($this->input->post('export') == 'export_pdf') {
                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully ' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller . $type);
                        }
                    }


                    $this->load->view($root . 'households', $data);
                    // $this->load->view($root.$type,$data);

                    break;

                case 'village_original':

                    //$this->load->view($root.'village_original',$data);
                    $this->load->view($root . 'village', $data);

                    break;

                case 'distribution_point':

                    $this->load->view($root . $type, $data);
                    break;

            }


            $this->load->view('static/footer_table', $data);
        }

    function inventory($type = null, $id = null)
        {
            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;

            $this->load->view('static/header', $data);

            $data = array('tree' => $this);


            switch ($type) {

                default;
                case 'list_inventory';

                //this is the export action of the report
                if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                    $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                        //$this->add_logs($this->session->id,'inventory',humanize($type),' has exported');
                        if (strlen($village = $this->input->post('village')) > 0) {

                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller.'list_inventory');
                        }
                        else {
                            $data = array(
                                'alert' => 'success',
                                'message' => humanize($type) . ' Has been exported Successfully' . $village,
                                'hide' => 1
                            );
                            $this->load->view('alert', $data);
                            redirect($controller.'list_inventory');
                        }
                    }


                    $this->load->view($root . 'list_inventory', $data);
                     $this->add_logs($this->session->id,'inventory',humanize($type),' has listed');
                    break;

                case 'new':

                    $this->form_validation->set_rules('id[]', 'id', 'trim')->set_rules('action', 'Request Type', 'trim|required');

                    if ($this->form_validation->run() == true) {

                        $data['ids'] = $ids = $this->input->post('id');
                        $data['request_type'] = $request_type = $this->input->post('action');


                        $data['alert'] = 'info';
                        $data['message'] = humanize(count($ids)) . ' Forms selected for <b>' . humanize($request_type) . '</b>';
                        $data['hide'] = 1;

                        $this->load->view('alert', $data);
                        //processing of the forms


                        $this->session->set_userdata(array('form_ids' => $ids));


                        $this->load->view($root . 'selected_forms', $data);

                    }
                    else {
                        $data = array(
                            'alert' => 'danger',
                            'message' => validation_errors(),
                            'hide' => 1
                        );
                        validation_errors() ? $this->load->view('alert', $data) : '';
                        $this->load->view($this->page_level . 'house_hold_reg/list_forms', $data);
                        // $this->load->view($root . 'new_request', $data);
                    }
                    break;


                case 'complete_request':


                    $this->form_validation->set_rules('form_num[]', 'id', 'trim|required')->set_rules('requester', 'Requester', 'trim|required')->set_rules('request_type', 'Request Type', 'trim|required');

                    if ($this->form_validation->run() == true) {

                        $request_code = $this->request_code();

                        $form_num = $this->input->post('form_num');

                        foreach ($form_num as $fn) {


                            $values = array(
                                'form_num' => $fn,
                                'request_code' => $request_code,
                                'requested_by' => $this->input->post('requester'),
                                'request_type' => $this->input->post('request_type'),
                                'requested_on' => time(),
                                'created_on' => time(),
                                'created_by' => $this->session->id
                            );

                            $this->db->insert('inventory', $values);

                        }


                        unset($_SESSION['form_ids']);


                        $data['alert'] = 'info';
                        $data['message'] = count($form_num) . ' Forms inserted and the request code is <b>' . $request_code . '</b>';
                        $data['hide'] = 1;

                        $this->load->view('alert', $data);

                        $this->add_logs($this->session->id, 'inventory', $this->input->post('request_type'), ' has requested for ' . $this->custom_library->get_user_full_name($this->input->post('requester')) . ' request code ' . $request_code . ' ');

                        $this->load->view($root . 'list_inventory', $data);


                    }
                    else {

                        $data['ids'] = $this->session->form_ids;
                        $this->load->view($root . 'selected_forms', $data);
                    }

                    break;


                case 'view':

                    $data = array(
                        'id' => $id
                    );
                    $this->load->view($root . 'selected_forms', $data);

                    break;


                case 'approve':


                    $values = array(
                        'status' => 'approved',
                        'approved_on' => time(),
                        'approved_by' => $this->session->id
                    );

                    if ($this->db->where('request_code', $id)->update('inventory', $values)) {


                        $this->update_form_status($id);

                        $data = array(
                            'alert' => 'success',
                            'message' => "Request $id has been Approved Successfully",
                            'hide' => 1
                        );
                        $this->load->view('alert', $data);

                        $this->add_logs($this->session->id, 'inventory', $id, ' has approved request code ');
                    }


                    $data = array(
                        'id' => $id
                    );
                    $this->load->view($root . 'selected_forms', $data);
                    break;


                case 'reject':


                    $values = array(
                        'status' => 'rejected',
                        'approved_on' => time(),
                        'approved_by' => $this->session->id
                    );

                    if ($this->db->where('request_code', $id)->update('inventory', $values)) {


                        // $this->update_form_status($id);

                        $data = array(
                            'alert' => 'success',
                            'message' => "Request $id has been Rejected Successfully",
                            'hide' => 1
                        );

                        $this->add_logs($this->session->id, 'inventory', $id, ' has rejected request code ');
                        $this->load->view('alert', $data);
                    }


                    $data = array(
                        'id' => $id
                    );
                    $this->load->view($root . 'selected_forms', $data);
                    break;

            }


            $this->load->view('static/footer_table', $data);
        }

    public function request_code()
        {


            $token = code(5);

            while (1) {

                if ($this->db->select('id')->from('inventory')->where(array('request_code' => $token))->get()->num_rows() == 0) {
                    break;
                }
                else {
                    $token = code(5);
                }

            }

            return strtoupper($token);
        }

    function update_form_status($request_id)
        {

            $request_type = $this->db->select('request_type,form_num')->from('inventory')->where('request_code', $request_id)->get()->result();

            foreach ($request_type as $rq) {


                if ($rq->request_type == 'release') {
                    $status = 'released';
                }
                elseif ($rq->request_type == 'store' || $rq->request_type == 'return') {
                    $status = 'in_store';
                }
                else {
                    $status = 'in_field';
                }


                $values = array(
                    'status' => $status,
                    'updated_by' => $this->session->id,
                    'updated_on' => time()
                );

                $this->db->where('form_num', $rq->form_num)->update('forms', $values);
            }

        }

    /** $parent is the parent of the children we want to see
     *
     * // $level is increased when we go deeper into the tree,
     *
     * //        used to display a nice indented tree
     *
     * */

    function ui_tree()
        {
            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => '',
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;

            $this->load->view('static/header', $data);

            $data = array('tree' => $this);

            $this->load->view($root . 'ui_tree', $data);

            $this->load->view('static/footer_table', $data);


        }

    // checking for the date of birth of the user

    function display_children($parent, $level)
        {

            // retrieve all children of $parent

            $result = $this->db->select('lft,name,type_id,tree_id')->from('locations')->where('tree_parent_id', $parent)->get()->result();


            $states = array(
                "country",
                "region",
                "district",
                "subcounty",
                "parish",
                "village"
            );

            $icon_type = array(
                "",
                "",
                "",
                "fa text-info fa-folder",
                "fa text-info fa-file",
                "fa text-info fa-file"
            );

            // display each child

            foreach ($result as $row) {
                // indent and display the title of this child

                $s = $states[$row->type_id - 1];

                $icon = $icon_type[$row->type_id - 1];//$row->type_id==6||$row->type_id==5?'fa text-info fa-file':'';


                $current_url = $this->uri->segment(2);
                ?>

                <ul>

                    <li data-jstree='{ "opened" : false, "icon" : "<?php echo $icon ?>" }'><?php echo anchor($this->page_level . "$current_url/$row->lft/0", $row->name . ' ' . ucfirst($s), 'target"new"'); ?>
                        <?php $this->display_children($row->lft, $level++); ?>
                        <!--                        <a href="http://www.jstree.com"> Clickanle link node </a>-->
                    </li>

                </ul>


                <?php

                // call this function again to display this

                // child's children

            }

        }

    public function newsletter()
        {

            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => 'Subscribed emails'
            );
            $root = $this->page_level . $this->page_level2;

            $this->load->view('static/header', $data);
            $this->load->view($root . $title, $data);
            $this->load->view('static/footer_table', $data);

        }

    function vht_phone($str)
        {


            $phone = phone($str);

            $output = $this->db->where(array('phone' => $phone))->from('vht')->count_all_results();


            if (($output > 0)) {
                $this->form_validation->set_message('vht_phone', 'This VHT <strong>%s</strong> is already Registered');
                return false;
            }
            else {
                return true;
            }


        }

    function vht($type = null, $id = null)
        {

            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,

            );
            $id = $data['id'] = $id / date('Y');
            $page_level = $this->page_level;
            $root = $page_level . $this->page_level2;
            $this->load->view('static/header', $data);

            switch ($type) {
                default:

                    //this is the export action of the report
                    if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                        $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                        $data = array(
                            'alert' => 'success',
                            'message' => humanize($type) . ' Has been exported Successfully',
                            'hide' => 1
                        );
                        $this->load->view('alert', $data);
                        redirect($controller.'vht');

                    }

                    $this->load->view($root . $title, $data);
                    $this->add_logs($this->session->id,'VHT Management','VHT',' has listed');
                    break;
                case 'new':
                    //this is the section for the form validation
                    $this->form_validation
                        ->set_rules('first_name', 'First Name', 'trim|required')
                        ->set_rules('last_name', 'Last Name', 'trim|required')
                        ->set_rules('code', 'Code', 'trim|required')
                        ->set_rules('village', 'Village', 'trim|required')
                        ->set_rules('phone', 'Phone', 'trim|required|is_unique[vht.phone]|callback_vht_phone|max_length[15]');

                    if ($this->form_validation->run() == true) {


                        $values = array(
                            'first_name' => $fname = $this->input->post('first_name'),
                            'last_name' => $this->input->post('last_name'),
                            'code' => $this->input->post('code'),
                            'phone' => phone($this->input->post('phone')),
                            'village' => $this->input->post('village'),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id'),

                        );
                        $this->db->insert('vht', $values);
                        $data['message'] = 'VHT User has been added successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        //this Lists the users in the system
                        $this->load->view($root . $title, $data);
                        $this->add_logs('', 'VHT Management', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has created an account for VHT user ');

                    }
                    else {

                        $this->load->view($root . $type, $data);
                    }

                    break;
                case 'edit':
                    //this is the section for the form validation
                    $this->form_validation
                        ->set_rules('first_name', 'First Name', 'trim|required')
                        ->set_rules('last_name', 'Last Name', 'trim|required')
                        ->set_rules('code', 'Code', 'trim|required')
                        ->set_rules('village', 'Village', 'trim|required')
                        ->set_rules('phone', 'Phone', 'trim|required|max_length[15]');

                    if ($this->form_validation->run() == true) {


                        $values = array(
                            'first_name' => $fname = $this->input->post('first_name'),
                            'last_name' => $this->input->post('last_name'),
                            'code' => $this->input->post('code'),
                            'phone' => phone($this->input->post('phone')),
                            'village' => $this->input->post('village'),
                            'updated_on' => time(),
                            'updated_by' => $this->session->userdata('id'),

                        );
                        $this->db->where('id', $id)->update('vht', $values);
                        $data['message'] = 'VHT User has been updated successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        //this Lists the users in the system
                        $this->load->view($root . $title, $data);
                        $this->add_logs('', 'VHT Management', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has updated an account for VHT user ');

                    }
                    else {

                        $this->load->view($root . $type, $data);

                    }

                    break;
            }

            $this->load->view('static/footer_table', $data);
        }

    function users($type = null, $id = null)
        {

            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'link_details' => 'Account overview',


            );
            $id = $data['id'] = $id / date('Y');
            $page_level = $this->page_level;
            $root = $page_level . $this->page_level2;
            $this->load->view('static/header', $data);


            switch ($type) {

                case 'new':
                    //this is the section for the form validation
                    $this->form_validation
//                        ->set_rules('sub_type', 'User Type', 'trim|required')
                        ->set_rules('first_name', 'First Name', 'trim|required')
                        ->set_rules('last_name', 'Last Name', 'trim|required')
                        ->set_rules('email', 'Email Address', 'trim|required|is_unique[users.email]')
//                    ->set_rules('zip_code', 'Zip Code', 'trim|required')
                        ->set_rules('country', 'Country', 'trim|required')
                        ->set_rules('city', 'City', 'trim')
                        ->set_rules('phone', 'Phone', 'trim|required|is_unique[users.phone]|max_length[15]')
                        ->set_rules('gender', 'Gender', 'trim|required|exact_length[1]')
                        ->set_rules('access', 'Access', 'trim')
                        ->set_rules('role', 'User Role', 'trim|required')
                        ->set_rules('dob', 'Date of Birth', 'trim|callback_dob_check');
                    // ->set_rules('referral', 'Referral', 'trim');


                    if ($this->form_validation->run() == true) {

                        $password = code(6);

                        $values = array(
                            'first_name' => $fname = $this->input->post('first_name'),
                            'last_name' => $this->input->post('last_name'),
                            'email' => $this->input->post('email'),
                            'username' => $email = $this->input->post('email'),
                            'password' => $this->hashValue($password),
                            'phone' => phone($this->input->post('phone')),
                            'city' => $this->input->post('city'),
                            'country' => $this->input->post('country'),
                            'gender' => $this->input->post('gender'),
                            'user_type' => $this->input->post('role'),
                            'dob' => strtotime($this->input->post('dob')),
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id'),
//                            'sub_type' => strlen($this->input->post('referral')) > 0 ? 2 : 1,
//                            'referral' => strlen($this->input->post('referral')) > 0 ? $this->input->post('referral') : ''

                        );
                        $this->db->insert('users', $values);
                        $data['message'] = 'User has been added successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        //this Lists the users in the system


                        $this->load->view($root . 'users', $data);
                        $email_msg = "Dear $fname ,\n \r Your account has been created. Your username is $email: and password is: $password
To login, go to URL. Remember you can change your password once logged.\r\n";
                        // $this->custom_library->sendMail($email, 'GEF Account', $email_msg);
                        $this->add_logs('', 'account_creation', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has created an account for user ');

                    }
                    else {

                        $this->load->view($root . 'new', $data);
                    }

                    break;
                case 'edit':

                    //this is the section for the form validation
                    $this->form_validation
                        ->set_rules('email', 'Email', 'valid_email|trim')
                        ->set_rules('phone', 'Phone', 'trim|max_length[15]|min_length[10]')
                        ->set_rules('first_name', 'First Name', 'trim')
                        ->set_rules('last_name', 'Last Name', 'trim')
                        ->set_rules('role', 'Role', 'trim');

                    if ($this->form_validation->run() == false) {

                        $this->load->view($root . 'user_profile', $data);
                    }
                    else {

                        $this->db->where('id', $id)
                            ->update('users', array(
                                'email' => $this->input->post('email'),
                                'phone' => $this->input->post('phone'),
                                'user_type' => $this->input->post('role'),
                                'first_name' => $fname = $this->input->post('first_name'),
                                'last_name' => $this->input->post('last_name'),


                            ));

                        $data['message'] = 'User account is updated Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        $this->load->view($root . 'user_profile', $data);

                        $email = $this->input->post('email');
                        $email_msg = "Dear $fname ,\r\n Your account has been Updated\n\r";
                        // $this->custom_library->sendMail($email, 'GEF Account', $email_msg);
                        $this->add_logs('', 'account_modification', $this->input->post('first_name') . ' ' . $this->input->post('last_name'), 'has updated account for user');

                    }
                    break;
                //this is the part for changing avatar
                case 'change_avatar':


                    // this is the function which uploads the profile image
                    $this->form_validation->set_rules('image', 'Image', 'trim')->set_rules('pa', 'Pas', 'trim');
                    //this is the company name

                    $cname = 'users_profile';
                    //managing of the images
                    $path = $config['upload_path'] = './uploads/profile/' . underscore($cname) . '/';
                    $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                    $config['max_size'] = '200';
                    $config['max_width'] = '1920';
                    $config['max_height'] = '850';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }
                    if ($this->form_validation->run() == true && $this->upload->do_upload('image') == true) {

                        $values = array(
                            'photo' => $path . $this->upload->file_name,
                            'updated_on' => time(),
                            'updated_by' => $this->session->userdata('id')
                        );
                        $this->db->where('id', $id)->update('users', $values);
                        // $this->session->set_userdata(array('photo'=>$path.$this->upload->file_name));
                        $data['message'] = 'Image has been updated Successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);

                        $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                        $email_msg = "Dear $fn->first_name ,\n \r Your account Profile Picture has been Updated";
                        //$this->custom_library->sendMail($fn->email, 'GEF Account', $email_msg);

                        $this->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has changed picture for user ');

                    }
                    else {
                        $data['error'] = $this->upload->display_errors();
                        $this->load->view($root . 'user_profile', $data);

                    }

                    break;
                //this is the part for changing the password
                case 'change_password':

                    $this->form_validation->set_rules('new_pass', 'New Password', 'required|trim|matches[rpt_pass]')->set_rules('rpt_pass', 'Repeat Password', 'required|trim');
                    if ($this->form_validation->run() == false) {

                        $this->load->view($root . 'user_profile', $data);
                    }
                    else {
                        $this->db->where('id', $id)->update('users', array('password' => $this->hashValue($password = $this->input->post('new_pass'))));
                        $data['message'] = 'Password has been changed successfully <br/>';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        $this->load->view($root . 'user_profile', $data);
                        $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                        $email_msg = "Dear $fn->first_name ,\n \r Your account password has been changed. Your new password is $password
If you didn't initiate this password request, contact the GEF Admin immediately.\r\n

";
                        // $this->custom_library->sendMail($fn->email, 'GEF Account', $email_msg);
                        $this->add_logs('', 'password_change', $fn->first_name . ' ' . $fn->last_name, '  has changed password for user ');
                    }

                    break;

                case 'documents':
                    $this->load->view($root . 'user_profile', $data);
                    break;

                //deleting user
                case 'delete':

                    $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                    $email_msg = "Dear $fn->first_name ,\n \r Your account has been Deleted";
                    // $this->custom_library->sendMail($fn->email, 'GEF Account', $email_msg);

                    $this->delete($id, 'users');
                    $data['message'] = 'Record has been Deleted successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    //this is where the default page i loaded

                    $this->load->view($root . 'users', $data);

                    $this->add_logs('', 'account_deletion', $fn->first_name . ' ' . $fn->last_name, '  has deleted user ');
                    break;

                case 'make_admin':
                    $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();
                    // $id = $data['id'] = $id / date('Y');
                    $this->db->where('id', $id)->update('users',
                        array('user_type' => '1',
                            'updated_on' => time(),
                            'updated_by' => $this->session->userdata('id')));
                    $data['message'] = 'Record has been Updated successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    //this is where the default page i loaded

                    $this->load->view($root . 'users', $data);

                    $this->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has made user admin ');
                    break;

                case 'ban':

                    $this->db->where('id', $id)->update('users',
                        array('status' => '2',
                            'updated_on' => time(),
                            'updated_by' => $this->session->userdata('id')));


                    $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                    $email_msg = "Dear $fn->first_name ,\n \r Your account has been blocked from the system";
                    //   $this->custom_library->sendMail($fn->email, 'GEF Account Creation', $email_msg);
                    $data['message'] = 'Record has been Blocked from accessing the System successfully';
                    $data['alert'] = 'warning';
                    $this->load->view('alert', $data);
                    //this is where the default page i loaded

                    $this->load->view($root . 'users', $data);

                    $this->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has blocked user');
                    break;

                case 'unblock':
                    $fn = $this->db->select('first_name,last_name,email')->from('users')->where('id', $id)->get()->row();

                    $email_msg = "Dear $fn->first_name ,\n \r Your account has been unblocked from the system";

                    // $this->custom_library->sendMail($fn->email, 'GEF Account Creation', $email_msg);

                    $this->db->where('id', $id)->update('users',
                        array('status' => '0',
                            'updated_on' => time(),
                            'updated_by' => $this->session->userdata('id')));
                    $data['message'] = 'Record has been unblocked from accessing the System successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    //this is where the default page i loaded

                    $this->load->view($root . 'users', $data);
                    $this->add_logs('', 'account_modification', $fn->first_name . ' ' . $fn->last_name, '  has unblocked user');
                    break;

                case 'filter':

                    $this->form_validation
                        ->set_rules('country', 'Country', 'trim')
                        ->set_rules('role', 'Role', 'trim');

                    $this->db->select()->from('users');
                    //this is selecting for the date
                    strlen($this->input->post('country')) > 0 ? $this->db->where(array('country' => $this->input->post('country'))) : '';
                    strlen($this->input->post('role')) > 0 ? $this->db->where(array('user_type' => $this->input->post('role'))) : '';

                    if ($this->uri->segment(4) == 'blocked') {
                        $this->db->where(array('status' => 2));
                    }


                    $this->db->where(array('id !=' => $this->session->userdata('id')));

                    $data['t'] = $this->db->get()->result();
                    //this is the loading of the pages
                    $this->load->view($root . 'users', $data);
                    break;

                case 'wallets':
                    $this->load->view($root . 'user_wallets', $data);
                    break;

                case 'topup':

                    break;
                case 'user_statement':

                    $data['t'] = $this->db->select()->from('user_statements')->where('user', $id)->get()->result();
                    $this->load->view($root . 'user_staments', $data);

                    break;

                default:

                    $this->form_validation->set_rules('user_role', 'User role', 'trim')->set_rules('status', 'Status', 'trim');
                    if ($this->form_validation->run() == 'true') {
                        strlen($user_role = $this->input->post('user_role')) > 0 ? $data['user_role'] = $user_role : '';
                        strlen($status = $this->input->post('status')) > 0 ? $data['status'] = $status : '';
                    }

                    if ($this->input->post('export') && ($this->input->post('export') == 'excel' || $this->input->post('export') == 'export_pdf')) {

                        $controller=$this->input->post('export')=='excel'?'excel_export/':'pdf_export/';

                        $data = array(
                            'alert' => 'success',
                            'message' => humanize($type) . ' Has been exported Successfully',
                            'hide' => 1
                        );
                        $this->load->view('alert', $data);
                        redirect($controller.'users');

                    }
                    $this->load->view($root . 'users', $data);
                    break;
            }

            $this->load->view('static/footer_table', $data);

        }

    public function hashValue($v)
        {
            return sha1(md5($v));
//            $this->add_logs()
        }

    function delete($id, $table)
        {
            $this->db->where('id', $id);
            if ($this->db->delete($table)) {
                return true;
            }
            else {
                return false;
            }
        }

    function phone_unique($str)
        {

            $dialing_code = $this->input->post('dialing_code');
            $phone = $dialing_code . $str;

            $output = $this->db->where(array('phone' => $phone))->from('users')->count_all_results();


            if (($output > 0)) {
                $this->form_validation->set_message('phone_unique', 'This <strong>%s</strong> is already Registered with us');
                return false;
            }
            else {
                return true;
            }


        }

    public function settings($type = null, $id = null)
        {
            $page_level = $this->page_level;
            $root = $page_level . $this->page_level2;

            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'link_details' => 'Account overview',
                'page_level' => $page_level
            );

            $this->load->view('static/header', $data);

            switch ($type) {
                //////////////////////// This is the part for the countries ///////////////////////////////////////
                case 'countries':
                    $this->load->view($root . 'countries', $data);
                    break;

                case 'new_country':
                    $this->form_validation->set_rules('country', 'Country', 'trim|is_unique[selected_countries.a2_iso]');
                    //checking if the form validation is passed
                    if ($this->form_validation->run() == true) {
                        $c = $this->db->select()->from('country')->where('a2_iso', $this->input->post('country'))->get()->row();
//                    country, a2_iso, a3_un, num_un, dialing_code, created_by, created_on, a2_iso, id
                        if (isset($c->country)) {
                            $values = array(
                                'country' => $c->country,
                                'a2_iso' => $c->a2_iso,
                                'a3_un' => $c->a3_iso,
                                'num_un' => $c->num_un,
                                'dialing_code' => $c->dialing_code,
                                'created_by' => $this->session->userdata('id'),
                                'created_on' => time()

                            );
                            $this->db->insert('selected_countries', $values);

                            $data['message'] = 'Record has been unblocked from accessing the System successfully';
                            $data['alert'] = 'success';
                            $this->load->view('alert', $data);
                        }
                        else {
                            $data['message'] = 'An error occurred during selection of the countries ' . anchor($this->page_level . $this->page_level2 . 'new_country', ' <i class="fa fa-refresh"></i> Try again', 'class="btn green-jungle btn-sm"');
                            $data['alert'] = 'warning';
                            $this->load->view('alert', $data);
                        }
                        $this->load->view($root . 'countries', $data);
                    }
                    else {


                        $this->load->view($root . 'new_country', $data);
                    }

                    $this->load->view($root . 'countries', $data);
                    break;

                //blocking the country
                case 'ban_country':
                    if ($this->db->where('a2_iso', $id)->update('selected_countries', array('status' => '0'))) {
                        // $this->db->where('country', $id)->update('branch', array('country_status' => '0'));
                        $data['message'] = 'Country has Been blocked and its Child Branches';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                    }


                    $this->load->view($root . 'countries', $data);
                    break;

                //this is the function for unblocking
                case 'unblock_country':
                    if ($this->db->where('a2_iso', $id)->update('selected_countries', array('status' => '1'))) {
                        //$this->db->where('country', $id)->update('branch', array('country_status' => '1'));
                        $data['message'] = 'Country has unblocked and its Child Branches';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                    }
                    $this->load->view($root . 'countries', $data);
                    break;
                //deleting_country
                case 'delete_country':
                    $this->db->where(array('a2_iso' => $id))->delete('selected_countries');
                    $data['message'] = 'Country has Deleted successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'countries', $data);
                    break;

                //////////// this is the end of the part for the countries//////////////////////

                case 'regions':

                    $this->load->view($root . 'regions', $data);
                    break;


                case 'newsletter':

                    $this->load->view($root . $type, $data);
                    break;

                default:
                    $this->load->view($page_level . 'reports/reports', $data);
                    break;
            }

            $this->load->view('static/footer_table', $data);

        }


    /**
     * This is where all  the message applies
     * sent message
     * received message
     * inbox
     * templates
     **/
    public function messaging($type = null, $id = null)
        {

            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;


            switch ($type) {

                default:
                    $this->load->view('static/header', $data);
                    $this->form_validation->set_rules('to', 'To', 'xss_clean|trim|required')
                        ->set_rules('subject', 'Subject', 'xss_clean|trim|required');

                    if ($this->form_validation->run() == true) {

                        $emails = explode(',', $this->input->post('to'));
                        $subject = $this->input->post('subject');
                        $message = $this->input->post('message');
                        $cc = $this->input->post('cc');
                        $bcc = $this->input->post('bcc');

                        foreach ($emails as $em) {

                            print_r($em);
                            valid_email($em) ? $this->custom_library->sendMail($em, $subject, $message, null, $cc, $bcc) : '';
                        }

                    }
                    else {

                    }

                    $this->load->view($root . 'messaging');
                    break;

                case 'app_inbox_reply':
                    $data['message_id'] = $id;//$this->input->get('message_id');//$_GET['message_id'];
                    // $file_name=$this->uri->segment(5);;
                    $this->load->view($root . $type, $data);
                    break;
                case 'inbox':

                    if (strlen($id) > 0) {
                        if ($id == 'Emails') {
                            $message_group = 'Email';
                        }
                        elseif ($id == 'SMS') {
                            $message_group = 'SMS';
                        }
                    }


                    $this->db->select()->from('inbox');

                    isset($message_group) ? $this->db->where(array('m_type' => $message_group)) : '';

//                    $this->session->userdata('user_type')==1?'':$this->db->where(array('company_id'=>$this->session->company));

                    $data['message'] = $this->db->order_by('id', 'desc')->limit(25)->get()->result();

                    $this->load->view($root . $type, $data);
                    break;
                case 'outbox':

                    if (strlen($id) > 0) {
                        if ($id == 'Emails') {
                            $message_group = 'Email';
                        }
                        elseif ($id == 'SMS') {
                            $message_group = 'SMS';
                        }
                    }


                    $this->db->select()->from('outbox');

                    isset($message_group) ? $this->db->where(array('m_type' => $message_group)) : '';

//                    $this->session->user_type==1?'':$this->db->where(array('company_id'=>$this->session->company));

                    $data['message'] = $this->db->order_by('id', 'desc')->limit(25)->get()->result();

                    $this->load->view($root . $type, $data);
                    break;
                case 'app_inbox_compose':

//                    $data=$this->input->post();
                    $this->load->view($root . $type);
                    break;
                case 'app_inbox_view':
                    $data['message_id'] = $id;//$this->input->get('message_id');//$_GET['message_id'];
                    $file_name = $this->uri->segment(5);;
                    $file = "app_" . $file_name . "_view";
                    $this->load->view($root . $file, $data);
                    break;


            }


            // $this->load->view('static/footer', $data);

        }

    public function site_options($type = null, $id = null)
        {

            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;


            $this->load->view('static/header', $data);

            $data['id'] = $id = $id / date('Y');
            switch ($type) {

                default:

                    $this->load->view($root . $title, $data);

                    break;
                case 'options':

                    break;
                case 'edit':


                    $data['op'] = $op = $this->db->select()->from('site_options')->where(array('id' => $id))->get()->row();


                    $this->form_validation->set_rules('option_value', 'Value', 'trim');

                    if ($op->option_name == 'site_logo') {
                        // this is the function which uploads the profile image

                        $this->form_validation->set_rules('image', 'Image', 'trim');
                        //this is the company name

                        //managing of the images
                        $path = $config['upload_path'] = './uploads/site_logo/';
                        $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                        $config['max_size'] = '200';
                        $config['max_width'] = '500';
                        $config['max_height'] = '1000';
                        $this->upload->initialize($config);


                        if (!is_dir($path)) //create the folder if it's not already exists
                        {
                            mkdir($path, 0777, TRUE);
                        }


                    }

                    if ($this->form_validation->run() == true || $this->upload->do_upload('image') == true) {

                        //echo $op->option_name.' success';
                        if ($op->option_name == 'site_logo') {

                            $value = $this->upload->do_upload('image') == true ? $path . $this->upload->file_name : $op->option_value;
                            $data['error'] = $this->upload->display_errors();

                        }
                        else {
                            $value = $this->input->post('option_value');
                        }


                        if ($this->db->where('id', $id)->update('site_options', array('option_value' => $value))) {

                            $data = array(
                                'alert' => 'success',
                                'message' => 'You have successfully updated site option'
                            );
                            $this->load->view('alert', $data);
                            $this->load->view($root . $title, $data);
                        }
                        else {
                            $data = array(
                                'alert' => 'error',
                                'message' => 'an error occurred while updating the the site option'
                            );
                            $this->load->view('alert', $data);
                            $this->load->view($root . $type, $data);
                        }


                    }
                    else {


                        $data['error'] = $this->upload->display_errors();
                        $this->load->view($root . $type, $data);
                    }
                    break;


            }


            $this->load->view('static/footer', $data);

        }

    function chat($type = null, $id = null)
        {

            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view('static/header', $data);

            $data['id'] = $id = isset($id) ? $id / date('Y') : null;
            $this->load->view($this->page_level . 'chat/chat', $data);


            $this->load->view('static/footer_table', $data);


        }


    function phpinfo()
        {
            echo phpinfo();
        }


    public function logs_upload($type = null, $id = null)
        {


            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;
            $this->load->view('static/header', $data);

            $data['id'] = $id = isset($id) ? $id / date('Y') : null;


            switch ($type) {

                default:
                    //$this->load->view($root .$title , $data);

                    $path = 'uploads/' . $this->page_level2 . '/';
                    $handle = fopen($path . 'access-Rwanda.log', 'r') or die ('File opening failed');

                    $data['handle'] = $this->extracting_file($handle);

                    $data['message'] = 'Item has been added successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view($root . 'new', $data);

                    break;

                case 'new':


                    $this->form_validation->set_rules('attachment', 'attachment', 'trim');
                    //checking if the form validation is passed
                    //managing of the images
                    $path = $config['upload_path'] = 'uploads/' . $this->page_level2 . '/';
                    $config['allowed_types'] = 'xls|xlsx|csv|XLS|XLSX|CSV|log|LOG';
                    // $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                    $config['max_size'] = '5000';
                    $config['max_width'] = '1920';
                    $config['max_height'] = '1000';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }

                    if ($this->form_validation->run() == true) {

                        //$attachment = $this->upload->do_upload('attachment') == true ? $path . $this->upload->file_name : '';

                        if (strlen($this->upload->display_errors()) > 0) {

                            $data['message'] = $this->upload->display_errors();
                            $data['alert'] = 'warning';
                            $date['hide'] = '';
                            $this->load->view('alert', $data);
                        }

                        $values = array(

//                            'resource_type' => $this->input->post('resource_type'),
                            'topic' => $this->input->post('title'),
                            'content' => $this->input->post('notes'),
                            'image' => $path . 'access-Rwanda.log',
                            'status' => 'published',
                            'blog_type' => $this->input->post('news_type'),
                            'created_by' => $this->session->userdata('id'),
                            'created_on' => time()

                        );
                        //$this->db->insert('blog', $values);

                        $handle = fopen($path . 'access-Rwanda.log', 'r') or die ('File opening failed');


                        $data['handle'] = $this->extracting_file($handle);

                        $data['message'] = 'Item has been added successfully';
                        $data['alert'] = 'success';
                        $this->load->view('alert', $data);
                        $this->load->view($root . $type, $data);

                    }
                    else {

                        $data['error'] = $this->upload->display_errors();
                        $this->load->view($root . $type, $data);
                    }

                    // $this->load->view($root . 'countries', $data);
                    break;


            }


            $this->load->view('static/footer_table', $data);

        }


    function extracting_file($file)
        {


            $output = array();
            while (!feof($file)) {

                $dd = fgets($file);

                array_push($output, $dd);
//                    $parts = explode('"', $dd);
//                    $statusCode = substr($parts[2], 0, 4);

            }

            return $output;
        }

    public function import($type = null)
        {

            $page_level = $this->page_level;
            $root = $page_level . $this->page_level2;
            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'link_details' => 'Account overview',
                'page_level' => $page_level

            );
            $this->load->helper('download');
            $this->load->view('static/header', $data);
            switch ($type) {
                case 'resource_persons':
                    // this is the function which uploads the profile image
                    $this->form_validation->set_rules('file_upload', 'File', 'xss_clean|trim')->set_rules('pa', 'Pas', 'xss_clean|trim');
                    //this is the company name


                    //managing of the images
                    $path = $config['upload_path'] = './uploads/imports/' . $type . '/';
                    $config['allowed_types'] = 'xls|xlsx|csv|XLS|XLSX|CSV';
                    $config['max_size'] = '200';
                    $this->upload->initialize($config);


                    if (!is_dir($path)) //create the folder if it's not already exists
                    {
                        mkdir($path, 0777, TRUE);
                    }
                    if ($this->form_validation->run() == true && $this->upload->do_upload('file_upload') == true) {

                        $values = array(
                            'path' => $path . $this->upload->file_name,
                            'file_name' => $this->upload->file_name,
                            'file_type' => $this->upload->file_type,
                            'file_ext' => $this->upload->file_ext,
                            'file_size' => $this->upload->file_size,
                            'original_name' => $this->upload->orig_name,
                            'created_on' => time(),
                            'created_by' => $this->session->userdata('id')
                        );
                        $this->db->insert('file_uploads', $values);

                        //ignore_user_abort(true); set_time_limit(0);
                        ///////////// this is the begining of the extraction of the files///////////////////
                        $excel = $this->extract_excel_rows($path . $this->upload->file_name);

                        $data['message'] = $this->upload->file_name . ' File is being uploaded Please Wait ';
                        $no = 0;
                        if (strlen(trim($excel[2]['A'])) == 0 || strlen(trim($excel[2]['B'])) == 0 || strlen(trim($excel[2]['D'])) == 0 || strlen(trim($excel[2]['E'])) == 0) {
                            $data['alert'] = 'danger';
                            $data['hide'] = '1';
                            $data['message'] = 'The file you are Uploading is not corresponding with the required file try again !!!
                        <br/> ' . anchor($root . 'resource_persons', 'Upload New');
                            $this->load->view('alert', $data);

                        }
                        else {


                            foreach ($excel as $col => $row) {
                                if ($col > 1) {

                                    if ($row['A'] != '' && $row['B'] != '' && $row['C'] != '' && $row['D'] != '' && $row['E'] != '') {

                                        $phone = phone2($row['D']);

                                        $r = $this->db->select('id')->from('resource_persons')->where(array('phone' => $phone,
                                            'email' => $row['E']))->get()->row();

                                        $trans = array(

                                            'full_names' => $row['A'],
                                            'title' => $row['B'],
                                            'area' => $row['C'],
                                            'phone' => $phone,
                                            'email' => $row['E'],
                                            'created_on' => time(),
                                            'created_by' => $this->session->userdata('id'),

                                        );

                                        if (count($r) == 0) {
                                            $this->db->insert('resource_persons', $trans);
                                        }
                                        else {
                                            $this->db->where('id', $r->id)->update('resource_persons', $trans);
                                        }

                                        $data['message'] = 'Data has been extracted Successfully ' . anchor($root . 'resource_persons', 'Upload New');
                                        $data['alert'] = 'success';
                                        $data['hide'] = '1';
                                        // $this->load->view('alert', $data);


                                    }
                                    else {
                                        $data['message'] = 'Please Fill in all the required Fields in the Excel Sheet  !!! ' . anchor($root . 'resource_persons', 'Upload New');
                                        $data['alert'] = 'danger';
                                        $data['hide'] = '1';
                                        $this->load->view('alert', $data);
                                    }
                                    $no++;
                                }

                            }
                        }


                        ///message for the successfully upload and extraction of the file
                        $data['message'] = '1.File has been Uploaded Successfully';
                        $data['alert'] = 'success';
                        $data['hide'] = '1';
                        $this->load->view('alert', $data);

                        $this->load->view($root . 'file_uploads', $data);

                    }
                    else {
                        $data['error'] = $this->upload->display_errors();
                        $this->load->view($this->page_level . $this->page_level2 . $type, $data);

                    }


                    break;
                default:

                    break;

            }

            $this->load->view('static/footer_table', $data);
        }

    function extract_excel_rows($file_and_dir)
        {
            $this->load->view('php-excel-reader/Classes/PHPExcel');
            $objPHPExcel = PHPExcel_IOFactory::load($file_and_dir);
            $objPHPExcel->setActiveSheetIndex(0);
            $rows = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            return $rows;
        }

    function editor($path, $width)
        {
            //Loading Library For Ckeditor
            $this->load->library('Ckeditor');
            $this->load->library('Ckfinder');
            //configure base path of ckeditor folder
            $this->ckeditor->basePath = base_url() . 'assets/ci-ckeditor/js/ckeditor/';
            $this->ckeditor->config['toolbar'] = 'Full';
            $this->ckeditor->config['language'] = 'en';
            $this->ckeditor->config['width'] = $width;
            //configure ckfinder with ckeditor config
            $this->ckfinder->SetupCKEditor($this->ckeditor, $path);
        }


    public function wallet($type = null, $id = null)
        {
            $data['page_level'] = $page_level = $this->page_level;
            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'link_details' => 'Account overview',
                'page_level' => $page_level

            );

            $this->load->view('static/header', $data);

            switch ($type) {
                case 'view_order':
//                $this->load->view($page_level.$this->page_level2.'view_order',$data);
                    break;

                default:
//                $this->load->view($page_level.'orders/orders');
                    break;
            }

            $this->load->view($page_level . 'footer_orders', $data);
            $this->load->view('ajax/orders');

        }

    function dob_check($str)
        {


            $then_year = date('Y', strtotime($str));
            $years = date('Y') - $then_year;

            if (($years <= 5) || ($years >= 70)) {
                $this->form_validation->set_message('dob_check', $years > 70 ?
                    '<strong>' . $years . '</strong> Yrs is over age, Years Must be 70 and Below '
                    :
                    '<strong>' . $years . '</strong> Yrs is Under  Age, Years Must be 6 and Above');
                return false;
            }
            else {
                return true;
            }

        }

    function checking_current_date($str)
        {
            if ($str >= (date('Y-m-d'))) {
                $this->form_validation->set_message('checking_current_date',
                    'The %s cannot be greater than Today');
                return false;
            }
            else {
                return true;
            }

        }


    public function payments($type = null, $id = null)
        {
            $data = array(
                'title' => 'orders',
                'subtitle' => 'invoices',
                'link_details' => 'Account overview',


            );
            $page_level = $this->page_level;
            $root = $page_level . $this->page_level2;
            $this->load->view('static/header', $data);
            switch ($type) {

                case 'pay_invoice':
                    $data['invoice_id'] = $invoice_id = $id / date('Y');
                    $data['invoice'] = $this->db->select()->from('invoices')->where('id', $invoice_id)->get()->row();
                    $this->load->view('payments/payments', $data);
                    break;
                case 'top_up':
                    $data['user_id'] = $user_id = $id / date('Y');
                    $data['wallet'] = $this->db->select()->from('wallet')->where('user', $user_id)->get()->row();
                    $this->load->view('payments/topup', $data);
                    break;

            }

            // $type!='pay_invoice'?$this->load->view('static/footer_table', $data):'';

        }


    function logs($type = 'all', $start = 0)
        {
            $page_level = $this->page_level;
            $root = $page_level . $this->page_level2;
            $data = array(
                'title' => $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $page_level

            );
            $this->load->view('static/header', $data);

            if ($type == 'realtime_monitoring') {
                $this->load->view($root . $type, $data);
            }
            else {

                ///////////////////////// this is the begining of the pagination
                $data['number'] = $number = $this->db->count_all('logs');
                $config['uri_segment'] = 4;
                $config['display_pages'] = FALSE;
                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                $config['cur_tag_open'] = '<li class="active"><a>';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['base_url'] = base_url('index.php/' . $this->page_level . '/logs/' . $type);
                $config['total_rows'] = $number;
                $data['per_page'] = $per_page = $config['per_page'] = 15;
                $this->pagination->initialize($config);
                $data['pg'] = $this->pagination->create_links();
                /////////////////////////this is the end of the paginition//////////////////////////

                $this->db->select('a.*,b.first_name,b.last_name')->from('logs a')->join('users b', 'a.created_by=b.id', 'left');
                $type == 'all' ? '' : $this->db->where('transaction_type', $type);
                //// this is when the filter is applied//////////////////
                $this->form_validation->set_rules('from', 'From', 'trim')->set_rules('to', 'To', 'trim')->set_rules('search', 'Search', 'trim');
                if ($this->form_validation->run() == true) {
                    $from = strtotime(date($this->input->post('from')));
                    $to = strtotime(date($this->input->post('to')));
                    strlen($this->input->post('from')) > 0 ? $this->db->where(array('a.created_on >=' => $from, 'a.created_on <=' => $to)) : '';
                    strlen($this->input->post('search')) > 0 ? $this->db->like(array('a.details' => $this->input->post('search')))->or_like(array('a.target' => $this->input->post('search'))) : '';

                    strlen($this->input->post('transaction_type')) > 0 ? $this->db->where(array('a.transaction_type' => $this->input->post('transaction_type'))) : '';
                }
                ///////////////this is the end of the filter//////////////
                $rows = $data['tlogs'] = $this->db->order_by('a.id', 'desc')->limit($per_page, $start)->get()->result();
                $data['rows'] = count($rows);
                $this->load->view($this->page_level . 'logs/logs', $data);
            }
            $this->load->view('static/footer_table', $data);

        }

    //this is the email verification

    function delete_table($table)
        {
            if ($this->db->truncate($table)) {
                echo 'all Data Deleted successfully';
            }
            else {
                echo $this->db->error();
            }

        }

    public function getNetwork($mobile)
        {
            $substring = substr($mobile, 0, 5);

            if ($substring == "25678" || $substring == "25677" || $substring == "25639") {
                return "MTN_UGANDA";

            }
            else if ($substring == "25675") {

                return "AIRTEL_UGANDA";

            }
            else if ($substring == "25670") {
                return "WARID_UGANDA";

            }
            else if ($substring == "25679") {

                return "ORANGE_UGANDA";

            }
            else if ($substring == "25671") {

                return "UTL_UGANDA";
            }
            else {
                return "RWANDACELL";
            }
        }

    public function GetSMSCode($length)
        {

            $codes = array();
            $chars = "0123456789";
            srand((double)microtime() * 1000000);
            $i = 0;
            $code = '';
            $serial = '';

            $i = 0;

            while ($i < $length) {
                $num = rand() % 10;
                $tmp = substr($chars, $num, 1);
                $serial = $serial . $tmp;
                $i++;
            }

            return $serial;

        }

    function pass_check($str)
        {
            $pass = $this->db->select('password')->from('users')->where(array('username' => $this->session->userdata('username'), 'password' => $this->hashValue($str)))->get()->row();
            if (!isset($pass->password)) {
                $this->form_validation->set_message('pass_check', ' The %s is incorrect Please type a Correct Password ');
                return false;
            }
            else {
                return true;
            }
        }


    //This is the profile function

    function code_check($str)
        {
            $code = $this->db->where(array('user_id' => $this->session->userdata('id'), 'code' => $str))->from('account_verification')->select('code')->get()->row();
            if (isset($code->code)) {
                return true;
            }
            else {
                $this->form_validation->set_message('code_check', 'The Verification Code you Provided is Wrong');
                return false;
            }

        }

    function email_verification()
        {

            $data['title'] = 'Email Verification';
            $data['subtitle'] = 'profile';
            $this->load->helper('text');
            //this is the query for the orders this month
            $this_month = strtotime(date('Y-m'));
            $total_order = $this->db->query('SELECT DISTINCT(shopping_code) FROM transactions where created_on >= ' . $this_month);
            $data['to'] = $total_order->result();
            // this is the query for new orders
            $new_orders = $this->db->query('SELECT DISTINCT(shopping_code) FROM transactions where transaction_status ="pending" ');
            $data['no'] = $new_orders->result();
            //this is the number of items which are completed
            $data['completed'] = $this->db->where('transaction_status', 'completed')->from('transactions')->count_all_results();
            $data['users'] = $this->db->count_all('users');
            // this is the begining of form validation
            $this->form_validation->set_rules('code', 'Code', 'required|trim|callback_code_check');
            if ($this->form_validation->run() == true) {
                $this->db->where('id', $this->session->userdata('id'))->update('users', array('verified' => 1));
                $this->session->set_userdata(array('verified' => 1));
                $data['alert'] = 'success';
                $data['message'] = 'Your Account Has been Verified';
                $email_msg = "Dear " . $this->session->userdata('fname') . " " . $this->session->userdata('lname') . " \n\r\n We Thank you for using Our Services\n\r\nYour Account Has Been Verified";
                $this->sendemail($this->session->userdata('email'), 'Email Verification', $email_msg);
            }
            $this->load->view('static/header', $data);
            $this->load->view($this->page_level . 'dashboard/dashboard');
            $this->load->view('static/footer');


        }

    //This google currency Converter

    function sendemail($reciever, $subject, $message)
        {

            $this->load->library('email');
            $config['protocol'] = 'smtp';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['useragent'] = 'Virtual Wallet';
            $config['smtp_host'] = 'www.timesolut.com';
            $config['smtp_user'] = 'no-reply@timesolut.com';
            $config['smtp_pass'] = 'b],PLIa~1y7h';
            $this->email->initialize($config);
            $this->email->from('no-reply@virtualwallet.com', 'Virtual Wallet');
            $this->email->to($reciever);
            $this->email->subject('Virtual Wallet | ' . $subject . ' Time >> ' . strftime("%T", time()));
            $this->email->message($message);
            if ($this->email->send()) {
                return true;
            }


        }

    //This is yahoo currency Converter

    function resend_code()
        {
            $data['title'] = 'email_verification';
            $data['subtitle'] = 'profile';
            $data['message'] = 'Code Has Been sent on your email (' . $this->session->userdata('email') . ')';
            $data['alert'] = 'success';
            $code = $this->db->where('user_id', $this->session->userdata('id'))->select('code')->from('account_verification')->get()->row();
            $email_msg = "Dear " . $this->session->userdata('fname') . " " . $this->session->userdata('lname') . " \n\r\n We Thank you for using Our Services\n\r\n Your Verification Code: " . $code->code;

            $this->sendemail($this->session->userdata('email'), 'Email Verification', $email_msg);
            $this->load->view('static/header', $data);
            $this->load->view($this->page_level . 'dashboard/home');
            $this->load->view('static/footer');

        }

    function profile($type = null)
        {
            $data['title'] = $this->uri->segment(2);
            $data['subtitle'] = $type == null ? 'user_profile' : $type;

            $this->load->view('static/header', $data);
            if ($type == null) {
                $this->load->view($this->page_level . 'profile/user_profile');
            }
            elseif ($type == 'info') {
                $this->form_validation->set_rules('email', 'Email', 'valid_email|trim')
                    ->set_rules('phone', 'Phone', 'trim|max_length[13]|min_length[10]')
                    ->set_rules('password', 'Current password', 'trim|callback_pass_check')
                    ->set_rules('fullname', 'Full Name', 'trim');
                if ($this->form_validation->run() == false) {

                    $this->load->view($this->page_level . 'profile/user_profile', $data);
                }
                else {
                    $this->db->where('id', $this->session->userdata('id'))
                        ->update('users', array(
                            'email' => $this->input->post('email'),
                            'phone' => $this->input->post('phone'),
                            'full_name' => $this->input->post('fullname')

                        ));
                    $values = array(
                        'fullname' => $this->input->post('fullname'),
                        'phone' => $this->input->post('phone'),
                        'email' => $this->input->post('email')
                    );


                    $this->session->set_userdata($values);
                    $data['message'] = 'You have successfully Updated your Profile';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view($this->page_level . 'profile/user_profile', $data);

                }
            }
            elseif ($type == 'change_password') {


                $this->form_validation->set_rules('current_password', 'Current password', 'trim|callback_pass_check')->set_rules('new_pass', 'New Password', 'required|trim|matches[rpt_pass]')->set_rules('rpt_pass', 'Repeat Password', 'required|trim');
                if ($this->form_validation->run() == false) {

                    $this->load->view($this->page_level . 'profile/user_profile', $data);
                }
                else {
                    $this->db->where('id', $this->session->userdata('id'))->update('users', array('password' => $this->hashValue($this->input->post('new_pass'))));
                    $data['message'] = 'You have successfully Changed your password <br/>';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);
                    $this->load->view($this->page_level . 'profile/user_profile', $data);
                }
            }
            elseif ($type == 'change_avatar') {

                // this is the function which uploads the profile image
                $this->form_validation->set_rules('image', 'Image', 'trim')->set_rules('pa', 'Pas', 'trim');
                //this is the company name

                $cname = $this->session->userdata('fullname');
                //managing of the images
                $path = $config['upload_path'] = './uploads/profile/' . underscore($cname) . '/';
                $config['allowed_types'] = 'gif|jpg|png|GIF|JPG|PNG';
                $config['max_size'] = '200';
                $config['max_width'] = '1920';
                $config['max_height'] = '850';
                $this->upload->initialize($config);


                if (!is_dir($path)) //create the folder if it's not already exists
                {
                    mkdir($path, 0777, TRUE);
                }
                if ($this->form_validation->run() == true && $this->upload->do_upload('image') == true) {

                    $values = array(
                        'photo' => $path . $this->upload->file_name,
                        'updated_on' => time(),
                        'updated_by' => $this->session->userdata('id')
                    );
                    $this->db->where('id', $this->session->userdata('id'))->update('users', $values);
                    $this->session->set_userdata(array('photo' => $path . $this->upload->file_name));
                    $data['message'] = 'Image has been updated Successfully';
                    $data['alert'] = 'success';
                    $this->load->view('alert', $data);

                }
                else {
                    $data['error'] = $this->upload->display_errors();
                    $this->load->view($this->page_level . 'profile/user_profile', $data);

                }

            }
            $this->load->view('static/footer_table');
        }


    function permissions($type = null, $id = null)
        {
            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;

            $this->load->view('static/header', $data);


            switch ($type) {

                default;
                case 'permissions';
                    $this->load->view($root . $title, $data);
                    break;

                case 'new':

                    $this->form_validation
                        ->set_rules('permission', 'Permission', 'required|trim|is_unique[permissions.title]')
                        ->set_rules('group', 'Group', 'required|trim')
                        ->set_rules('description', 'Description', 'trim');
                    //checking if the form validation is passed
                    if ($this->form_validation->run() == true) {

                        $values = array(
                            'title' => $this->input->post('permission'),
                            'perm_group' => $this->input->post('group'),
                            'perm_desc' => $this->input->post('description'),
                            'created_by' => $this->session->id,
                            'created_on' => time()
                        );
                        if ($this->db->insert('permissions', $values)) {
                            $data = array(
                                'alert' => 'success',
                                'message' => 'You have successfully added a permission'
                            );
                            $this->load->view('alert', $data);
                        }


                    }
                    else {
                        $this->load->view($root . $type, $data);
                    }
                    $this->load->view($root . $title, $data);
                    break;

                case 'roles':
                    $this->load->view($root . $type, $data);
                    break;

                case 'remove_permission':

                    $id = $id / date('Y');

                    if ($this->delete($id, 'role_perm')) {
                        $data = array(
                            'alert' => 'success',
                            'message' => 'Permission has been removed successfully'
                        );
                        $this->load->view('alert', $data);
                    }
                    $this->load->view($root . 'roles', $data);
                    break;

                case 'role_perm':
                    $data = array(
                        'id' => $id = $id / date('Y')
                    );

                    $this->form_validation->set_rules('permissions[]', 'Permission', 'trim');

                    if ($this->form_validation->run() == true) {

                        //print_r($this->input->post('permissions'));

                        $values = array();
                        foreach ($this->input->post('permissions') as $perm_id) {

                            $select_perm = array(
                                'role_id' => $id,
                                'perm_id' => $perm_id
                            );

                            $res = $this->db->where($select_perm)->from('role_perm')->count_all_results();

                            $res == 0 ? array_push($values, $select_perm) : '';

                        }


                        count($values) > 0 ? $this->db->insert_batch('role_perm', $values) : '';

                        $data = array(
                            'id' => $id,
                            'alert' => 'success',
                            'message' => 'Roles are added successfully'
                        );

                        $this->load->view('alert', $data);

                    }
                    else {

                    }

                    $this->load->view($root . $type, $data);
                    break;


            }


            $this->load->view('static/footer_table', $data);
        }


    function coverage($type = null, $id = null)
        {
            $data = array(
                'title' => $title = $this->uri->segment(2),
                'subtitle' => $type,
                'page_level' => $this->page_level

            );
            $root = $this->page_level . $this->page_level2;

            $this->load->view('static/header', $data);

            switch ($type) {

                default;
                    $this->load->view($root . $title, $data);

                    break;


                case 'allocation':
                    $this->load->view($root . 'coverage_accodian');

                    break;
                case 'distribution_gap':
                    $this->load->view($root . $type);

                    break;

                case 'map_view':

                    $this->load->view($root .'maps/county/' . $type);

                    break;
                case 'registration_map':

                    $data['geojson']=$this->model->build_Geo_json($type);
                    $this->load->view($root .'maps/county/'. $type,$data);

                    break;
                case 'distribution_map':

                    $data['geojson']=$this->model->build_Geo_json($type);
                    $this->load->view($root .'maps/county/'. $type,$data);

                    break;



                case 'maps':
                    $subtype=$id;
                    $ext='/subcounty/';
                    switch ($subtype){
                        case 'registration_map':

                            $data['geojson']=$this->model->build_Geo_json_subcounty($subtype);
                            $this->load->view($root . $type.$ext.$subtype,$data);

                            break;
                        case 'distribution_map':

                            $data['geojson']=$this->model->build_Geo_json_subcounty($subtype);
                            $this->load->view( $root . $type.$ext.$subtype,$data);

                            break;

                        case 'map_view':
                            $data['geojson']=$this->model->build_Geo_json_subcounty();
                            $this->load->view($root . $type.$ext . $subtype,$data);

                            break;

                    }
                    break;



            }
            $this->add_logs($this->session->id,'coverage',humanize($type),' has listed');
            $this->load->view('static/footer_table', $data);
        }


    function missing_districts()
    {
//           Array ( [0] => Array ( [id] => 5 [name] => Northern ) [1] => Array ( [id] => 105 [name] => Apac ) [2] => Array ( [id] => 200 [name] => Akokoro ) )
        // $path = $this->locations->get_path($id);
        // LEFT JOIN baseline_subcounty b ON a.id=b.location_id
        $subs = $this->db->select('a.id,b.id as mis')->from('subs a')->join('baseline_subcounty b', 'a.id=b.location_id', 'left')->get()->result();
//           print_r($subs);
        echo '<table width="50%">';
        echo '<thead><tr><th>District</th><th>Subcounty</th><th>Status</th></tr></thead>';
        echo '<tbody>';
        foreach ($subs as $s):
            $path = $this->locations->get_path($s->id);
            echo '<tr>';
            echo '<td>' . $path[1]['name'] . '</td>';
            echo '<td>' . $path[2]['name'] . '</td>';
            echo '<td>' . (strlen($s->mis) > 0 ? 'Available' : 'Missing') . '</td>';
            echo '</tr>';
        endforeach;
        echo '</tbody>';
        echo '</table>';

    }

    function url_get_contents($url)
        {
            if (function_exists('curl_exec')) {
                $conn = curl_init($url);
                curl_setopt($conn, CURLOPT_SSL_VERIFYPEER, true);
                curl_setopt($conn, CURLOPT_FRESH_CONNECT, true);
                curl_setopt($conn, CURLOPT_RETURNTRANSFER, 1);
                $url_get_contents_data = (curl_exec($conn));
                curl_close($conn);
            }
            elseif (function_exists('file_get_contents')) {
                $url_get_contents_data = file_get_contents($url);
            }
            elseif (function_exists('fopen') && function_exists('stream_get_contents')) {
                $handle = fopen($url, "r");
                $url_get_contents_data = stream_get_contents($handle);
            }
            else {
                $url_get_contents_data = false;
            }
            return $url_get_contents_data;
        }

    public function session_logs($method = null, $page = null)
        {
            if ($this->isloggedin()) {
                $data['view'] = 'session_logs';
                $data['sublink'] = 'session_logs';
                $data['link_details'] = 'session_logs';

                $data['page'] = strlen($page) > 0 ? $page : 0;


                $this->load_admin($data);
            }
            else {
                $this->logout();
            }


        }

    public function sms_routes($method = null, $id = null)
        {
            if ($this->isloggedin()) {
                //$data = array();
                //$data['controller'] = 'sms_routes';
                $data['view'] = 'sms_routes';
                $data['sublink'] = 'sms_routes';
                $data['link_details'] = 'manage_sms_routes';


                switch ($method) {
                    case 'delete':


                        $this->db->where(array('id' => $id / date('Y')))->delete('sms_routes');
                        redirect($this->page_level . 'sms_routes', 'refresh');
                        // echo '<META http-equiv=refresh content=0;URL='.base_url().'index.php/admin/sms_routes/>';
                        break;
                    case 'activate': {
                        $this->db->update('sms_routes', array('status' => 0));
                        $this->db->update('sms_routes', array('status' => 1), array('id' => $id / date('Y')));
                    }
                        //   echo '<META http-equiv=refresh content=0;URL='.base_url().'index.php/admin/sms_routes/>';
                        redirect($this->page_level . 'sms_routes', 'refresh');
                        break;

                    default: {

                        //   print_r($_REQUEST);
                        if (isset($_REQUEST['route'])) {


                            if ($this->db->select('id')->from('sms_routes')->where('route_name = "' . $_REQUEST['route_name'] . '" or parameters = "' . $_REQUEST['route'] . '"')->get()->num_rows() == 0) {
                                $values = array('route_name' => $_REQUEST['route_name'],
                                    'parameters' => $_REQUEST['route'],

                                    'created_at' => date('Y-m-d H:i:s'));

                                $data['alert'] = 'alert-info';
                                $data['message'] = 'The SMS route added successfully';

                                $this->db->insert('sms_routes', $values);
                            }
                            else {

                                $data['alert'] = 'alert-danger';
                                $data['message'] = 'The SMS route you are adding already exists.';


                            }


                        }


                    }
                        break;

                }


                $this->load_admin($data);


            }
            else {

                $this->logout();
            }


        }

    function custom_queries($list = 'order_list')
        {


            $this->load->library('table');


            switch ($list) {

                case 'order_list':

                    $data = $this->db->query("SELECT
c.`id` as `AccNo`,
c.`first_name` as `First Name`,
c.`last_name` as `Last Name`,
c.phone,
b.`title`,
a.`class`,
a.`stream`
FROM `users` c LEFT JOIN `kids` a ON a.user_id=c.id LEFT JOIN schools b ON a.school_id=b.id JOIN orders d on d.subscriber_id=c.id WHERE c.user_type=6 AND c.sub_type !=5 AND d.status='paid'");

                    // $this->table->set_heading('First Name', 'Color', 'Size');
                    break;
                case 'table':

                    $this->table->set_heading('Name', 'Color', 'Size');
                    $this->table->add_row('Fred', 'Blue', 'Small');
                    $this->table->add_row('Mary', 'Red', 'Large');
                    $this->table->add_row('John', 'Green', 'Medium');

                    echo $this->table->generate();

                    $this->table->clear();

                    $this->table->set_heading('Name', 'Day', 'Delivery');
                    $this->table->add_row('Fred', 'Wednesday', 'Express');
                    $this->table->add_row('Mary', 'Monday', 'Air');
                    $this->table->add_row('John', 'Saturday', 'Overnight');
                    break;

            }


            $tmpl = array(
                'table_open' => '<table border="1" cellpadding="4" cellspacing="0">',

                'heading_row_start' => '<tr align="left">',
                'heading_row_end' => '</tr>',
                'heading_cell_start' => '<th>',
                'heading_cell_end' => '</th>',

                'row_start' => '<tr>',
                'row_end' => '</tr>',
                'cell_start' => '<td>',
                'cell_end' => '</td>',

                'row_alt_start' => '<tr>',
                'row_alt_end' => '</tr>',
                'cell_alt_start' => '<td>',
                'cell_alt_end' => '</td>',

                'table_close' => '</table>'
            );

            $this->table->set_template($tmpl);
            //$this->table->function = 'htmlspecialchars';
            echo $this->table->generate($data);


        }

    function reformat_date($date)
        {
            $newDate = explode(' ', $date);

            $date_p = $newDate[0];
            $time = $newDate[1];

            $date_p_splitted = explode('-', $date_p);

            $day = str_pad($date_p_splitted[0], 2, "0", STR_PAD_LEFT);
            $month = $date_p_splitted[1];
            $year = $date_p_splitted[2];

            $date = $year . '-' . $month . '-' . $day . ' ' . $time;

            return $date;
        }

    public function change_destination($destination, $duration = 2)
        {
            echo '<META http-equiv=refresh content=' . $duration . ';URL=' . base_url() . 'index.php/' . $destination . '>';
        }




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

