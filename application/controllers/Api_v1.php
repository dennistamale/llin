<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class api_v1 extends CI_Controller {

    public $username = "apiuser";
    public $password = "@p1us3r";


    function __construct()
        {
            parent::__construct();
            $this->api_auth();

        }

//this is the function for basic authentication
    function api_auth()
        {
            if (($_SERVER['PHP_AUTH_PW'] != $this->password || $_SERVER['PHP_AUTH_USER'] != $this->username) || !$_SERVER['PHP_AUTH_USER']) {

                header('WWW-Authenticate: Basic realm="Test auth"');
                header('HTTP/1.0 401 Unauthorized');
                $response = 'Authorization failed';

                header('Content-Type:application/json');
                $response = array(
                    'response' => 'error',
                    'output' => $response
                );
                echo json_encode($response);
                exit;


            }
        }


    function index()
        {

        }



    function get_personnel($dp_code = null)
        {


            if ($this->input->method() == 'get') {

                if(isset($dp_code)) {

                    $this->db->select()->from('distribution_point');
                    isset($id) ? $this->db->where(array('dp_code' => $dp_code)) : '';
                    $sp = $this->db->get()->result();


                    if (count($sp) > 0) {


                        //id, first_name, last_name, birth_date, gender, district, country, email, phone, signature, image, status, created_on, created_by, updated_on, updated_by, id, id
                        foreach ($sp as $ch => $key) {

//                    array($key->gender=>($key->gender==1?'Female':'Male'));


                            $u[$ch]['first_name'] = $key->first_name;
                            $u[$ch]['last_name'] = $key->last_name;
                            $u[$ch]['birth_date'] = $key->birth_date;
                            $u[$ch]['gender'] = $key->gender == 1 ? '1*Male' : '0*Female';
                            $u[$ch]['district'] = $key->district . '*' . $this->locations->get_location_name($key->district);
                            $u[$ch]['country'] = $key->country . '*' . $this->locations->get_location_name($key->country);
                            $u[$ch]['email'] = $key->email;
                            $u[$ch]['phone'] = $key->phone;
                            $u[$ch]['signature'] = $key->signature;
                            $u[$ch]['image'] = $key->image;
                            $u[$ch]['status'] = $key->status;
                            $u[$ch]['created_on'] = $key->created_on;
                            $u[$ch]['created_by'] = $key->created_by;
                            $u[$ch]['updated_on'] = $key->updated_on;
                            $u[$ch]['updated_by'] = $key->updated_by;

                        }

                        $response = array(
                            'response' => 'success',
                            'output' => $u
                        );

                    }
                    else {
                        $output = 'No records found';
                        $response = array(
                            'response' => 'error',
                            'output' => $output
                        );
                    }
                }else{
                    $output = 'Please provide subcounty code';
                    $response = array(
                        'response' => 'error',
                        'output' => $output
                    );
                }

            }
            else {
                $output = 'This method is not allowed';
                $response = array(
                    'response' => 'error',
                    'output' => $output
                );
            }

            header('Content-Type:application/json');
            echo json_encode($response);

        }




}