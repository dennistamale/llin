<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//define('HEAD', 'Head');
define('SUBHEAD', 'Print Date : ' . date('d-m-Y H:i:s'));
define('LOGO', 'uploads/site_logo/coart-of-arms.png');
//define('ORIENTATION',isset($orientation)?$orientation:'P');

//error_reporting(0);
class Pdf_export extends CI_Controller {


    public $logo = '';
    public $orientation = "";

    public $page_level = "";
    public $page_level2 = "";
    public $page_level3 = "";
    public $sub_head = "";
    public $location_title=null;

    function __construct()
        {
            parent::__construct();
            $this->isloggedin() == true ? '' :$this->invalid();
            $this->load->library('fpdf_gen');

            $this->page_level = $this->uri->segment(1);
            $this->page_level2 = $this->uri->segment(2);
            $this->page_level3 = $this->location_title= $this->uri->segment(3);

//            $this->logo = base_url($this->site_options->title('site_logo'));


            $this->fpdf->AddPage();
            $this->cover_page();

            //$this->fpdf->AliasNbPages();

           // $this->fpdf->AddPage();


        }

        function invalid(){

           echo "You can't access this report Contact administrator";
            exit;
}

    public function isloggedin()
        {
            return $this->session->userdata('user_type') == 1 || $this->session->userdata('user_type') == 2 || $this->session->userdata('user_type') == 3 || $this->session->userdata('user_type') == 4 || $this->session->userdata('user_type') == 5|| $this->session->userdata('user_type') == 6 ? true : false;

        }

    public function index()
        {

            $this->orientation = 'L';

            $this->fpdf->SetFont('Arial', 'B', 16);
            $this->fpdf->Cell(40, 10, 'Sorry You Did not select any pdf to Export !!!');


            //$this->fpdf->AddPage('P');
//        $this->fpdf->AddPage();
            echo $this->fpdf->Output('LLINPdd.pdf', 'I');
//        $this->fpdf->AddPage();
//		echo $this->fpdf->Output('hello_world.pdf','I');
        }

    function sub_head($sub_head = null)
    {
        $fg= intval($this->fpdf->GetPageWidth());
        isset($sub_head) ?($fg>=297?$this->fpdf->Cell(0, 6, humanize($sub_head), 0, 1, 'C'):$this->fpdf->Cell(190, 6, humanize($sub_head), 0, 1, 'C') ): '';
    }

    function cover_page(){

             $vill=$this->location_title;
            $this->fpdf->SetFont('Arial', 'B', 50);


            $this->fpdf->Cell(0, 50, 'LLIN', 0, 0, 'C');
            $this->fpdf->Ln();
            $this->fpdf->Cell(0, 5, humanize($this->page_level2=='distribution_report'?'Allocation':$this->page_level2 ), 0, 0, 'C');
            $this->fpdf->Ln(25);
            $this->fpdf->Cell(0, 5, humanize($this->page_level2=='distribution_report'?'List':'Report' ), 0, 0, 'C');
            $this->fpdf->Ln(60);
            $this->fpdf->Image(LOGO, 90, 110, 30);
            $this->fpdf->SetFont('Arial', 'B', 15);

        is_int($vill) ?( $this->fpdf->Cell(0, 5, humanize((isset($vill)?$this->locations->get_location_type($vill):'Location').' : '.(isset($vill)?$this->locations->get_location_name($vill):'All Data')), 0, 0, 'C')):$this->fpdf->Cell(0, 5,humanize($vill), 0, 0, 'C');

            $this->page_level2=='distribution_report'?'':$this->fpdf->AddPage();






        }

    function users()
        {

            $this->sub_head($this->page_level2 . ' Report');
            $this->fpdf->AliasNbPages();

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'Name', 'Phone', 'Location', 'username', 'Email',);

            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 10);

// Header
            $w = array(8, 40, 25, 20, 45, 45);
            for ($i = 0; $i < count($header); $i++)
                $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 8);

// Data
            $fill = false;
            $no = 1;
            $id = $this->db->select()->from('users')->get()->result();
            foreach ($id as $d) {


                $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[1], 6, humanize($d->first_name . ' ' . $d->last_name), 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[2], 6, $d->phone, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], 6, $d->city, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[4], 6, $d->username, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[5], 6, $d->email, 1, 0, 'L', $fill);
                $this->fpdf->Ln();
                $fill = !$fill;
                $no++;
            }

// Closing line
            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN ' . $this->page_level2 . ' export ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);
        }

    function vht()
        {

            $this->sub_head($this->page_level2 . ' Report');
            $this->fpdf->AliasNbPages();
            // $this->fpdf->AddPage();

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'Name', 'Phone', 'Code', 'District', 'Parish', 'Village');

            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 10);

// Header
            $w = array(8, 45, 25, 15, 30, 30, 30);
            for ($i = 0; $i < count($header); $i++)
                $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 8);

// Data
            $fill = false;
            $no = 1;
            $id = $this->db->select()->from('vht')->get()->result();


            foreach ($id as $d) {

                $path = $this->locations->get_path($d->village);


                $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[1], 6, humanize($d->first_name . ' ' . $d->last_name), 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[2], 6, $d->phone, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], 6, $d->code, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[4], 6, $path[1]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[5], 6, $path[3]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[6], 6, $path[4]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Ln();
                $fill = !$fill;
                $no++;
            }

// Closing line
            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN ' . $this->page_level2 . ' export ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);
        }

    function village($vill = null)
        {


            $this->sub_head($this->page_level2 . ' Report');
            $this->fpdf->AliasNbPages();
            // $this->fpdf->AddPage();

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'Sub County', 'Parish', 'Village', 'Households', 'Popn', 'Nets');

            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 10);

// Header
            $w = array(8, 35, 30, 30, 25, 30, 30);
            for ($i = 0; $i < count($header); $i++)
                $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 8);

// Data
            $fill = false;
            $no = 1;


            if (isset($vill)) {

                $wher = $this->all_villages($vill, 'a.village');

            }

            $this->db->select('distinct(a.village) as village,b.name')->from('registration a')->join('locations_view b', 'a.village=b.id')->where('confirm', 1);

            isset($wher) ? $this->db->where($wher) : '';

            $qry = $this->db->order_by('a.id', 'desc')->group_by('a.village')->get()->result();

            foreach ($qry as $q) {


                $for_det = $this->db->select('b.person_vht,b.person_final')
                    ->from('registration a')
                    ->join('reg_detail b', 'b.data_id=a.id')
                    ->where(array('a.village' => $q->village, 'confirm' => 1))->get()->result();


                $np = nets_popn_agg($for_det);

                $path = $this->locations->get_path($q->village);


                $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[1], 6, $path[2]['name'], 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[2], 6, $path[3]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], 6, $q->name, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[4], 6, count($for_det), 1, 0, 'C', $fill);
                $this->fpdf->Cell($w[5], 6, $np['population'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[6], 6, $np['house_hold_nets'], 1, 0, 'L', $fill);
                $this->fpdf->Ln();
                $fill = !$fill;
                $no++;
            }

            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN ' . humanize($this->page_level2) . ' export ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);


        }

    function all_villages($id, $village = 'village')
        {
            $villages = '(';

            $village_array = $this->locations->get_villages_under_selection($id);
            $nov = count($village_array);
            $n1 = 1;
            foreach ($village_array as $v) {
                $villages .= " $village = $v ";
                $villages .= $nov == $n1 ? '' : ' OR ';
                $n1++;

            }
            $villages .= ')';
            return $villages;

        }

    function households($vill = null)
        {


            $this->sub_head($this->page_level2 . ' Report');
            $this->fpdf->AliasNbPages();
            // $this->fpdf->AddPage();

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'Household', 'ID Number', 'Chalk ID', 'Phone', 'Village', 'Subcounty', 'Popn', 'Nets', 'Picked');


            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 10);

// Header
            $w = array(8, 32, 30, 16, 20, 25, 25, 10, 10, 15);
            for ($i = 0; $i < count($header); $i++)
                $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 8);

// Data
            $fill = false;
            $no = 1;


            if (isset($vill)) {

                $wher = $this->all_villages($vill, 'd.village');

            }


            $this->db->select('a.*,d.form_num,d.village')->from('reg_detail a')
                ->join('registration d', 'a.data_id=d.id')
                ->join('forms c', 'c.form_num=d.form_num')
                ->where('d.confirm', 1);

            isset($wher) ? $this->db->where($wher) : '';

            $qry = $this->db->order_by('d.village', 'asc')->get()->result();

            foreach ($qry as $sb) {

                $final_pop=$sb->person_final!=0?$sb->person_final:$sb->person_vht;
                $nets=nets_agg_only($final_pop);

//                var_dump($nets);


                $path = $this->locations->get_path($sb->village);
                //this is the end for the status


                $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[1], 6, (strlen($sb->last_name) ? $sb->last_name.' '. $sb->first_name : 'No Name'), 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[2], 6, $sb->national_id, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], 6, $sb->chalk_id, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[4], 6, $sb->tel, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[5], 6, $path[4]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[6], 6, $path[2]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[7], 6, $final_pop, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[8], 6, $nets['house_hold_nets'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[9], 6, $sb->no_picked, 1, 0, 'L', $fill);
                $this->fpdf->Ln();
                $fill = !$fill;
                $no++;
            }

            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN ' . humanize($this->page_level2) . ' export ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);


        }

    function distribution_report($vill = null)
        {
//            error_reporting(0);
            $this->fpdf->Ln(30);
            $this->fpdf->Cell(0, 5, 'Start Date : __ __/__ __/__ __ __ __/', 0, 0, 'C');
            $this->fpdf->Ln(20);
            $this->fpdf->Cell(0, 5, 'End Date : __ __/__ __/__ __ __ __/', 0, 0, 'C');


            //this is the beginning of the report
            $this->fpdf->AddPage('L');
            if (isset($vill)) {

                $wher = $this->all_villages($vill, 'd.village');

            }


            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->AliasNbPages();

            $this->sub_head($this->page_level2 . ' Report');

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'Household', 'ID Number', 'Chalk ID', 'Phone', 'Village', 'Subcounty', '#HH Popn', 'Allocated', 'Received', 'Signature');


            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 9);

// Header
            $w = array(8, 35, 30, 16, 20, 25, 30, 25, 20, 15, 43);
            for ($i = 0; $i < count($header); $i++)



                            $this->fpdf->Cell($w[$i], 8, $header[$i], 0, 0, 'L', true);


            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 9);

// Data
            $fill = false;
            $no = 1;



            $this->db->select('a.*,d.form_num,d.village')->from('reg_detail a')
                ->join('registration d', 'a.data_id=d.id')
                ->join('forms c', 'c.form_num=d.form_num')
                ->where('d.confirm', 1);

            isset($wher) ? $this->db->where($wher) : '';

            $qry = $this->db->order_by('a.last_name', 'asc')->get()->result();

            $h=14;
            foreach ($qry as $sb) {

                $original_num = bcdiv(($sb->person_vht / 2), 1, 0);
                $modulus_num = ($sb->person_vht % 2) == 0 ? 0 : 1;
                $nets = $original_num + $modulus_num;


                $path = $this->locations->get_path($sb->village);
                //this is the end for the status

                $this->fpdf->Cell($w[0], $h, $no . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[1], $h, (strlen($sb->last_name) ? $sb->last_name.' '. $sb->first_name  : 'No Name'), 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[2], $h, $sb->national_id, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], $h, $sb->chalk_id, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[4], $h, $sb->tel, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[5], $h, $path[4]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[6], $h, $path[2]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[7], $h, $pop = $sb->person_vht, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[8], $h, $nets, 1, 0, 'L', $fill);
                $this->fpdf->SetTextColor(137, 137, 137);
                $this->fpdf->Cell($w[9], $h, '', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[10], $h, '', 1, 0, 'L', $fill);

                $this->fpdf->Ln();
                $this->fpdf->SetTextColor(0);
                $fill = !$fill;
                $no++;
            }

            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN Allocation List export ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);


        }

    function list_inventory($vill = null)
        {


            $this->sub_head($this->page_level2 . ' Report');
            $this->fpdf->AliasNbPages();
            // $this->fpdf->AddPage();

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'Request ID', 'Date', 'Requester', 'Forms', 'Reqst type', 'Reqst Status', 'Approver', 'Approval Date');



            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 10);

// Header
            $w = array(8, 20, 20, 25, 15, 20, 25, 30,'');
            for ($i = 0; $i < count($header); $i++)
                $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 8);

// Data
            $fill = false;
            $no = 1;


            if (isset($vill)) {

                $wher = $this->all_villages($vill, 'd.village');

            }

            $this->db->select('a.id,a.form_num,a.approved_by,a.approved_on,count(a.request_code) as forms,a.request_code,a.request_type,a.created_on,a.status as request_status,d.first_name,d.last_name')
                ->from('inventory a')
                ->join('users d', 'a.requested_by=d.id', 'left');

            isset($wher) ? $this->db->where($wher) : '';

            $qry = $this->db->order_by('a.id', 'desc')->group_by('request_code')->get()->result();

            foreach ($qry as $sb) {

                //this is the end for the status

                $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[1], 6, $sb->request_code, 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[2], 6, trending_date($sb->created_on), 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], 6, strlen($sb->first_name) > 0 ? $sb->first_name . ' ' . $sb->last_name : 'Not registered', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[4], 6, $sb->forms, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[5], 6, humanize($sb->request_type), 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[6], 6,  $sb->request_status, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[7], 6, $sb->approved_by != 0 ? $this->custom_library->get_user_full_name($sb->approved_by) : '', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[8], 6,$sb->approved_on != 0 ? trending_date($sb->approved_on) : '', 1, 0, 'L', $fill);

                $this->fpdf->Ln();
                $fill = !$fill;
                $no++;
            }

            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN ' . humanize($this->page_level2) . ' export ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);


        }

    function variance_reports($type = null)
        {


            $this->sub_head($this->page_level2 . ' Report');
            $this->fpdf->AliasNbPages();
            // $this->fpdf->AddPage();

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'Form#','District', 'Village', 'Variations', 'Resolved', 'Entrant', 'Validator');


#	Form#	Village	Variations	Resolved	Entrant	Validator	Actions



            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 10);

// Header
            $w = array(8, 25, 25, 25, 25, 20, 30, 30);
            for ($i = 0; $i < count($header); $i++)
                $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 8);

// Data
            $fill = false;
            $no = 1;



            $this->db->select('a.form_num,b.village')->from('forms a')
                ->join('vht b', 'a.phone=b.phone')//->join('registration c','a.form_num=c.form_num')
            ;

            $qry = $this->db->order_by('a.id', 'desc')->get()->result();

            foreach ($qry as $sb) {

                //this is the end for the status


                $path = $this->locations->get_path($sb->village);

                $form_variations = $type == 'entry_variance' ? $this->custom_library->entry_variations($sb->form_num) : $this->custom_library->collection_variations($sb->form_num);


                if($form_variations['variance']!='N/A'||$form_variations['variance']!=0){
                $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[1], 6, $sb->form_num, 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[2], 6, $path[1]['name'], 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], 6, $path[4]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[4], 6, $form_variations['variance'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[5], 6, $form_variations['resolved'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[6], 6, $form_variations['entrant'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[7], 6, $form_variations['validation'], 1, 0, 'L', $fill);


                $this->fpdf->Ln();
                $fill = !$fill;
                $no++;
            }
            }

            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN ' . humanize($this->page_level2) . ' export ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);


        }

    function list_forms($vill = null)
        {


            $this->sub_head($this->page_level2 . ' Report');
            $this->fpdf->AliasNbPages();
            // $this->fpdf->AddPage();

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'Form#', 'VHT', 'Entered', '#HH', '#Pop', 'District', 'Village', 'Subcounty', 'Registered', 'Status');


            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 10);

// Header
            $w = array(8, 12, 28, 15, 15, 10, 25, 20, 25, 20, 15);
            for ($i = 0; $i < count($header); $i++)
                $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 8);

// Data
            $fill = false;
            $no = 1;


            if (isset($vill)) {

                $wher = $this->all_villages($vill, 'c.village');

            }


            $this->db->select('a.id,a.form_num,a.phone,a.date_created,c.first_name,c.last_name,c.village,a.status')->from('forms a')->join('vht c', 'a.phone=c.phone');


            isset($wher) ? $this->db->where($wher) : '';

            $qry = $this->db->order_by('a.id', 'asc')->get()->result();

            foreach ($qry as $sb) {



                $path = $this->locations->get_path($sb->village);
                //this is the end for the status

                $form_entered = $this->custom_library->house_holds($sb->form_num) ? $this->custom_library->house_holds($sb->form_num) : array();


                if (count($form_entered) != 0) {

                    $people_in_household = $this->custom_library->people_in_household($form_entered[0]->id) ? $this->custom_library->people_in_household($form_entered[0]->id) : array();


                    $house_holds = $this->db->select_sum('person_vht')->from('reg_detail')->where(array('data_id' => $form_entered[0]->id))->get()->row();

                }
                else {
                    $people_in_household = array();
                }

                $forms_entered=$this->db->where('form_num',$sb->form_num)->from('registration')->count_all_results();


                $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[1], 6, $sb->form_num . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[2], 6, ($sb->first_name . ' ' . $sb->last_name), 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], 6, $forms_entered, 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[4], 6, count($people_in_household), 1, 0, 'L', $fill);
//            $this->fpdf->Cell($w[5], 6,   count($people_in_household) > 0 ? $house_holds->person_vht : '', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[5], 6, count($people_in_household) > 0 ? $house_holds->person_vht : '', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[6], 6, $path[1]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[7], 6, $path[4]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[8], 6, $path[2]['name'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[9], 6, trending_date(strtotime($sb->date_created)), 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[10], 6, humanize($sb->status), 1, 0, 'L', $fill);
                $this->fpdf->Ln();
                $fill = !$fill;
                $no++;
            }

            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN ' . humanize($this->page_level2) . ' export ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);


        }

    function parish_report($vill = null)
        {

            $this->sub_head($this->page_level2);

            $this->fpdf->AliasNbPages();
            // $this->fpdf->AddPage();

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'Sub County', 'Parish', 'Village', 'Households', 'Popn', 'Nets', 'Bales', 'Extra');


            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 10);

// Header
            $w = array(8, 35, 30, 15, 25, 15, 15, 15, 30);
            for ($i = 0; $i < count($header); $i++)
                $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 8);

// Data
            $fill = false;
            $no = 1;


            if (isset($vill)) {

                $wher = $this->all_villages($vill, 'a.village');

            }

            $this->db->select('a.village,b.name,b.parish')->from('registration a')->join('villages b', 'a.village=b.village')->where('confirm', 1);

            isset($wher) ? $this->db->where($wher) : '';

            $parish = $this->db->order_by('a.id', 'desc')->group_by('parish')->get()->result();

            foreach ($parish as $q) {


                $np = $this->custom_library->getting_villages_in_parish($q->parish);


                $path = $this->locations->get_path($q->village);



                $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'C', $fill);
                $this->fpdf->Cell($w[1], 6, $path[2]['name'], 0, 0, 'C', $fill);
                $this->fpdf->Cell($w[2], 6, $path[3]['name'], 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], 6, $np['village_no'], 1, 0, 'C', $fill);
                $this->fpdf->Cell($w[4], 6, $np['house_holds'], 1, 0, 'C', $fill);
                $this->fpdf->Cell($w[5], 6, $np['population'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[6], 6, $np['house_hold_nets'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[7], 6, $np['bails'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[8], 6, $np['extra_bails'], 1, 0, 'L', $fill);
                $this->fpdf->Ln();
                $fill = !$fill;
                $no++;
            }

            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN ' . humanize($this->page_level2) . ' export ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);


        }

    function sub_county_report($vill = null)
        {


            $this->sub_head('Subcounty Report');
            $this->fpdf->AliasNbPages();
            // $this->fpdf->AddPage();

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'Sub County', 'Parish', 'Village', 'Households', 'Popn', 'Nets', 'Bales', 'Extra');


            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 10);

// Header
            $w = array(8, 35, 20, 20, 25, 20, 20, 20, 20);
            for ($i = 0; $i < count($header); $i++)
                $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 8);

// Data
            $fill = false;
            $no = 1;


            if (isset($vill)) {

                $wher = $this->all_villages($vill, 'a.village');

            }

            $this->db->select('a.village,b.name,b.sub_county')->from('registration a')->join('villages b', 'a.village=b.village')->where('confirm', 1);

            isset($wher) ? $this->db->where($wher) : '';

            $sub_county = $this->db->order_by('a.id', 'desc')->group_by('b.sub_county')->get()->result();

            foreach ($sub_county as $q) {

                $np = $this->custom_library->getting_parish_in_sub_county($q->sub_county);


                $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[1], 6, $this->locations->get_location_name($q->sub_county), 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[2], 6, $np['parish_no'], 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], 6, $np['village_no'], 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[4], 6, $np['house_holds'], 1, 0, 'C', $fill);
                $this->fpdf->Cell($w[5], 6, $np['population'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[6], 6, $np['house_hold_nets'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[7], 6, $np['bails'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[8], 6, $np['extra_bails'], 1, 0, 'L', $fill);
                $this->fpdf->Ln();
                $fill = !$fill;
                $no++;
            }

            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN ' . humanize($this->page_level2) . ' report ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);


        }

    function district_report($vill = null)
        {


            $this->sub_head('District Report');
            $this->fpdf->AliasNbPages();
            // $this->fpdf->AddPage();

// Line break
            $this->fpdf->Ln(1);
            $header = array('#', 'District', 'Sub County', 'Parish', 'Village', 'Households', 'Popn', 'Nets', 'Bales', 'Extra');


            $this->fpdf->SetFillColor(152, 152, 152);
            $this->fpdf->SetTextColor(0);
            $this->fpdf->SetDrawColor(240, 240, 245);
            $this->fpdf->SetLineWidth(.3);
            $this->fpdf->SetFont('Arial', 'B', 10);

// Header
            $w = array(8, 25,25, 15, 15, 25, 20, 20, 20, 20, 20);
            for ($i = 0; $i < count($header); $i++)
                $this->fpdf->Cell($w[$i], 6, $header[$i], 0, 0, 'L', true);
            $this->fpdf->Ln();

// Color and font restoration
            $this->fpdf->SetFillColor(240, 240, 245);
            $this->fpdf->SetTextColor(0);

            $this->fpdf->SetFont('Arial', '', 8);

// Data
            $fill = false;
            $no = 1;


            if (isset($vill)) {

                $wher = $this->all_villages($vill, 'a.village');

            }

            $this->db->select('a.village,b.name,a.district')->from('registration a')->join('villages b', 'a.village=b.village')->where('confirm', 1);

            isset($wher) ? $this->db->where($wher) : '';

            $district = $this->db->order_by('a.id', 'desc')->group_by('a.district')->get()->result();

            foreach ($district as $q) {

                $np = $this->custom_library->getting_sub_county_in_district($q->district);


                $this->fpdf->Cell($w[0], 6, $no . '.', 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[1], 6, $this->locations->get_location_name($q->district), 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[2], 6, $np['sub_county_no'], 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[3], 6, $np['parish_no'], 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[4], 6, $np['village_no'], 0, 0, 'L', $fill);
                $this->fpdf->Cell($w[5], 6, $np['house_holds'], 1, 0, 'C', $fill);
                $this->fpdf->Cell($w[6], 6, $np['population'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[7], 6, $np['house_hold_nets'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[9], 6, $np['bails'], 1, 0, 'L', $fill);
                $this->fpdf->Cell($w[10], 6, $np['extra_bails'], 1, 0, 'L', $fill);
                $this->fpdf->Ln();
                $fill = !$fill;
                $no++;
            }

            $this->fpdf->Cell(array_sum($w), 0, '', 'T');

            $file = 'LLIN ' . humanize($this->page_level2) . ' report ' . time() . '.pdf';
            $D = 'D';
            $this->fpdf->Output($file, $D);


        }




}
