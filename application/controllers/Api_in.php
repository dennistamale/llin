<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class api_in extends CI_Controller {


    public $username = "apiuser";
    public $password = "@p1us3r";



    function __construct()
        {
            parent::__construct();

        }

        function index(){
            echo phpinfo();
        }



    function url($type = 'update_forms',$location=null)
        {

            $array = array();
            switch ($type) {

                case 'update_forms':
                    //get records in every ten minutes
                    $time = date("Y-m-d H:i:s", strtotime("-11 min"));

                    //http://llin.gcinnovate.com/api/v1/forms_endpoint/BzcK9Ble3IFT/?from_date=YYYY-MM-DD

                    $url = 'http://llin.gcinnovate.com/api/v1/forms_endpoint/vkdE7Cmm5Kv4/?from_date=' . $time;
                    $array = readJsonAuth($url, $this->username, $this->password, 'url');

                    break;
                case 'update_dp':

                   /// http://llin.gcinnovate.com/api/v1/dpoints_endpoint/<subcounty-code>/

                    $url = "http://llin.gcinnovate.com/api/v1/dpoints_endpoint/$location/";
                    $array = readJsonAuth($url, $this->username, $this->password, 'url');
                    break;

            }

            return $array;


        }

        function update_forms()
        {


            if (count($this->url()) > 0) {




                $forms_data = array();
                $missing_vht=array();

                foreach ($this->url() as $q) {

                    //location Details
                    $location_code = $q->location_code;
                    $location_id = $q->location_id;
                    $location_name = $q->location_name;
                    $location_uuid = $q->location_uuid;



                    //vht Details
                    $firstname = $q->firstname;
                    $lastname = $q->lastname;
                    $telephone = phone($q->telephone);
                    $email = $q->email;
                    $vht_code=$q->reporter_code;

                    //form Details
                    $form_serial = $q->form_serial;
                    $created = $q->created;

                    if ($this->db->where(array('phone' => $telephone))->from('vht')->count_all_results() == 0) {
                        //id, first_name, last_name, code, phone, village, vht_timestamp, created_on, created_by, updated_by, updated_on, location_code

                        $phone_key= array_search(strtolower($telephone), array_column($missing_vht, 'phone'));

                        $values = array(

                            'first_name' => $firstname,
                            'last_name' => $lastname,
                            'code' => strlen($vht_code)>1?$vht_code:1,
                            'phone' => $telephone,
                            'village' => $location_id,
                            'vht_timestamp' => $created,
                            'created_on' => time(),
                            'created_by' => 0,
                            'location_code' => $location_code


                        );

                        $phone_key==0?array_push($missing_vht,$values):'';
                        //$this->db->insert('vht', $values);

                    }


                    if ($this->db->where(array('form_num' => $form_serial))->from('forms')->count_all_results() == 0) {
//                    id, phone, form_num, form_created, date_created, status, updated_by, updated_on, created_by, entry_method, created_on, id, id

                        //cheking it already exist in the array
                       $key= array_search(strtolower($form_serial), array_column($forms_data, 'form_num'));

                        $form = array(
                            'phone' => $telephone,
                            'form_num' => $form_serial,
                            'form_created' => $created,
                            'created_on' => time(),
                            'created_by' => 0,
                            'entry_method' => 'sms'

                        );

                       $key==0?array_push($forms_data, $form):'';
                    }

                }


                //adding the missing VHT
                count($missing_vht)>0?$this->db->insert_batch('vht',$missing_vht):'';

                //inserting the forms

                if (count($forms_data) > 0) {

                    if ($this->db->insert_batch('forms', $forms_data)) {

                        echo 'Data has been inserted successfully';
                    }else{
                        echo 'An error has occurred while inserting data';
                    }
                }
                else {
                    echo 'No Data to insert';
                }


            }else{
                echo 'No data found !!!!';
            }
        }


    function test(){
        $dd=$this->url('update_dp');
        $d=$dd[0];


        $villages=$d->villages;
        $code=$d->code;
        $id=$d->id;
        $name=$d->name;

        $vill=$villages[0];

        $vill->name;
        $vill->code;
    }


    function update_dp($location_code='PrqCCftLMvRf')
        {


            if (count($this->url('update_dp',$location_code)) > 0) {



                foreach ($this->url('update_dp',$location_code) as $q) {

                     $subcounty_code = $q->code;

                    $dp_name = $q->name;


                    $dp_exist = $this->db->where(array('dp_code' => $subcounty_code))->from('distribution_points')->count_all_results();




                    if ($dp_exist == 0) {
                        //location Details

                        $subcounty_id = $this->db->select('id')->from('locations')->where(array('code' => $location_code))->get()->row();

                        $subcounty_id = count($subcounty_id) == 1 ? $subcounty_id->id : 0;

                        $values = array(
                            'point_name' => $dp_name,
                            'dp_code' => $q->code,
                            'subcounty' => $subcounty_id,
                            'created_on' => time()


                        );

                        if ($this->db->insert('distribution_points', $values)) {
                            $dp = $this->db->select('id')->from('distribution_points')->where(array('dp_code' => $subcounty_code))->get()->row();

                            if (count($dp) == 1) {
                                $dp_villages = array();
                                foreach ($q->villages as $village) {

                                    $village_code = $village->code;
                                    $village_c = $this->db->select('id')->from('locations')->where(array('code' => $village_code))->get()->row();
                                    $village_id = count($village_c) == 1 ? $village_c->id : 0;

                                    $dp_village = array(
                                        'village_code' => $village_code,
                                        'village' => $village_id,
                                        'village' => $village_id,
                                        'distribution_point' => $dp->id,
                                        'created_on'=>time()
                                    );
                                    array_push($dp_villages,$dp_village);
                                }

                                //adding dp _villages
                                count($dp_villages)>0?$this->db->insert_batch('distribution_point_villages',$dp_villages):'';
                            }

                        }


                    }


                }
            }
            else {
                echo 'No data found !!!!';
            }

        }


}