<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('HEAD', 'Head');
define('SUBHEAD', 'Print Date : '.date('d-m-Y H:i:s'));
define('LOGO', 'uploads/site_logo/coart-of-arms.png');
define('ORIENTATION',isset($orientation)?$orientation:'P');

//error_reporting(0);
class Fpdf_test extends CI_Controller {

	/**
	 * Example: FPDF 
	 *
	 * Documentation: 
	 * http://www.fpdf.org/ > Manual
	 *
	 */

    function __construct()
        {
            parent::__construct();
//            LOGO = base_url($this->site_options->title('site_logo'));
        }

	public function index() {	
		$this->load->library('fpdf_gen');
		
		$this->fpdf->SetFont('Arial','B',16);
		$this->fpdf->Cell(40,10,'Hello World!');

//        $this->fpdf->AddPage();
//        $this->fpdf->AddPage();
		echo $this->fpdf->Output('hello_world.pdf','I');
//        $this->fpdf->AddPage();
//		echo $this->fpdf->Output('hello_world.pdf','I');
	}
}
