<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/html">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title><?php echo humanize($title) ?> | <?php echo $this->site_options->title('site_name') ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="Dennis Tamale(tamaledns@gmail.com)" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo base_url() ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?php echo base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<?php echo base_url() ?>assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/layouts/layout2/css/themes/dark.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo base_url() ?>assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="<?php echo base_url($this->site_options->title('site_logo')) ?>" /> </head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid <?php //echo $title=='house_hold_reg'?'page-sidebar-closed':'' ?>" cz-shortcut-listen="true">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <?php  $them_color=$this->site_options->title('site_color_code'); ?>
        <style>
            .theme-color{
                background-color: <?php echo $them_color; ?> !important;
            }
        </style>
        <div class="page-logo theme-color">
            <a href="<?php echo base_url('index.php/'.$this->page_level)?>" style="text-decoration: none; font-weight: bold;">
<!--               <h4> --><?php //echo $this->site_options->title('site_name') ?><!--</h4>-->
                <img src="<?php echo base_url($this->site_options->title('site_logo')) ?>" alt="logo" class="logo-default" height="40" />
            </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN PAGE TOP -->
        <div class="page-top theme-color">
            <!-- BEGIN HEADER SEARCH BOX -->
            <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
            <form  class="hidden search-form search-form-expanded" action="page_general_search_3.html" method="GET">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search..." name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                </div>
            </form>

            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu" style="background-color:rgba(122,9,9,0.1) !important;">
                <ul  class="nav navbar-nav pull-right">
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-notification hidden" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-default"> 7 </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>
                                    <span class="bold">12 pending</span> notifications</h3>
                                <a href="page_user_profile_1.html">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">just now</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span> New user registered. </span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END NOTIFICATION DROPDOWN -->
                    <!-- BEGIN INBOX DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-inbox hidden" id="header_inbox_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-envelope-open"></i>
                            <span class="badge badge-default"> 4 </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>You have
                                    <span class="bold">7 New</span> Messages</h3>
                                <a href="app_inbox.html">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                    <li>
                                        <a href="#">
                                                    <span class="photo">
<!--                                                        <img src="--><?php //echo base_url() ?><!--assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> -->
                                                    </span>
                                                    <span class="subject">
                                                        <span class="from"> Lisa Wong </span>
                                                        <span class="time">Just Now </span>
                                                    </span>
                                            <span class="message"> Vivamus sed  auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END INBOX DROPDOWN -->
                    <!-- BEGIN TODO DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-tasks hidden" id="header_task_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-calendar"></i>
                            <span class="badge badge-default"> 3 </span>
                        </a>
                        <ul class="dropdown-menu extended tasks">
                            <li class="external">
                                <h3>You have
                                    <span class="bold">12 pending</span> tasks</h3>
                                <a href="app_todo.html">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                    <li>
                                        <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">New release v1.2 </span>
                                                        <span class="percent">30%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">40% Complete</span>
                                                        </span>
                                                    </span>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="<?php echo base_url($this->session->userdata('photo')) ?>" />
                            <span class="username username-hide-on-mobile"> <?php echo ucwords($this->session->userdata('first_name')); ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>

                                <?php echo anchor($this->page_level.'profile','<i class="icon-user"></i> My Profile'); ?>
                            </li>


                            <li class="divider">
                            </li>

                            <li>

                                <?php echo anchor($this->page_level.'logout','<i class="icon-key"></i> Logout'); ?>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->

                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('static/page-sidebar-wrapper') ?>

    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->

            <h3 class="page-title hidden"> Ajax Datatables
                <small>basic datatable samples</small>
            </h3>
            <div class="page-bar <?php echo $subtitle=='registration_map'||$subtitle=='distribution_map'||$subtitle=='map_view'?'hidden':'' ?>">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <?php echo anchor($this->uri->segment(1),'Home') ?>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <?php echo strlen($title)>0? anchor($this->page_level.$title,humanize($title)):''; ?>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>

                        <?php echo humanize($subtitle); ?>
                        <?php echo strlen($subtitle)>0?' <i class="fa fa-angle-right"></i>':''; ?>
                    </li>
                    <li>
                        <?php echo humanize($this->uri->segment(4)); ?>
                    </li>
                </ul>
                <div class="page-toolbar" style="padding-top: 5px; padding-right :5px;">
                    <div class="btn-group pull-right">


                        <?php if($title=='dashboard'||$title=='reports'){ ?>

                            <div>

                                <?php //echo humanize($this->session->userdata('branch_name')); ?>
                                <?php echo form_open() ?>
                                <!--                            <label for="topbar-multiple" class="control-label pr10 fs11 text-muted">Reporting Period --><?php ////echo $fdate ?><!--  --><?php ////echo ' : last day'. isset($last_day)?$last_day:''; ?><!--</label>-->
                                <!--                    multiple selection has been removed multiple="multiple"-->
                                <select style="background: rgba(255,255,255,0.2)" name="filter" class="form-control input-small input-sm hidde" onchange="this.form.submit()" >
                                    <optgroup label="Filter By:">
                                        <option value="8    " <?php echo set_select('filter', 8,true); ?> >This Year</option>
                                        <option value="7" <?php echo set_select('filter', 7); ?> >This Week</option>
                                        <option value="1" <?php echo set_select('filter', 1); ?> >This Month</option>
                                        <option value="2" <?php echo set_select('filter', 2); ?> >Last 30 Days</option>
                                        <option value="3" <?php echo set_select('filter', 3); ?>>Last 60 Days</option>
                                        <option value="4" <?php echo set_select('filter', 4); ?>>Last 90 Days</option>
                                        <option value="5" <?php echo set_select('filter', 5); ?>>Last Month</option>
                                        <option value="6" <?php echo set_select('filter', 6); ?>>Last Year</option>
                                    </optgroup>
                                </select>

                                <?php form_close() ?>


                            </div>

                        <?php  }else{ ?>

                            <div id="time" class="text-success" align="center">

                                <script type="text/javascript">
                                    var int=self.setInterval("clock()",10);
                                    function clock() {
                                        var d=new Date();
                                        var t=d.toLocaleTimeString();
                                        var m=new Date();
                                        var y=m.toDateString();
                                        document.getElementById("time").innerHTML=t+" &nbsp;"+y;

                                    }
                                </script>

                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
            <!-- END PAGE HEADER-->

<!--            --><?php //var_dump($this->custom_library->role_exist('allocation plan','group')) ?>
<!---->
<!--            --><?php //var_dump($this->custom_library->role_exist('vht ','group')) ?>
<!--<hr/>-->
<!--            --><?php
//
           // print_r($array=$this->session->permission);


            //var_dump($this->custom_library->any_role());
            //echo $this->custom_library->role_exist('dashboard','group')?'Yes':'No';

           //echo date("Y-m-d H:i:s", strtotime("-11 min"))
            ?>





