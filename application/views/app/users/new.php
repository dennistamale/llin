<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<link href="--><?php //echo base_url() ?><!--assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />-->
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

<div class="row">

    <div class="col-md-12 ">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title hidden">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase">New User</span>
                </div>
                <div class="actions hidden">

                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body form">

                <?php echo form_open('') ?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input has-success">
                                        <select class="form-control" name="role" id="role"  required >

                                            <option value="" <?php echo set_select('role', '', TRUE); ?> >Select User Role</option>
                                            <?php foreach($this->db->select('id,title')->from('user_type')->order_by('id','asc')->get()->result() as $role): ?>
                                                <option value="<?php echo $role->id ?>" <?php echo set_select('role', $role->id); ?> ><?php echo $role->title ?></option>
                                            <?php  endforeach; ?>



                                        </select>
                                        <label for="form_control_1">User Role</label>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group  form-md-radios margin-top-20"  id="gender">
                                        <label class="col-md-2 control-label  margin-top-10">Gender </label>
                                        <div class="md-radio-inline  col-md-10">
                                            <div class="md-radio">
                                                <input type="radio" id="radio6" name="gender" value="M" <?php echo set_radio('gender','M'); ?> class="md-radiobtn">
                                                <label for="radio6">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Male </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="radio7" name="gender" value="F" <?php echo set_radio('gender','F'); ?> class="md-radiobtn">
                                                <label for="radio7">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Female </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>

</div>

                            <div class="row">


                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" name="first_name" value="<?php echo set_value('first_name') ?>">
                                        <label for="form_control_1">First Name <?php echo form_error('first_name','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="text" class="form-control" name="last_name" value="<?php echo set_value('last_name') ?>">
                                        <label for="form_control_1">Last Name <?php echo form_error('last_name','<span style=" color:red;">','</span>') ?></label>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="email" class="form-control" name="email" value="<?php echo set_value('email') ?>" id="form_control_1">
                                        <label for="form_control_1">Email  <?php echo form_error('email','<span style=" color:red;">','</span>') ?></label>
                                        <span class="help-block">Add subscribers Email</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input form-md-floating-label">
                                        <input type="phone" class="form-control" name="phone" value="<?php echo set_value('phone') ?>" id="form_control_1">
                                        <label for="for m_control_1">Phone  <?php echo form_error('phone','<span style=" color:red;">','</span>') ?></label>
                                        <span class="help-block">Add subscribers phone</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input">
                                        <div class="input-group" >
                                            <input class="form-control date date-picker" value="<?php echo set_value('dob') ?>" name="dob"  size="16" data-date-format="yyyy-mm-dd" type="text"/>
                                <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>

                                            <label for="form_control_1"> Date of birth <?php echo form_error('dob','<span style=" color:red;">','</span>') ?></label>

                                        </div>

                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group form-md-line-input has-success">
                                        <select class="form-control" name="country" id="country"><?php echo form_error('country','<label class="text-danger">','<label>') ?>
                                            <option value="" <?php echo set_select('country', ''); ?>>Select Country...</option>
                                            <?php foreach($this->db->select('country,a2_iso')->from('country')->get()->result() as $tg): ?>
                                                <option value="<?php echo $tg->a2_iso ?>" <?php echo set_select('country', $tg->a2_iso,$tg->a2_iso=='UG'?true:''); ?>><?php echo $tg->country; ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                        <label for="form_control_1">Select Country</label>
                                    </div>
                                </div>

                                <div class="col-md-6" hidden>
                                    <div class="form-group form-md-line-input has-success"><?php echo form_error('city','<label style="color: red;">','<label>') ?>
                                        <select class="form-control" name="city" id="city">
                                            <option value="" <?php echo set_select('city', '',true); ?>>Select City...</option>

                                        </select>
                                        <label for="form_control_1">Select City</label>
                                    </div>
                                </div>







                            </div>

                        </div>




                    </div>



                </div>
                <div class="form-actions">
                    <button type="submit"  class="btn blue"><i class="icon-plus"></i> Add User</button>

                    <button type="reset" class="btn default">Cancel</button>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>
</div>

<?php $this->load->view('ajax/get_city'); ?>
<?php $this->load->view('ajax/get_users'); ?>


<script>
    $("#user_type").change(function(){
        var ut=$("#user_type").val();
        if(ut==1){
            //$("#gender").removeClass( "hidden" );
            $("#gender").slideDown();
            // alert('');
        }else{
            $("#gender").slideUp();
        }

    });

</script>


<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>



<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
