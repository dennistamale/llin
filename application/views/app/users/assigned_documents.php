<?php $user_id=$prof->id;
 $this->db->select('a.*,b.title,b.document')->from('document_assignment a')->join('documents b','a.document_id=b.id');
    $this->db->where(array('a.assigned_to' => $user_id));
$docs = $this->db->get()->result();
?>

<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">

            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="2">#</th>
                        <th>Documenet&nbsp;Title</th>

                        <th> Document </th>

                        <th> Status </th>
                        <th> Comment </th>

                        <th> Assigned on </th>

                        <th> Completed on </th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php


                    $no=1;
                    foreach($docs as $d): ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td>
                            <?php echo ucfirst( $d->title) ?>
                        </td>

                        <td>

                            <?php  echo (strlen($d->document)>0?'<a href="'.base_url($d->document).'" target="_blank" class="btn btn-sm btn-info pull-right"><i class="fa fa-download"></i> Download</a>':'') ?>
                        </td>

                        <td>
                            <?php echo $d->status ?>
                        </td>

                        <td>
                            <?php echo $d->comment ?>
                        </td>


                        <td> <?php
                          echo  trending_date($d->assigned_on)
                            ?> </td>

                        <td> <?php
                           echo trending_date($d->completed_on)
                            ?> </td>


                        <td style="width: 80px;">
<!--                            <div class="btn-group">-->
<!--                                <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">-->
<!--                                    <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>-->
<!--                                </a>-->
<!--                                <ul class="dropdown-menu pull-right">-->
<!--                                    <li>-->
<!--                                        --><?php //echo anchor($this->page_level.$this->page_level2.'edit/'.$user->id*date('Y'),'  <i class="fa fa-pencil"></i> Edit') ?>
<!--                                    </li>-->
<!--                                    --><?php //if($this->session->userdata('user_type')!='2'){ ?>
<!--                                    <li >-->
<!---->
<!--                                        --><?php //echo anchor($this->page_level.$this->page_level2.'delete/'.$user->id*date('Y'),'  <i class="fa fa-trash-o"></i> Delete','onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
<!--                                    </li>-->
<!--                                    <li>-->
<!---->
<!--                                        --><?php //echo $user->status==2? anchor($this->page_level.$this->page_level2.'unblock/'.$user->id*date('Y'),'  <i class="fa fa-check"></i> Unblock'): anchor($this->page_level.$this->page_level2.'ban/'.$user->id*date('Y'),'  <i class="fa fa-ban"></i> Ban' ,'onclick="return confirm(\'You are about to ban User from accessing the System \')"') ?>
<!--                                    </li>-->
<!--                                    <li class="divider">-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        --><?php //echo anchor($this->page_level.$this->page_level2.'make_admin/'.$user->id*date('Y'),'  <i class="i"></i> Make admin') ?>
<!--                                    </li>-->
<!--                                    --><?php //} ?>
<!--                                </ul>-->
<!--                            </div>-->
            </td>
                    </tr>
                    <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

