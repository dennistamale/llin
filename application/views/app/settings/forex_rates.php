<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-users font-dark"></i>
                    <span class="caption-subject bold uppercase">Forex Rates&nbsp;</span>
                </div>
                <div class="actions pull-left">

            <?php echo anchor($this->page_level.$this->page_level2.'add_forex_rate',' <i class="fa fa-plus"></i> Add Forex Rate','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="1" hidden></th>
                        <th> From </th>
                        <th> To </th>
                        <th> Amount </th>

                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $nc=1;
                    foreach($this->db->select('country,a2_iso')->from('selected_countries')->get()->result() as $countries): ?>
                    <tr style="background: rgba(156, 255, 156,0.5); font-weight: bold;">

                        <td hidden></td><td style="border-right: none;"><?php echo $countries->country ?></td><td style="border-right: none;"></td><td style="border-right: none;"></td><td style="border-right: none;"></td>
                    </tr>

<!--                        this is the forex for the various countries-->

                        <?php
                        $no=1;
                        $fx=$this->db->select()->from('forex_settings')->where('country',$countries->a2_iso)->get()->result();
                        if(isset($fx[0]->id))
                        {
                            foreach ($fx as $c): ?>
                                <tr>
                                    <td hidden><?php //echo $no; ?></td>
                                    <td> <?php
                                        $from = $this->db->select('currency_name')->from('country')->where('currency_alphabetic_code', $c->from_currency)->get()->row();
                                        echo $c->from_currency . ' (' . $from->currency_name . ')'; ?> </td>

                                    <td> <?php
                                        $to = $this->db->select('currency_name')->from('country')->where('currency_alphabetic_code', $c->to_currency)->get()->row();
                                        echo $c->to_currency . ' (' . $to->currency_name . ')'; ?> </td>
                                    <td> <?php echo number_format($c->to_amount,4) ?> </td>

                                    <td>
                                        <div class="btn-group">
                                            <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <?php echo anchor($this->page_level . $this->page_level2 . 'edit_exchange_rate/' . $c->id * date('Y'), '  <i class="fa fa-edit"></i> Edit'); ?>
                                                </li>
                                                <li>

                                                    <?php echo anchor($this->page_level . $this->page_level2 . 'delete_exchange_rate/' . $c->id * date('Y'), '  <i class="fa fa-trash-o"></i> Delete', 'onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                                </li>


                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $no++;
                            endforeach;
                        }
                        else{ ?>

                            <tr style="border: none; ">

                                <td hidden></td><td style="border-right: none;">No Forex-rate for <?php echo $countries->country ?></td><td style="border-right: none;"></td><td style="border-right: none;"></td><td style="border-right: none;"></td>
                            </tr>

                        <?php }
                        ?>


                    <?php $nc++;  endforeach; ?>



                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->