<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/multiselect/css/multi-select.css">

<?php //$this->load->view($this->page_level.$this->page_level2.'select');?>



<?php
$selected_entrants='';
if(isset($data_en)) {
    if(count($data_en)){
        // print_r($data_en);
        $selected_entrants = " AND (";
        $got = count($data_en);
        $n = 0;

        foreach ($data_en as $d) {


            $selected_entrants .= " id =$d";

            $n++;
            $n != $got ? $selected_entrants .= " OR" : "";
        }
        $selected_entrants .= " )";
    }

}


?>


<?php


//$last_day= strtotime('friday this week');

$begin = new DateTime( date('Y-m-d',$fdate) );
$end = new DateTime( date('Y-m-d',$last_day) );
//$ldate=$last_day

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end->modify( '+1 day' ));

//print_r($period);

?>


<div class="row">


    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">

                <div class="caption  font-dark">
<!--                    Range From --><?php //echo date('d-m-Y',$fdate). ' to '. date('d-m-Y',$last_day) ?>
                    <?php // echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn green-jungle"'); ?>


<!--                    <form class="form-inline" role="form">-->
                        </form>
                    <?php echo form_open('', 'class="form-inline hidden-print"') ?>

                    <div class="form-group">

                        <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <span class="input-group-addon">From </span>
                            <input type="text" class="form-control" name="from" placeholder="<?php echo date('Y-m-d',$fdate) ?>" value="<?php echo set_value('from') ?>">
                            <span class="input-group-addon">to </span>
                            <input type="text" class="form-control" name="to" placeholder="<?php echo date('Y-m-d',$last_day)?>" value="<?php echo set_value('to') ?>">
                        </div>

                        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title sbold red">Select Senders</h4>
                                    </div>
                                    <div class="modal-body">

                                        <?php


                                        $this->db->select('id,sender');
                                        $this->db->from('sender_accounts');
                                        $rep=$this->db->get()->result();

                                        //            print_r($rep);


                                        ?>

                                        <?php


                                        foreach($rep as $t){
                                            $options[$t->id] = $t->sender;
                                        }

                                        $attr=array(
                                            'id'=>'pre-selected-options'
                                        );
                                        //                        id="pre-selected-options"
                                        echo form_multiselect('data_entrants[]', $options,'',' id="pre-selected-options"');
                                        ?>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Select</button>
                                        <!--                                        <button type="button" class="btn green">Save changes</button>-->
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

                        <select  class="hidden table-group-action-input form-control input-medium" name="month">
                            <option value="" <?php echo set_select('month', ''); ?>>Select month...</option>
                            <?php for($i=1;$i<=12;$i++){ ?>
                                <option value="<?php echo $i ?>" <?php echo set_select('month', $i,date('n')==$i?true:''); ?>><?php echo monthName($i-1); ?></option>
                            <?php } ?>

                        </select>

                    </div>
                    <a class="btn yellow-gold btn-outline sbold" data-toggle="modal" href="#basic"><i class="fa fa-users"></i> Accounts </a>

                    <button type="reset" class="btn red"><i class="fa fa-refresh"></i> Reset</button>
                    <button type="submit" class="btn green"><i class="fa fa-sliders"></i> Filter</button>



                    <?php echo form_close(); ?>






                </div>

                <div class="tools hidden-print">
                    <?php echo anchor($this->page_level.$this->page_level2.'update_accounts','Update Account','class="blue tooltips"'); ?>
<!--                    <a class=" yellow-gold" onclick="javascript:window.print();"> Print-->
                        <i class="fa fa-print"></i>
                    </a>

                    <a href="javascript:;" class="collapse"> </a>

                    <a href="javascript:;" data-load="true" data-url="<?php echo base_url('index.php/ajax_api/'.$title.'/'.$fdate.'/'.$last_day) ?>" class="reload"> </a>
                    <a href="javascript:;" class="fullscreen"> </a>

                </div>
            </div>
            <div class="portlet-body portlet-empty"> Filter Your data and press the button to load data. </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>




</div>



<!--<script src="--><?php //echo base_url() ?><!--assets/global/plugins/jquery.min.js" type="text/javascript"></script>-->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/global/scripts/app.min.js" type="text/javascript"></script>-->
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--<script src="--><?php //echo base_url() ?><!--assets/pages/scripts/portlet-ajax.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL SCRIPTS -->



<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->


<script src="<?php echo base_url() ?>assets/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript">
    // run pre selected options
    $('#pre-selected-options').multiSelect();

    //    $('#dataen').click(function(){
    //
    //        var p=$('#entrants_select');
    ////        alert('');
    //        if((p).is(":hidden"))
    //        {
    //            p.slideDown();
    //        }else{
    //            p.slideUp();
    //        }
    //
    //    });
</script>

<!--<script src="--><?php //echo base_url() ?><!--assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL SCRIPTS -->






