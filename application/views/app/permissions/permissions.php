
<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">


                <div class="caption font-dark hidden-print">


                    <?php echo form_open('','class="form-inline" ')   ?>
                    <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn btn-sm green-jungle"'); ?>
                    <div class="form-group hidden">
                        <div class="">

                            <div class="input-group">
                                <select class="form-control input-sm" name="status" >
                                    <option value="" <?php echo set_select('status','',true) ?>>Select Status</option>
                                    <option value="1" <?php echo set_select('status','1') ?>>Active</option>
                                    <option value="2" <?php echo set_select('status','2') ?>>Blocked</option>

                                </select>
                            </div>
                            <button class="btn green btn-sm" type="submit"><i class="fa fa-sliders"></i> Apply</button></div>
                    </div>

                    <?php echo form_close(); ?>

                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="2">#</th>
                        <th style="width: 20%"> Title </th>
                        <th style="width: 20%"> Group </th>
                        <th style="width: 60%"> Description </th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php


                    $permissions=$this->db->select()->from('permissions')->get()->result();
                    $no=1;
                    foreach($permissions as $t): ?>
                    <tr>
                        <td><?php echo $no; ?></td>

                        <td style="white-space: nowrap">

                            <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$t->id*date('Y'),$t->title) ?>
                        </td>
                        <td><?php echo $t->perm_group ?></td>
                        <td></td>

                        <td style="width: 80px;">
                            <div class="btn-group">
                                <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$t->id*date('Y'),'  <i class="fa fa-eye"></i> View') ?>
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$t->id*date('Y'),'  <i class="fa fa-pencil"></i> Edit') ?>
                                    </li>

                                </ul>
                            </div></td>
                    </tr>
                    <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

