
<style>

    /* ========================================================================
     * RECENT ACTIVITY
     * ======================================================================== */
    .recent-activity {
        margin: 0;
        padding: 0;
        position: relative;
        margin-bottom: 30px;
    }
    .recent-activity > h3 {
        margin-top: 0;
        font-size: 20px;
        font-weight: 300;
    }

    .recent-activity-item {
        position: relative;
        margin: 0;
        padding: 0;
    }
    .recent-activity-item:before {
        content: "";
        position: absolute;
        display: block;
        width: 3px;
        background: #e6e6e8;
        top: 0px;
        bottom: -30px;
        margin-left: 8px;
    }
    .recent-activity-item.recent-activity-danger:before {
        background: #f6bbb1;
    }
    .recent-activity-item.recent-activity-danger .recent-activity-badge-userpic {
        border: 3px solid #f6bbb1;
    }
    .recent-activity-item.recent-activity-success:before {
        background: #cae3b0;
    }
    .recent-activity-item.recent-activity-success .recent-activity-badge-userpic {
        border: 3px solid #cae3b0;
    }
    .recent-activity-item.recent-activity-primary:before {
        background: #62ddff;
    }
    .recent-activity-item.recent-activity-primary .recent-activity-badge-userpic {
        border: 3px solid #62ddff;
    }
    .recent-activity-item.recent-activity-info:before {
        background: #d3f3f9;
    }
    .recent-activity-item.recent-activity-info .recent-activity-badge-userpic {
        border: 3px solid #d3f3f9;
    }
    .recent-activity-item.recent-activity-warning:before {
        background: #fce7bc;
    }
    .recent-activity-item.recent-activity-warning .recent-activity-badge-userpic {
        border: 3px solid #fce7bc;
    }
    .recent-activity-item.recent-activity-lilac:before {
        background: #c6abc9;
    }
    .recent-activity-item.recent-activity-lilac .recent-activity-badge-userpic {
        border: 3px solid #c6abc9;
    }
    .recent-activity-item.recent-activity-teals:before {
        background: #93dfcc;
    }
    .recent-activity-item.recent-activity-teals .recent-activity-badge-userpic {
        border: 3px solid #93dfcc;
    }
    .recent-activity-item.recent-activity-inverse:before {
        background: #6a6a6a;
    }
    .recent-activity-item.recent-activity-inverse .recent-activity-badge-userpic {
        border: 3px solid #6a6a6a;
    }
    .recent-activity-item.recent-activity-last:before {
        content: initial;
    }

    .recent-activity-badge {
        float: left;
        position: relative;
        padding-right: 20px;
        height: 20px;
        width: 20px;
    }

    .recent-activity-badge-userpic {
        width: 20px;
        height: 20px;
        content: "";
        display: block;
        border: 3px #F3F3F4 solid;
        background-color: #F3F3F4;
        -moz-border-radius: 50% !important;
        -webkit-border-radius: 50%;
        border-radius: 50% !important;
    }

    .recent-activity-body {
        position: relative;
        padding: 0;
        margin-top: 10px;
        margin-left: 30px;
    }

    .recent-activity-body:before, .recent-activity-body:after {
        content: " ";
        display: table;
    }

    .recent-activity-body:after {
        clear: both;
    }

    .recent-activity-body-head {
        margin-bottom: 10px;
    }

    .recent-activity-body-title {
        font-size: 15px;
        font-weight: 600;
        margin-top: 0;
        margin-bottom: 0;
        text-transform: capitalize;
    }

    .recent-activity-body-content {
        font-size: 13px;
        margin-top: 10px;
    }
    .recent-activity-body-content p:last-child {
        margin-bottom: 0;
    }

    .text-muted {
        color: #B0ADAD;
    }

    @media (max-width: 768px) {
        .recent-activity-body-head-caption {
            width: 100%;
        }
    }
</style>

<!--/ END THEME STYLES -->




<div class="row">

    <div class="col-md-4">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bar-chart"></i>Recent Form Entries</div>
                <div class="actions">
                    <div class="btn-arrow-link pull-right">
                        <a style="text-decoration: none;" href="#<?php echo base_url()?>">See All <i class="icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div  class="table-responsive scroller" style="height:275px;">
                    <table class="table table-striped table-hover  table-light">

                        <thead>
                        <th>Form</th>
                        <th>Entrant</th>
                        <th>Date</th>
                        </thead>
                        <tbody id="form_entries">  </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>



    <div class="col-md-4">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bar-chart"></i>Recent Nets Picked</div>
                <div class="actions">
                    <div class="btn-arrow-link pull-right">
                        <a style="text-decoration: none;" href="#<?php echo base_url()?>">See All <i class="icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div  class="table-responsive scroller" style="height:275px;">
                    <table class="table table-striped table-hover  table-light">


                        <thead>
                        <th>House Hold</th>
                        <th>Nets</th>
                        <th>Date</th>
                        </thead>

                        <tbody id="recent_nets_picked">  </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>



    <div class="col-md-3 hidden">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bar-chart"></i>Recent Inventory</div>
            </div>
            <div class="portlet-body">
                <div  class="table-responsive scroller" style="height:275px;">
                    <table class="table table-striped table-hover  table-light">


                        <thead>
                        <th>Code</th>
                        <th>Status</th>
                        <th>Date</th>
                        </thead>

                        <tbody id="inventory">  </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>





    <div class="col-md-4">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-share"></i>
                    <span class="caption-subject">Recent Activities</span>
                </div>

                <div class="actions">
                    <div class="btn-arrow-link pull-right">
                        <a style="text-decoration: none;" href="#<?php echo base_url()?>">See All <i class="icon-arrow-right"></i></a>
                    </div>
                </div>

            </div>
            <div  class="table-responsive scroller" style="height:285px;">

                <div class="recent-activity">
                    <!--            <h3>Recent Activity</h3>-->
                    <?php
                    $tlogs=$this->db->select('a.*,b.first_name,b.last_name')->from('logs a')->join('users b','a.created_by=b.id','left')->order_by('id','desc')->limit(10)->get()->result();
                    foreach($tlogs as $tl ): ?>
                        <!-- Start recent activity item -->
                        <div class="recent-activity-item recent-activity-primary">
                            <div class="recent-activity-badge">
                                <span class="recent-activity-badge-userpic"></span>
                            </div>
                            <div class="recent-activity-body">
                                <div class="recent-activity-body-head">
                                    <div class="recent-activity-body-head-caption">
                                        <h3 class="recent-activity-body-title"><?php echo humanize($tl->transaction_type) ?></h3>
                                    </div>
                                </div>
                                <div class="recent-activity-body-content">
                                    <p>
                                        <a href="#"> <?php echo humanize($tl->first_name.' '.$tl->last_name.' ') ?></a>  <?php echo $tl->details ?>
                                        <b><?php echo $tl->target ?></b>
                                        <span class="text-block text-muted">on <?php echo trending_date($tl->created_on) ?></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End recent activity item -->

                    <?php endforeach; ?>

                </div>




            </div>
        </div>
    </div>


</div>

