<!--<script src="--><?php //echo base_url() ?><!--assets/global/plugins/jquery.min.js" type="text/javascript"></script>-->

<script src="<?php echo base_url() ?>assets/highcharts/highcharts.js"></script>

<!--<script src="https://code.highcharts.com/highcharts-3d.js"></script>-->

<script src="<?php echo base_url() ?>assets/highcharts/highcharts-3d.js"></script>

<!--<script src="--><?php //echo base_url() ?><!--assets/highcharts/exporting.js"></script>-->
<!--<script src="--><?php //echo base_url() ?><!--assets/highcharts/themes/grid.js"></script>-->
<?php
$begin = new DateTime( $fdate );
$ldate=isset($last_day)?date($last_day):date('Y-m-d');
$end = new DateTime( $ldate );

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end->modify( '+1 day' ));


?>



<script>
    var Dashboard = function() {

        return {



            initSparklineCharts: function() {
                if (!jQuery().sparkline) {
                    return;
                }
                $("#sparkline_bar").sparkline([8, 9, 10, 11, 10, 10, 12, 10, 10, 11, 9, 12, 11, 10, 9, 11, 13, 13, 12], {
                    type: 'bar',
                    width: '100',
                    barWidth: 5,
                    height: '55',
                    barColor: '#f36a5b',
                    negBarColor: '#e02222'
                });

                $("#sparkline_bar2").sparkline([9, 11, 12, 13, 12, 13, 10, 14, 13, 11, 11, 12, 11, 11, 10, 12, 11, 10], {
                    type: 'bar',
                    width: '100',
                    barWidth: 5,
                    height: '55',
                    barColor: '#5c9bd1',
                    negBarColor: '#e02222'
                });

                $("#sparkline_bar5").sparkline([8, 9, 10, 11, 10, 10, 12, 10, 10, 11, 9, 12, 11, 10, 9, 11, 13, 13, 12], {
                    type: 'bar',
                    width: '100',
                    barWidth: 5,
                    height: '55',
                    barColor: '#35aa47',
                    negBarColor: '#e02222'
                });

                $("#sparkline_bar6").sparkline([9, 11, 12, 13, 12, 13, 10, 14, 13, 11, 11, 12, 11, 11, 10, 12, 11, 10], {
                    type: 'bar',
                    width: '100',
                    barWidth: 5,
                    height: '55',
                    barColor: '#ffb848',
                    negBarColor: '#e02222'
                });

                $("#sparkline_line").sparkline([9, 10, 9, 10, 10, 11, 12, 10, 10, 11, 11, 12, 11, 10, 12, 11, 10, 12], {
                    type: 'line',
                    width: '100',
                    height: '55',
                    lineColor: '#ffb848'
                });
            },


            init: function() {
                this.initSparklineCharts();

            }
        };

    }();

    if (App.isAngularJsApp() === false) {
        jQuery(document).ready(function() {
            Dashboard.init(); // init metronic core componets
        });
    }
</script>