
<div class="row">
    <div class="col-lg-7 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bar-chart"></i>Variances
                </div>
                <div class="actions">
                    <div class="btn-arrow-link pull-right">
                        <a style="text-decoration: none;" href="#<?php echo base_url()?>">See All <i class="icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row number-stats margin-bottom-30">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="stat-left">
                            <div class="stat-chart">
                                <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                <div id="sparkline_bar"></div>
                            </div>
                            <div class="stat-number">
                                <div class="title"> Total </div>
                                <div class="number"> 2460 </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="stat-right">
                            <div class="stat-chart">
                                <!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->
                                <div id="sparkline_bar2"></div>
                            </div>
                            <div class="stat-number">
                                <div class="title"> New </div>
                                <div class="number"> 719 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-scrollable table-scrollable-borderless">
<!--                    table table-striped table-bordered table-hover resource_persons-->
                    <table class="table table-hover table-light resource_persons">
                        <thead>
                        <tr class="uppercase">
                            <th> Variance Report </th>
                            <th> Collection </th>
                            <th> Entry </th>
                            <th> Total </th>

                        </tr>
                        </thead>

                        <tbody id="variance_report">

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-5 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bar-chart"></i>Data Entrants
                </div>
                <div class="actions">
                    <div class="btn-arrow-link pull-right">
                        <a style="text-decoration: none;" href="#<?php echo base_url()?>">See All <i class="icon-arrow-right"></i></a>
                    </div>
                </div>

            </div>
            <div class="portlet-body">
                <div class="scroller" style="height: 283px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                    <table class="table table-hover table-light resource_persons">
                        <thead>
                        <tr class="uppercase">
                            <th> Name </th>
                            <th><span class="pull-right">Forms </span> </th>


                        </tr>
                        </thead>

                        <tbody id="data_entrant">

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
