
<div class="row">

    <div class="col-md-4">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bar-chart"></i>Top Household</div>
                <div class="actions">
                    <div class="btn-arrow-link pull-right">
                        <a style="text-decoration: none;" href="#<?php echo base_url()?>">See All <i class="icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div  class="table-responsive scroller" style="height:275px;">
                    <table class="table table-hover table-light ">

                        <tbody class="top_household"> </tbody>

                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>

    <div class="col-md-4">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bar-chart"></i>Top Villages</div>
                <div class="actions">
                    <div class="btn-arrow-link pull-right">
                        <a style="text-decoration: none;" href="#<?php echo base_url()?>">See All <i class="icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div  class="table-responsive scroller" style="height:275px;">
                    <table class="table table-hover table-light ">

                        <tbody class="top_villages">  </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>

    <div class="col-md-4">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bar-chart"></i>Distributions Points</div>
                <div class="actions">
                    <div class="btn-arrow-link pull-right">
                        <a style="text-decoration: none;" href="#<?php echo base_url()?>">See All <i class="icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div  class="table-responsive scroller" style="height:275px;">
                    <table class="table table-hover table-light ">

                        <tbody class="top_subcounty">  </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>



</div>