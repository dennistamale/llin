<?php


$this->db->select('distinct(district) as district')->from('registration');
$districts = $this->db->get()->result();

//echo print_r($districts);


if(count($districts)==0){
$data=array(
    'alert'=>'info',
    'message'=>'There are no distributions for now, Try again later',
    'hide' => 1
);
$this->load->view('alert',$data);
}
?>



<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ACCORDION PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo humanize($subtitle) ?></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <!--                <a href="#portlet-config" data-toggle="modal" class="config"> </a>-->
                    <!--                <a href="javascript:;" class="reload"> </a>-->
                    <!--                <a href="javascript:;" class="remove"> </a>-->
                </div>
            </div>
            <div class="portlet-body">
                <div class="panel-group accordion" id="accordion3">
                    <?php  $no=0;
                    foreach($districts as $dis){ ?>

                        <?php

                        $parishes =  $this->db->select('parish')->from('registration')->where(array('district'=>$dis->district))->group_by('parish')->get()->result();




                        ?>

                        <?php //print_r($forms); ?>

                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_<?php echo $no ?>"> <b> <?php echo humanize($this->locations->get_location_name($dis->district))  ?> </b> > <?php echo $cnt=count($parishes) ?> Parish<?php echo $cnt>1?'es':''; ?>  </a>
                                </h4>
                            </div>

                            <div id="collapse_3_<?php echo $no ?>" class="panel-collapse <?php echo $no==0?'in':'collapse'; ?>">
                                <div class="panel-body">



                                    <div class="table-scrollable">
                                        <table class="table table-condensed table-hover">
                                            <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Subcounty </th>
                                                <th> Parish </th>
                                                <th> #Village </th>
                                                <th> #HouseHolds </th>
                                                <th> #Pop </th>
                                                <th> #Nets </th>
                                                <th> #Bailes </th>
                                                <th> #Extra </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php

                                            $forms = $this->db->select()->from('registration')->where(array('district'=>$dis->district))->group_by('village')->get()->result();



                                            $n2=1;
                                            foreach($forms as $f){

                                               $path=$this->locations->get_path($f->village);

                                                ?>
                                                <tr>
                                                    <td> <?php echo $n2; ?> </td>
                                                    <td> <?php echo isset($path[2])? humanize($path[2]['name']):'' ?> </td>
                                                    <td> <?php echo isset($path[3])? humanize($path[3]['name']):'' ?> <?php //echo $f->village; ?> </td>

                                                    <td>
                                                        <?php
                                                     $villages=$this->db->select('distinct(village)')->from('registration')->where(array('parish'=>$f->parish))->get()->result();

                                                      echo  count($villages);
                                                        ?>


                                                    </td>
                                                    <td>
                                                        <?php
                                                        $for_det=$this->db->select('b.person_vht')
                                                            ->from('registration a')
                                                            ->join('reg_detail b','b.data_id=a.id')
                                                            ->where(array('a.village'=>$f->village,'confirm'=>1))->get()->result();

                                                        echo count($for_det);


                                                        ?>
                                                    </td>

                                                    <td> <?php




                                                        $pop=0;
                                                        $hh_net=0;
                                                        foreach($for_det as $dd){

                                                            //adding the total house holds
                                                            $pop=$pop+$dd->person_vht;


                                                            $original_num=bcdiv(($dd->person_vht/2), 1,0);
                                                            $modulus_num=($dd->person_vht%2)==0?0:1;
                                                            $nets=$original_num+$modulus_num;

                                                            //computing the the nets
                                                            $hh_net=$hh_net+$nets;

                                                        }

                                                        echo $pop;

                                                        //echo count($for_det);


                                                        ?>   </td>

                                                    <td>

                                                        <?php
                                                        //computing the the nets
                                                        echo $hh_net;

                                                        ?>


                                                    </td>

                                                    <td>  <?php
                                                        //these are the house holds $hh_net
                                                        //echo  $bb=round((50/40),0, PHP_ROUND_HALF_DOWN);

                                                        //50/40

                                                        $mod_bails=($hh_net%40);
                                                        $modulus_bails=$mod_bails==0?0:1;
                                                        echo  bcdiv($hh_net,40,0)+$modulus_bails;


                                                        ?> </td>
                                                    <td>
                                                        <?php echo $mod_bails>0?(40-$mod_bails):0; ?>
                                                    </td>
                                                </tr>

                                                <?php $n2++;
                                            } ?>

                                            </tbody>
                                        </table>
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <?php $no++; } ?>

                </div>
            </div>
        </div>
        <!-- END ACCORDION PORTLET-->
    </div>
</div>