<?php //$this->load->view($this->page_level.$this->page_level2.'tree_diagram'); ?>


<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable ">
            <div class="portlet-title hidden " style="    padding: 0px 20px 0px;">


                <div class="caption font-dark" style="    padding: 19px 0;">
                    <i class="icon-home font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo humanize($title) ?></span>
                    <?php //echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> Add new','class="btn btn-sm green-jungle"'); ?>
                </div>


                <div class="tools hidden">
                    <div class="dt-button buttons-print btn dark btn-outline"><i class="fa fa-export"></i>Print</div>
                    <div class="dt-button buttons-print btn green btn-outline"><i class="fa fa-export"></i>PDF</div>
                    <div class="dt-button buttons-print btn purple btn-outline"><i class="fa fa-export"></i>CSV</div>
                </div>
            </div>
            <div class="portlet-body  table-responsive ">
                <div class="table-container ">
                    <div class="table-actions-wrapper  <?php echo $this->custom_library->role_exist('Export Forms Data')?'':'hidden' ?>">


                        <?php echo form_open() ?>

                        <select name="export"  class="btn red btn-sm  btn-outline dropdown-toggle"  onchange="this.form.submit()">
                            <option value="">Export</option>
                            <option value="export_pdf">PDF</option>
                            <option value="excel">Excel</option>

                        </select>
                        <?php echo form_close(); ?>

                        <div class="form-inline hidden">
                            <div class="form-group">

                                <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                                    <span class="input-group-addon">From </span>
                                    <input type="text" class="form-control table-group-from-input input-sm" name="from" value="<?php echo set_value('from') ?>">
                                    <span class="input-group-addon">to </span>
                                    <input type="text" class="form-control table-group-to-input input-sm" name="to" value="<?php echo set_value('to')?>">
                                </div>
                                <button class="btn btn-sm green table-group-action-submit">
                                    <i class="fa fa-sliders"></i> Apply</button>

                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="<?php echo $title ?>">
                        <thead>
                        <tr role="row" class="heading">
                                                        <th width="1%">
                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                                                <span></span>
                                                            </label>
                                                        </th>


<!--                            <th width="20%" style="white-space: nowrap;"> VHT </th>-->
                            <th width="1%"> Request ID </th>
                            <th width="2%"> Date </th>
                            <th width="10%"> Requester </th>
                            <th width="5%"> Forms </th>
                            <th width="5%"> Request type </th>
                            <th width="1%">Request Status </th>
                            <th width="1%">Approver </th>
                            <th width="1%">Approval Date </th>
                            <th width="1%"> Actions </th>
                        </tr>
<!--                        Request ID | Request Date| Requester| #Forms | Request type | Request Status | Actions-->
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<?php //echo $title ?><!--"-->
<?php
$this->load->view('ajax/'.$title);
?>