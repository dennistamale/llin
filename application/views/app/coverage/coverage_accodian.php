<?php


$this->db->select('distinct(district) as district')->from('registration');
$districts = $this->db->get()->result();

//echo print_r($districts);


if(count($districts)==0){
$data=array(
    'alert'=>'info',
    'message'=>'There are no distributions for now, Try again later',
    'hide' => 1
);
$this->load->view('alert',$data);
}
?>

<style>
    .print_icon{
        position: absolute;
        margin-top: -26px;
        /* float: right; */
        /* width: 2px; */
        height: 30px;
        margin-left: -50px!important;
        cursor: pointer;

    }
</style>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN ACCORDION PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo humanize($subtitle) ?></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <!--                <a href="#portlet-config" data-toggle="modal" class="config"> </a>-->
                    <!--                <a href="javascript:;" class="reload"> </a>-->
                    <!--                <a href="javascript:;" class="remove"> </a>-->
                </div>
            </div>
            <div class="portlet-body">
                <div class="panel-group accordion" id="accordion3">
                    <?php  $no=0;
                    foreach($districts as $dis){ ?>

                        <?php

                        $parishes =  $this->db->select('parish')->from('registration')->where(array('district'=>$dis->district))->group_by('parish')->get()->result();




                        ?>

                        <?php //print_r($forms); ?>

                        <div class="panel panel-default">

                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" tbody_id="0" href="#<?php echo $dis->district ?>"> <b> <i class="fa fa-globe"></i>  <?php echo humanize($this->locations->get_location_name($dis->district))  ?> </b>




                                    </a>
                                    <span class="pull-right print" id="print<?php echo $dis->district?>"> <i class="fa fa-print  print_icon text-primary"></i> </span>


                                </h4>
                            </div>

                            <div id="<?php echo $dis->district ?>" class="table-responsive panel-collapse <?php echo $no==0?'in':'collapse'; ?>">
<!--                                    this is the space where the tables will be loaded-->
                            </div>
                        </div>

                        <?php $no++; } ?>

                </div>
            </div>
        </div>
        <!-- END ACCORDION PORTLET-->
    </div>
</div>

<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script>


    $(".accordion-toggle").on('ready , click',function (e) {
        var href = $(this).attr( "href" );
        var p=href.replace(/#/g, "");

        $.ajax({
            type: 'GET',

            url: '<?php echo base_url("index.php/ajax_api/get_coverage")?>/' + p,
            beforeSend: function () {

                 $("#"+p).html('<div style="text-align: center; padding:15px;" class="text-success"><i class="fa fa-spinner fa-2x fa-pulse"></i> Please wait.....</div>');
            },
            success: function (d) {

                 $("#"+p).html(d);


            }


        });
    });


    <?php $dis=$districts[0]->district; ?>



    $("document").ready(function (e) {

        var current=<?php echo $dis; ?>

        $.ajax({
            type: 'GET',

            url: '<?php echo base_url("index.php/ajax_api/get_coverage/$dis")?>',
            beforeSend: function () {
                $("#"+current).html( '<div style="text-align: center; padding:15px;" class="text-success"><i class="fa fa-spinner fa-2x fa-pulse"></i> Please wait.....</div>');
            },
            success: function (d) {
                $("#"+current).html(d);

            }

        });
    });



</script>


<script>

    $(".print").click(function(){
        var print = $(this).attr( "id" );
        //alert(print);
//        $("#"+p).html(d);

        //alert($("."+print).print());


        //  $("."+print).html();
        //window.print();



        var newWin=window.open('','Print-Window');


        newWin.document.write('<html><body>');

        newWin.document.write('<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /><link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" /><link href="<?php echo base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" /><link href="<?php echo base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" /><link href="<?php echo base_url() ?>assets/layouts/layout2/css/themes/dark.min.css" rel="stylesheet" type="text/css" id="style_color" /><link href="<?php echo base_url() ?>assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />');


        newWin.document.write($("."+print).html());

        newWin.document.write('</body></html>');

        newWin.document.close();


        newWin.print();
        newWin.close();


    });

</script>