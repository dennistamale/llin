<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() ?>assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->




<div class="row">
    <div class="col-md-3">
        <div class="portlet light  bordered">

            <div class="portlet-body">


                <div id="tree_1" class="tree-demo">
                    <ul>
                        <li  data-jstree='{ "selected" : true , "opened" : true }'> Uganda

                            <?php

                            $tree->display_children(1,0);

                            ?>
                            <!--                            <ul>-->
                            <!--                                <li data-jstree='{ "selected" : true }'>-->
                            <!--                                    <a href="javascript:;"> Initially </a>-->
                            <!--                                </li>-->
                            <!--                                <li data-jstree='{ "icon" : "fa fa-briefcase icon-state-success " }'> custom icon URL </li>-->
                            <!--                                <li data-jstree='{ "opened" : true }'> initially open-->
                            <!--                                    <ul>-->
                            <!--                                        <li data-jstree='{ "disabled" : true }'> Disabled Node </li>-->
                            <!--                                        <li data-jstree='{ "type" : "file" }'> Another node </li>-->
                            <!--                                    </ul>-->
                            <!--                                </li>-->
                            <!--                                <li data-jstree='{ "icon" : "fa fa-warning icon-state-danger" }'> Custom icon class (bootstrap) </li>-->
                            <!--                            </ul>-->
                        </li>
                        <!--                        <li data-jstree='{ "type" : "file" }'>-->
                        <!--                            <a href="http://www.jstree.com"> Clickanle link node </a>-->
                        <!--                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bubble font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Checkable Tree</span>
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;"> Option 1</a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="javascript:;">Option 2</a>
                            </li>
                            <li>
                                <a href="javascript:;">Option 3</a>
                            </li>
                            <li>
                                <a href="javascript:;">Option 4</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div id="tree_2" class="tree-demo"> </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="portlet yellow-lemon box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Contextual Menu with Drag & Drop </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                    <a href="javascript:;" class="reload"> </a>
                    <a href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div id="tree_3" class="tree-demo"> </div>
                <div class="alert alert-success no-margin margin-top-10"> Note! Opened and selected nodes will be saved in the user's browser, so when returning to the same tree the previous state will be restored. </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet red-pink box">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>Ajax Tree with Drag & Drop </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                    <a href="javascript:;" class="reload"> </a>
                    <a href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div id="tree_4" class="tree-demo"> </div>
                <div class="alert alert-info no-margin margin-top-10"> Note! The tree nodes are loaded from <?php echo base_url() ?>demo/jstree_ajax_data.php via ajax. </div>
            </div>
        </div>
    </div>
</div>

