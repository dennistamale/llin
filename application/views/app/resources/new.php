<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-user font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> New</span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">

                <?php echo form_open_multipart('',array('class'=>'form-horizontal')) ?>
                <!--                id, full_name, city, password, username, region, country, phone, email, gender, photo, user_type, sub_type, status, verified, created_on, created_by, updated_on, updated_by, id, id-->
                <div class="form-body">

<div class="row">


                    <div class="form-group col-md-6">

                        <label class="col-md-4 control-label" for="title">Name</label>
                        <div class="col-md-8">


                            <input type="text" name="title" class="form-control" value="<?php echo set_value('title') ?>">
                            <label for="form_control_1"> <?php echo form_error('title','<span style=" color:red;">','</span>') ?></label>

                        </div>

                        </div>

    <div class="form-group col-md-6">
        <label class="col-md-4 control-label" for="form_control_1">Resource Type</label>
        <div class="col-md-8">
            <select class="form-control" name="resource_type" required  >
                <option value="" <?php echo set_select('resource_type', '', TRUE); ?> >Select Type</option>

                <?php foreach($this->db->select()->from('resource_types')->get()->result() as $cat){ ?>
                    <option value="<?php echo $cat->id  ?>"  <?php echo set_select('resource_type', $cat->id); ?> ><?php echo $cat->title ?></option>
                <?php } ?>

            </select>
            <label for="form_control_1"> <?php echo form_error('resource_type','<span style=" color:red;">','</span>') ?></label>

        </div>

    </div>


    <div class="form-group col-md-12">

                        <label class="col-md-2 control-label" for="form_control_1">Description</label>
                        <div class="col-md-10">

                            <textarea class="form-control" rows="5" maxlength="500" required   name="notes"><?php echo set_value('notes') ?></textarea>
                            <label for="form_control_1"> <?php echo form_error('notes','<span style=" color:red;">','</span>') ?></label>

                        </div>

                    </div>

                    <div class="form-group col-md-6">
                        <label class="control-label col-md-4" for="form_control_1">Url</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="Source Url" name="url" value="<?php echo set_value('url') ?>" >
                            <label for="form_control_1"> <?php echo form_error('url','<span style=" color:red;">','</span>') ?></label>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label col-md-4" for="form_control_1">Attachment  <?php echo form_error('attachment','<span style=" color:red;">','</span>') ?></label>
                        <div class="col-md-8">

                            <?php if( isset($error)){?>
                                <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
                            <?php } ?>

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="input-group input-large">
                                    <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                        <span class="fileinput-filename"> </span>
                                    </div>
                                                                            <span class="input-group-addon btn default btn-file">
                                                                                <span class="fileinput-new"> Select file </span>
                                                                                <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="attachment"/> </span>
                                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>
                    </div>

</div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                            <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>


</div>

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
