<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">



                <div class="caption font-dark">
                    <i class="icon-home font-dark"></i>
                    <span class="caption-subject bold uppercase">Emails</span>
                </div>
                <div class="actions pull-left">

            <?php //echo anchor($this->page_level.$this->page_level2.'new_branch',' <i class="fa fa-plus"></i> New Branch','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Email </th>
                        <th>Date</th>
                        <th> Status </th>

                        <th width="70"> Sent </th>
<!--                        <th width="70">Actions</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    //id, branch_name, country, state, street, phone, website, others, created_by, created_on, updated_by, updated_on, id, id
                    foreach($this->db->select()->from('newsletter_emails')->get()->result() as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td> <?php echo $c->email ?> </td>
                            <td><?php echo trending_date_time($c->created_on) ?></td>



                            <td><?php echo $c->status=='active'?'<div class="btn btn-sm green-jungle" style="width: 70px;">Active</div>':'<div class="btn btn-sm btn-danger" style="width: 70px;">Blocked</div>' ?></td>

                            <td><?php echo $c->email_sent=='1'?'<div class="btn btn-sm green-jungle" style="width: 70px;">Sent</div>':'<div class="btn btn-sm btn-danger"  style="width: 70px;" data-original-title="" title="For these Branches to access the System you Countries have to to unblocked First">Not sent</div>' ?></td>

                        </tr>
                    <?php
                    $no++;
                    endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->