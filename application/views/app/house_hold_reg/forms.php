<style>
    .no_background {
        background: none;
        border: none;
        width: 60%;
    }
</style>

<div class="row">
    <?php $this->load->view($this->page_level . $this->page_level2 . 'tree_diagram'); ?>


    <div class="col-md-9">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-file"></i>House Hold Registration Form
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <!--                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>-->
                    <!--                    <a href="javascript:;" class="reload"> </a>-->
                    <!--                    <a href="javascript:;" class="remove"> </a>-->
                </div>
            </div>
            <div class="portlet-body">

                <?php
                $form_attributes = array(
                    'class' => 'form-inline',
                );

                echo form_open('', $form_attributes)
                ?>


                <span class="hidden-print">
                      <div class="form-group">

                          <?php echo form_error('form_date', '<label style="color:#ff0000;">', '</label>'); ?>

                          <div class="input-group input-group-sm input-medium">
                              <span class="input-group-addon">Form Date</span>
                              <input class="form-control date-picker" data-date="<?php echo date('Y-m-d') ?>"
                                     data-date-format="yyyy-mm-dd" name="form_date"
                                     value="<?php echo set_value('form_date') ?>">
                          </div>

                      </div>

                      <div class="form-group">

                          <?php echo form_error('serial_no', '<label style="color:#ff0000;">', '</label>'); ?>

                          <div class="input-group input-group-sm input-small">
                              <span class="input-group-addon">Serial #</span>
                              <input class="form-control" name="serial_no" value="<?php echo set_value('serial_no') ?>">
                          </div>

                      </div>




    <button type="submit" name="submit" value="filter" class="btn btn-sm green"><i class="fa fa-search"></i> Search
    </button>
    </span>
                <?php echo form_close(); ?>

                <hr/>

                <?php echo form_open('', $form_attributes) ?>


                <?php //isset($form)?print_r($form):''; ?>

                <?php

                if (isset($alert)) {
                    $alert = array(
                        'alert' => 'warning',
                        'message' => 'Form <b>(' . $alert . ')</b> cannot be found !!!',
                        'hide' => 1
                    );
                    $this->load->view('alert', $alert);
                }



                ?>

                <input name="village_id" hidden class="form_control no_background"
                       value="<?php echo isset($form) ? $form->village_id : '' ?>"/>
                <input name="form_num" hidden class="form_control no_background"
                       value="<?php echo isset($form) ? $form->form_num : '' ?>"/>

                <table class="table table-bordered table-striped  table-hover">
                    <tbody>
                    <tr class="active">
                        <td><b>District : <input name="district" readonly class="form_control no_background"
                                                 value="<?php echo isset($form) ? $form->district : '' ?>"/> </b></td>
                        <td><b>Subcounty : <input name="subcounty" readonly class="form_control no_background"
                                                  value="<?php echo isset($form) ? $form->subcounty : '' ?>"/> </b></td>
                        <td><b>Parish : <input name="parish" readonly class="form_control no_background"
                                               value="<?php echo isset($form) ? $form->parish : '' ?> "/> </b></td>
                        <td><b>Village : <input name="village" readonly class="form_control no_background"
                                                value="<?php echo isset($form) ? $form->village : '' ?>"/> </b></td>
                    </tr>
                    <tr class="active">
                        <td><b>VHT Name : <input name="vht_name" readonly class="form_control no_background"
                                                 value="<?php echo isset($form) ? $form->name : '' ?>"/> </b></td>
                        <td><b>VHT Code : <input name="vht_code" readonly class="form_control no_background"
                                                 value="<?php echo isset($form) ? $form->code : '' ?>"/> </b></td>
                        <td><b>VHT Phone : <input name="vht_phone" readonly class="form_control no_background"
                                                  value="<?php echo isset($form) ? $form->phone : '' ?>"/> </b></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>




                <span style="color: red;">
                <?php //$this->input->post()?print_r($this->input->post()):'';


                echo validation_errors();

                //print_r($this->input->post());
                // ?>

                    <hr/>
                    <?php // print_r($test_form) ?>
                </span>
                <div class="row" style="padding-bottom: 5px;">
                    <div class="col-md-12">

                        <div class="form-group">

                            <?php echo form_error('house_holds[][household_head]', '<label style="color:#ff0000;">', '</label>'); ?>

                            <div class="input-group input-group-sm input-medium">
                                <span class="input-group-addon">Household head</span>
                                <input required class="form-control" name="house_holds[][household_head]"
                                       value="<?php echo set_value('house_holds[][household_head]') ?>">
                            </div>

                        </div>

                        <div class="form-group">

                            <?php echo form_error('phones[][phone]', '<label style="color:#ff0000;">', '</label>'); ?>

                            <div class="input-group input-group-sm input-small">
                                <span class="input-group-addon">Phone</span>
                                <input required class="form-control" maxlength="13" minlength="10"
                                       name="phones[][phone]" value="<?php echo set_value('phones[][phone]') ?>">
                            </div>

                        </div>
                        <div class="form-group">

                            <?php echo form_error('national_ids[][national_id]', '<label style="color:#ff0000;">', '</label>'); ?>

                            <div class="input-group input-group-sm input-medium">
                                <span class="input-group-addon">National ID</span>
                                <input class="form-control" name="national_ids[][national_id]"
                                       value="<?php echo set_value('national_ids[][national_id]') ?>">
                            </div>

                        </div>

                        <div class="form-group">

                            <?php echo form_error('persons[][total_persons]', '<label style="color:#ff0000;">', '</label>'); ?>

                            <div class="input-group input-group-sm input-small">
                                <span class="input-group-addon">Total Persons</span>
                                <input required class="form-control" name="persons[][total_persons]"
                                       value="<?php echo set_value('persons[][total_persons]') ?>">
                            </div>

                        </div>
                    </div>


                    <span id="house_form"> </span>


                </div>
            </div>


            <div id="add_more" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></div>
            <button name="submit" value="house_hold_form" type="submit" class="btn btn-sm green"><i
                    class="fa fa-search"></i> Save
            </button>

            <?php echo form_close(); ?>

        </div>
    </div>
</div>

</div>


<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script>
    $("#add_more").click(function (e) {
        e.preventDefault();
        $("#house_form").append('<div class="col-md-12" style="padding-top:5px;"><div class="form-group"><?php echo form_error('house_holds[][household_head]', '<label style="color:#ff0000;">', '</label>'); ?><div class="input-group input-group-sm input-medium"><span class="input-group-addon">Household head</span><input class="form-control" required name="house_holds[][household_head]" value="<?php echo set_value('house_holds[][household_head]') ?>"></div> </div> <div class="form-group"><?php echo form_error('phones[][phone]', '<label style="color:#ff0000;">', '</label>'); ?> <div class="input-group input-group-sm input-small"><span class="input-group-addon">Phone</span><input required  maxlength="13" minlength="10" class="form-control" name="phones[][phone]" value="<?php echo set_value('phones[][phone]') ?>"></div></div> <div class="form-group"><?php echo form_error('national_ids[][national_id]', '<label style="color:#ff0000;">', '</label>'); ?><div class="input-group input-group-sm input-medium"><span class="input-group-addon">National ID</span><input class="form-control" name="national_ids[][national_id]" value="<?php echo set_value('national_ids[][national_id]') ?>"></div></div> <div class="form-group"><?php echo form_error('persons[][total_persons]', '<label style="color:#ff0000;">', '</label>'); ?><div class="input-group input-group-sm input-small"><span class="input-group-addon">Total Persons</span><input class="form-control" required name="persons[][total_persons]" value="<?php echo set_value('persons[][total_persons]') ?>"></div></div></div>');
    })
</script>
