<style>
    .no_background {
        background: none !important;
        border: none;
        /*width: 60%;*/
    }
    .no_background_table {
        background: none;
        /*border: none;*/
        width: 100% !important;
    }

    .flagged{
        background-color: rgba(240,200,0,0.5);
    }
    .table {
        width: 100%;
        margin-bottom: 10px;
    }

    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 5px;
        /* line-height: 1.42857; */
        /* vertical-align: top; */
        border-top: 1px solid #e7ecf1;
    }
    .table td, .table th {
        font-size: 12px;
    }

    .input-group-sm>.form-control, .input-group-sm>.input-group-addon, .input-group-sm>.input-group-btn>.btn, .input-sm {
        height: 25px;
        padding: 0px 1px;
        font-size: 11px;
        line-height: 1.5;
        border-radius: 3px;
    }

    .collection_var{

        border: solid thin red;
    }
</style>


<?php
//this show the form status

if(isset($form) ) {

    if ($form->status == 'in_field') {

        $status = '<div style="width:100px;" class="label  label-sm label-danger">' . humanize($form->status) . '</div>';
    }
    elseif ($form->status == 'in_store') {

        $status = '<div style="width:100px;" class="label  label-sm label-warning">' . humanize($form->status) . '</div>';
    }
    elseif ($form->status == 'released') {

        $status = '<div style="width:100px;" class="label label-sm label-primary">' . humanize($form->status) . '</div>';
    }
    elseif ($form->status == 'returned') {

        $status = '<div style="width:100px;" class="label  label-sm  label-success">' . humanize($form->status) . '</div>';
    }
    else {
        $status = '<div style="width:100px;" class="label   label-sm label-danger">In field</div>';
    }
}else{
    $status = '<div style="width:100px;" class="label  label-sm label-danger">In field</div>';
}

?>




<?php  $form_confirm=0;//$this->db->where(array('confirm'=>1,'form_num'=> $form->form_num))->from('registration')->count_all_results(); ?>

<div class="row">


    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-file"></i>House Hold Registration Form
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <!--                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>-->
                    <!--                    <a href="javascript:;" class="reload"> </a>-->
                    <!--                    <a href="javascript:;" class="remove"> </a>-->
                </div>
            </div>
            <div class="portlet-body">

                <?php
                $form_attributes = array(
                    'class' => 'form-inlin',
                );
                ?>


                <?php echo form_open('', $form_attributes) ?>
                <?php

//                if (isset($alert)) {
//                    $alert = array(
//                        'alert' => 'warning',
//                        'message' => 'Form <b>(' . $alert . ')</b> cannot be found !!!',
//                        'hide' => 1
//                    );
//                    $this->load->view('alert', $alert);
//                }



                ?>

                <input name="village_id" hidden class="form_control no_background"  value="<?php echo isset($form) ? $form->village : '' ?>"/>
                <input name="form_num" hidden class="form_control no_background"
                       value="<?php echo isset($form) ? $form->form_num : '' ?>"/>




                <table class="table table-bordered  table-hover">
                    <tbody>
                    <tr>

                        <?php  $path=$this->locations->get_path($form->village);
                        //$village_name=$this->locations->get_location_name($form->village);

                       // print_r($path);
                        ?>
                        <td >

                            <b>District : <input name="district" readonly class="form_control no_background"
                                                 value="<?php echo isset($form) ?  $path[1]['name']  : '' ?>"/> </b></td>
                        <td><b>Subcounty : <input name="subcounty" readonly class="form_control no_background"
                                                  value="<?php echo isset($form) ? $path[2]['name']  : '' ?>"/> </b></td>
                        <td><b>Parish : <input name="parish" readonly class="form_control no_background"
                                               value="<?php echo isset($form) ? $path[3]['name'] : '' ?> "/> </b></td>
                        <td><b>Village : <input name="village" readonly class="form_control no_background"
                                                value="<?php echo isset($form) ?   $path[4]['name'] : '' ?>"/> </b></td>
                    </tr>
                    <tr >
                        <td><b>VHT Name : <input name="vht_name" readonly class="form_control no_background"
                                                 value="<?php echo isset($form) ? $form->first_name.' '.$form->last_name : '' ?>"/> </b></td>

                        <td><b>VHT Phone : <input name="vht_phone" readonly class="form_control no_background"
                                                  value="<?php echo isset($form) ? $form->phone : '' ?>"/> </b></td>

                        <td><b>Form Status : <?php echo $status ?> </b></td>
                        <td><b>Collection Date : <?php echo isset($form) ? trending_date($form->collection_date) : '' ?> </b></td>
                    </tr>
                    </tbody>
                </table>







                <!--                    This is the part where get the two entered forms  -->

                <?php $this->db->select('a.id,first_name,last_name,form_num,a.created_on,confirm,entry_times')
                    ->from('registration a')
                    ->join('users b','a.created_by=b.id','left')
                    ->where(array('form_num'=>$form->form_num,'entry_times!='=>3));
//                if($form_confirm=!0){$this->db->order_by('id');}
                $registered_forms=$this->db->limit(2)->get()->result();
                $forms['registered_forms']=$registered_forms;

                ?>


                <?php if(count($registered_forms)!=0){ ?>


<!--                    this is the part for the Tabs-->


                    <div class="row">
                      <div class="col-md-12">
                          <div class="tabbable-custom ">
                              <ul class="nav nav-tabs ">

                                  <li class="<?php echo $this->uri->segment(5)=='compare'?'active':'hidden' ?>">
                                      <a href="#entered_forms" data-toggle="tab"> Entered Forms </a>
                                  </li>

                                  <li class="<?php echo $this->uri->segment(5)==''?'active':'hidden' ?>">
                                      <a href="#final_form" data-toggle="tab"> Form Data </a>
                                  </li>


                              </ul>
                              <div class="tab-content">
                                  <div class="tab-pane <?php echo $this->uri->segment(5)=='compare'?'active':'' ?>" id="entered_forms">
                                      <?php $this->load->view($this->page_level.$this->page_level2.'form_entries',$forms) ?>
                                  </div>
                                  <div class="tab-pane <?php echo $this->uri->segment(5)==''||$this->uri->segment(5)=='success'?'active':'' ?>" id="final_form">

                                      <?php

                                      if($this->uri->segment(5)=='success'){
                                          $data['alert']='success';
                                          $data['message']='Distribution has been saved successfully';
                                          $this->load->view('alert',$data);
                                      }

                                      ?>
                                      <?php $this->load->view($this->page_level.$this->page_level2.'final_form',$forms) ?>
                                  </div>

                              </div>
                          </div>
                      </div>

                    </div>

<!--                    this is the end of tabs-->




                <?php echo form_close(); ?>

                <?php }else{

                        $alert = array(
                            'alert' => 'info',
                            'message' => '<b>Sorry</b> No form entered '.anchor($this->page_level.$this->page_level2.'new/'.$form->form_num,'Click to Enter The Forms'),
                            'hide' => 1
                        );

                        $this->load->view('alert', $alert);


                } ?>
            </div>



        </div>
    </div>
</div>

</div>

