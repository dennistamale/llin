<?php



$this->db->select('a.id,first_name,last_name,form_num,a.created_on')
    ->from('registration a')
    ->join('users b','a.created_by=b.id','left')
    ->where(array('form_num'=>$form->form_num,'confirm'=>1));
//                if($form_confirm=!0){$this->db->order_by('id');}
$final_form=$this->db->limit(1)->get()->row();


?>
<!---->
<?php
//print_r($this->input->post('id'));
//echo '<br/>';
//print_r($this->input->post())
//
//?>


<div class="row">

    <?php echo form_open() ?>

<div class="col-md-12">

<div class="row">
    <div class="col-md-12" style="border-bottom: solid thin #d5d5d5; margin-bottom: 10px;">
    <?php if(count($final_form)==1){ ?>
        <h4>

            <b>Final : <?php echo strlen($final_form->first_name)>0?$final_form->first_name.' '.$final_form->last_name:'Not Known'; ?> </b> <?php echo trending_date($final_form->created_on) ?>



        </h4>

        <?php
        //                        getting house holds for the specific form_id
        $form2=$this->db->select()->from('reg_detail')->where(array('data_id'=>$final_form->id))->order_by('row_no','asc')->get()->result();
        ?>
    <?php }else{
        $form2=array();
        echo '<h4>

                            <b>Form2 : Not Known</b></h4>';
    } ?>
    </div>
    <div class="col-md-6 hidden">
           <span class="form-inline pull-right">
                  <div class="form-group">


                      <select name="action" required class="form-control input-inline input-small input-sm">
                                    <option value="">Signed ?</option>
                                    <option value="Y">Yes</option>
                                    <option value="N">No</option>

                                </select>
                                <button type="submit" name="submit" value="sign"  class="btn btn-sm green table-group-action-submi">
                                    <i class="fa fa-check"></i> Submit</button>


                            </div>
            </span>
    </div>
</div>




    <div class=" table-responsiv ">


        <table class="table table-bordered table-striped  table-hover" id="sample_">
            <thead>
            <tr>
                <th style="width: 10px;">

                </th>
                <th colspan="2">Household Head</th>
                <th>Phone</th>
                <th>National ID</th>
                <th>Chalk ID</th>
                <th  colspan="3">Total Persons</th>
                <th>Alloc ated</th>
                <th>Distri buted</th>
                <th>Sign ature</th>
                <th>Date</th>
                <th style="width: 80px;">Action</th>
            </tr>

            <tr>
                <th></th>
                <th>Surname</th>
                <th>First Name</th>
                <th colspan="3">&nbsp;</th>

                <th style="width: 70px;">VHT</th>
                <th style="width: 70px;">PC</th>
                <th style="width: 70px;">FINAL</th>
                <th colspan="5"></th>
            </tr>

            </thead>
            <tbody>

            <?php echo count($form2)==0?'<tr style="text-align: center;"><td colspan="7">No Final record Yet</td></tr>':''; ?>

            <?php foreach($form2 as $f2){ ?>

                <tr>
                    <td>

                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="group-checkable" name="id[]" value="<?php echo $f2->id ?>" />
                            <span></span>
                        </label>

                    </td>

                    <td>
                        <?php echo $f2->last_name ?>

                    </td>
                    <td>
                        <?php echo $f2->first_name ?>

                    </td>
                    <td>
                        <?php echo $f2->tel ?>
                    </td>
                    <td >
                        <?php echo $f2->national_id ?>
                    </td>
                    <td>
                        <?php echo $f2->chalk_id ?>
                    </td>
                    <td >
                        <?php echo $f2->person_vht ?>
                    </td>
                    <td>
                        <?php echo $f2->person_sc ?>
                    </td>
                    <td>
                        <?php echo $f2->person_final ?>
                    </td>
                    <?php

                    $final_pop=$f2->person_final!=0?$f2->person_final:$f2->person_vht;

                    $hp= nets_agg_only($final_pop); ?>
                    <td><?php echo  $hp['house_hold_nets'] ?></td>

                    <?php
                    if ($f2->picked == 0) {

                        $net_picked = '<div class="btn btn-xs  btn-info">' . $f2->no_picked . '</div>';
                    }
                    elseif ($f2->picked == 1) {

                        $net_picked = '<div  class="btn btn-xs  green-jungle">' . $f2->no_picked . '</div>';

                    }
                    else {
                        $net_picked = '<div class="btn btn-xs btn-info">' . $f2->no_picked . '</div>';
                    }
                    ?>
                    <td><?php echo  $net_picked ?></td>

                    <td>

                        <?php
                        if ($f2->signed == 'N') {

                            $signed = '<div class="btn btn-xs  btn-info">No</div>';
                        }
                        elseif ($f2->signed == 'Y') {

                            $signed = '<div  class="btn btn-xs  green-jungle">Yes</div>';

                        }
                        else {
                            $signed = '';
                        }
                        ?>

                        <?php echo $signed; ?>
                    </td>

                    <td  style="white-space: nowrap;"><?php echo $f2->picked_on != 0 ? trending_date($f2->picked_on):'' ?></td>
                    <td><?php echo  anchor('ajax_api/get_form/'.$f2->id*date('Y'), '<i class="fa fa-check"></i> Distribute ', 'data-target="#ajax" data-toggle="modal" class="btn btn-xs btn-info"') ?></td>

                </tr>

            <?php } ?>



            </tbody>
        </table>


    </div>

</div>

    <?php echo form_close(); ?>
</div>