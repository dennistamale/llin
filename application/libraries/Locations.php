<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php

class locations {
    private $ci;


    function __construct()
        {
            $this->ci =& get_instance();
            $this->ci->load->database();
        }


    function dennis()
        {
            //$this->CI
            $users = $this->CI->db->select()->from('users')->get()->result();
            return ($users);


        }


    ///this the Location Management for the LLIN project

    function get_country($id)
        {


        }

    function get_district($id = null)
        {

            // SELECT lft, rght INTO our_lft, our_rght FROM locations WHERE id = loc_id;
//        SELECT name FROM locations WHERE lft <= 65991 AND rght >= 67742 AND type_id=(SELECT id FROM locationtype WHERE name = 'district');

            if (isset($id)) {
                $result = $this->ci->db->select('lft, rght')->from('locations')->where(array('id' => $id))->get()->row();

                if (count($result) == 1) {

                    $our_lft = $result->lft;
                    $our_rght = $result->rght;

                    $qry = "SELECT name FROM locations WHERE lft <= $our_lft AND rght >= $our_rght AND type_id=(SELECT id FROM locationtype WHERE name = 'district');";

                    $name = $this->ci->db->query($qry)->row();

                    return $name;


                }
                else {

                    return false;
                }
            }
            else {
                return false;
            }
        }

    function get_sub_country($id)
        {


        }

    function get_parish($id)
        {

        }

    function get_village($id)
        {

        }


    function get_location_name($id = null)
        {

            if (isset($id)) {
                $result = $this->ci->db->select('name')->from('locations')->where(array('id' => $id))->get()->row();

                if (count($result) == 1) {

                    return $result->name;


                }
                else {

                    return false;
                }

            }
            else {
                return false;
            }
        }

    function get_villages_under_selection($id)
        {

            /*Array (

            [0] => Array ( [id] => 3 [name] => Eastern )
            [1] => Array ( [id] => 64 [name] => Mbale )
            [2] => Array ( [id] => 956 [name] => Busiu )
            [3] => Array ( [id] => 6105 [name] => Buwalasi )
            [4] => Array ( [id] => 27141 [name] => Namunyu )

            )*/


            $path = $this->get_path($id);

            $path_no = count($path);

            $villages = array();
            switch ($path_no) {

                case 5://return this Village
                    array_push($villages, $id);
                    break;

                case 4://Villages
                    foreach ($this->get_children($id) as $v) {
                        array_push($villages, $v->id);
                    }
                    break;

                case 3://Parish
                    foreach ($this->get_children($id) as $p) {
                        foreach ($this->get_children($p->id) as $v) {
                            array_push($villages, $v->id);
                        }
                    }
                    break;

                case 2://sub county
                    foreach ($this->get_children($id) as $p) {
                        foreach ($this->get_children($p->id) as $v) {

                            foreach ($this->get_children($v->id) as $k) {
                                array_push($villages, $k->id);
                            }
                        }
                    }
                    break;

                case 1://District
                    foreach ($this->get_children($id) as $p) {
                        foreach ($this->get_children($p->id) as $v) {

                            foreach ($this->get_children($v->id) as $k) {

                                foreach ($this->get_children($k->id) as $p) {
                                    array_push($villages, $p->id);
                                }
                            }
                        }
                    }
                    break;
                case 0://Region
                    foreach ($this->get_children($id) as $p) {
                        foreach ($this->get_children($p->id) as $v) {

                            foreach ($this->get_children($v->id) as $k) {

                                foreach ($this->get_children($k->id) as $p) {
                                    foreach ($this->get_children($p->id) as $t) {
                                        array_push($villages, $t->id);
                                    }
                                }
                            }
                        }
                    }
                    break;
            }

            return $villages;

        }

    function get_path($id)
        {


            $result = $this->ci->db->select('id,tree_parent_id,lft, rght,type_id,name')->from('locations')->where(array('id' => $id))->get()->row();


            $path = array();

            if (count($result) == 1) {
                if ($result->tree_parent_id != '') {

                    $pat = array(array('id' => $result->id, 'name' => $result->name));

                    $path = array_merge($this->get_path($result->tree_parent_id), $pat);

                }
            }

            return $path;

        }

    function get_children($id = null)
        {

            //SELECT lft, rght, level  INTO our_lft, our_rght, lvl FROM locations_view WHERE id = loc_id;
            //FOR r IN SELECT * FROM locations_view WHERE lft > our_lft AND rght < our_rght AND level = lvl + 1
            if (isset($id)) {
                $result = $this->ci->db->select('lft, rght,level')->from('locations_view')->where(array('id' => $id))->get()->row();

                if (count($result) == 1) {

                    $our_lft = $result->lft;
                    $our_rght = $result->rght;
                    $level = $result->level;

                    $result = $this->ci->db->select()->from('locations_view')->where(array('lft >' => $our_lft, 'rght <' => $our_rght, 'level' => $level + 1))->get()->result();


                    return $result;


                }
                else {

                    return false;
                }
            }
            else {
                return false;
            }


        }

        function get_location_type($id=null){
            if(isset($id)){
                $loc=$this->ci->db->select('b.name')->from('locations a')->join('locationtype b','a.type_id=b.id')->where('a.id',$id)->get()->row();
                return $loc->name;
            }else{
                return false;
            }

        }

}

?>
