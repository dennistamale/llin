<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<?php

class custom_library{
    private $CI;
    private $product_id = 2;

    function __construct() {
        $this->CI =& get_instance();
        $this->CI->load->database();
    }


    function dennis(){
        //$this->CI
        $users=$this->CI->db->select()->from('users')->get()->result();
        return($users);


    }

    function get_user_full_name($id){

        $user=$this->CI->db->select('first_name,last_name')->from('users')->where('id',$id)->get()->row();
       if(count($user)==1){
           return $user->first_name.' '.$user->last_name;
       }else{
           return false;
       }


    }


    function send_sms($phone, $sms)
        {
            if (!empty($phone)) {
                if (strstr($phone, ',') or strstr($phone, '/') or strstr($phone, '-')) {
                    if (strstr($phone, ',')) {
                        $phone = substr($phone, 0, strpos($phone, ','));
                    }
                    if (strstr($phone, '/')) {
                        $phone = substr($phone, 0, strpos($phone, '/'));
                    }
                    if (strstr($phone, '-')) {
                        $phone = substr($phone, 0, strpos($phone, '-'));
                    }
                    $phone = trim($phone);
                }
                if (strlen($phone) >= 9) {
                    if (substr($phone, 0, 1) == '0') {
                        if (strlen($phone) == 10) {
                            $phone = '256' . substr($phone, 1);
                        }
                    }
                    if (strlen($phone) == 9 and substr($phone, 0, 1) !== 0) {
                        $phone = '256' . $phone;
                    }
                    if (substr($phone, 0, 1) == '+') {
                        $phone = substr($phone, 1);
                    }
                    $q = $this->CI->db->select('id')->from('outbox')->where(array('to_user' => $phone, 'message' => $sms))->limit(1)->get()->result();
                    if (!isset($q[0]->id)) {
                        $request = "";
                        $request .= urlencode("Mocean-Username") . "=" . urlencode("mixa") . "&";
                        $request .= urlencode("Mocean-Password") . "=" . urlencode("0n6cAd") . "&";
                        $request .= urlencode("Mocean-From") . "=" . urlencode("Mixakids") . "&";
                        $request .= urlencode("Mocean-To") . "=" . urlencode($phone) . "&";
                        $request .= urlencode("Mocean-Url-Text") . "=" . urlencode($sms);
                        // Build the header
                        $host = "sms.smsone.co.ug";
                        $script = "/cgi-bin/sendsms";
                        $request_length = 0;
                        $method = "POST";
                        //Now construct the headers.
                        $header = "$method $script HTTP/1.1\r\n";
                        $header .= "Host: $host\r\n";
                        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                        $header .= "Content-length: " . strlen($request) . "\r\n";
                        $header .= "Connection: close\r\n\r\n";
                        // Open the connection
                        $port = 8866;
                        $socket = @fsockopen($host, $port, $errno, $errstr);
                        if ($socket) {
                            // Send HTTP request
                            fputs($socket, $header . $request);
                            // Get the response
                            while (!feof($socket)) {
                                $output[] = fgets($socket); //get the results
                            }
                            fclose($socket);
                            //$this->CI->db->insert('outbox','to_user,subject,m_type,message,created_on,created_by',"'$phone','Notification','SMS','$sms','".time()."','0'");
                            $this->CI->db->insert('outbox', array('to_user' => $phone, 'subject' => 'Mixakids', 'm_type' => 'SMS', 'message' => $sms, 'created_on' => time(), 'created_by' => '0'));
                        }
                    }
                }
            }
        }

    public function stringNumbers($x)
        {
            $nwords = array("zero", "one", "two", "three", "four", "five", "six", "seven",
                "eight", "nine", "ten", "eleven", "twelve", "thirteen",
                "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
                "nineteen", "twenty", 30 => "thirty", 40 => "forty",
                50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty",
                90 => "ninety");


            if (!is_numeric($x))
                $w = '#';
            else if (fmod($x, 1) != 0)
                $w = '#';
            else {
                if ($x < 0) {
                    $w = 'minus ';
                    $x = -$x;
                }
                else
                    $w = '';
                // ... now $x is a non-negative integer.

                if ($x < 21)   // 0 to 20
                    $w .= $nwords[$x];
                else if ($x < 100) {   // 21 to 99
                    $w .= $nwords[10 * floor($x / 10)];
                    $r = fmod($x, 10);
                    if ($r > 0)
                        $w .= ' ' . $nwords[$r];
                }
                else if ($x < 1000) {   // 100 to 999
                    $w .= $nwords[floor($x / 100)] . ' hundred';
                    $r = fmod($x, 100);
                    if ($r > 0) {
                        $w .= ' and ';
                        if ($r < 100) {   // 21 to 99
                            $w .= $nwords[10 * floor($r / 10)];
                            $r = fmod($r, 10);
                            if ($r > 0)
                                $w .= ' ' . $nwords[$r];
                        }
                    }
                }
                else if ($x < 1000000) {   // 1000 to 999999
                    $w .= $nwords[floor($x / 1000)] . ' thousand';
                    $r = fmod($x, 1000);
                    if ($r > 0) {
                        $w .= ' ';
                        if ($r < 100)
                            $w .= ' and ';
                        $w .= $nwords[$r];
                    }
                }
                else {    //  millions
                    $w .= stringNumbers(floor($x / 1000000)) . ' million';
                    $r = fmod($x, 1000000);
                    if ($r > 0) {
                        $w .= ' ';
                        if ($r < 100)
                            $w .= ' and ';
                        $w .= int_to_words($r);
                    }
                }
            }
            //echo $w;

            return $w;


        }




    function get_enum_values($table, $field)
        {
            $type = $this->CI->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
            preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
            $enum = explode("','", $matches[1]);
            return $enum;
        }

    function site_visits($ip=null){


       $ip=isset($ip)?$ip:$_SERVER["REMOTE_ADDR"];

       $cp=$this->CI->db->select('statusCode,ipAddress,countryCode,countryName,regionName,cityName,zipCode,latitude,longitude,timeZone')->from('site_visit')->where('ipAddress',$ip)->get()->row();
        if(count($cp)==1){

           // statusCode,ipAddress,countryCode,countryName,regionName,cityName,zipCode,latitude,longitude,timeZone
//            $xml=array(
//                'statusCode'=>$cp->statusCode,
//                'ipAddress'=>$cp->ipAddress,
//                'countryCode'=>$cp->countryCode,
//                'countryName'=>$cp->countryName,
//                'regionName'=>$cp->regionName,
//                'cityName'=>$cp->cityName,
//                'zipCode'=>$cp->zipCode,
//                'latitude'=>$cp->latitude,
//                'longitude'=>$cp->longitude,
//                'timeZone'=>$cp->timeZone
//            );

            $xml=$cp;
        }else{
             $xml=simplexml_load_file("http://api.ipinfodb.com/v3/ip-city/?key=1df5a82c001338901a85b18f75576ce3874c14c23b5c547f3e208e63883413bf&ip=$ip&format=xml");
        }

        $this->CI->db->insert('site_visit',$xml);

     // return $xml;
    }



    function sendHTMLEmail2($to, $subject, $message)
        {


            $mailto = $to;
            // $file="thanks.htm";
            $pcount = 0;
            $gcount = 0;
            $subject = $subject;
            $b = time();
            $pstr = $message;//$this->email_template($message);
            $gstr = $message;//$this->email_template($message);
            $from = "noreply@deltaits.net";


            $headers = sprintf("From: GEF<noreply@opm.co.ug>\r\n");
            $headers .= sprintf("MIME-Version: 1.0\r\n");
            $headers .= sprintf("Content-type: text/html; charset=utf-8\r\n");

            while (list($key, $val) = each($_POST)) {
                $pstr = $pstr . "$key : $val \n ";
                ++$pcount;

            }
            while (list($key, $val) = each($_GET)) {
                $gstr = $gstr . "$key : $val \n ";
                ++$gcount;

            }
            if ($pcount > $gcount) {
                $message_body = $message;//$pstr;
                // $message_body = $pstr;
                mail($mailto, $subject, $message_body, "From:" . $from);
                //$send = @mail($to, $subject, $body, $headers);

                $this->CI->db->insert('outbox', array('to_user' => $mailto, 'subject' => $subject, 'm_type' => 'Email', 'message' => $message_body, 'created_on' => time(),));
                //  include("$file");
                return true;
            }
            else {
                $message_body = $gstr;

                if (!mail($mailto, $subject, $message_body, "From:" . $from)) {
//                die ("Not sent");
                    return false;
                }
                else {
                    // include("$file");
                    // print $b;
                    return true;
                }
            }

        }


    function email_template($body)
        {
            $logo = 'http://citiexpress.net/Ariane/wp-content/uploads/2016/05/Coat_of_arms_of_the_Republic_of_Uganda.svg.png';
            $html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
				<html style="-webkit-text-size-adjust: none;-ms-text-size-adjust: none;">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>Government Evaluation Facility | Email</title>
				<style type="text/css">
				html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
				@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
					*[class="table_width_100"] {
						width: 96% !important;
					}
					*[class="border-right_mob"] {
						border-right: 1px solid #dddddd;
					}
					*[class="mob_100"] {
						width: 100% !important;
					}
					*[class="mob_center"] {
						text-align: center !important;
						padding: 0 !important;
					}
					*[class="mob_center_bl"] {
						float: none !important;
						display: block !important;
						margin: 0px auto;
					}
					.iage_footer a {
						text-decoration: none;
						color: #929ca8;
					}
					img.mob_display_none {
						width: 0px !important;
						height: 0px !important;
						display: none !important;
					}
					img.mob_width_50 {
						width: 40% !important;
						height: auto !important;
					}
					img.mob_width_80 {
						width: 80% !important;
						height: auto !important;
					}
					img.mob_width_80_center {
						width: 80% !important;
						height: auto !important;
						margin: 0px auto;
					}
					.img_margin_bottom {
						font-size: 0;
						height: 25px;
						line-height: 25px;
					}
				}
				.table_width_100 {
					width: 680px;
				}
				</style>
				</head>

				<body style="padding: 0px; margin: 0px;">
				<div id="mailsub" class="notification" align="center">

				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">


				<!--[if gte mso 10]>
				<table width="680" border="0" cellspacing="0" cellpadding="0">
				<tr><td>
				<![endif]-->

				<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px;min-width: 300px;width: 680px;">
					<tr><td>
					<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--header -->
					<tr><td align="center" bgcolor="#ffffff">
						<!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;">&nbsp;</div>
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left"><!--

								Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
									<table class="mob_center" width="180" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
										<tr><td align="left" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="180" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="left" valign="top" class="mob_center">
													<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
													<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
													<img src="' . $logo . '" alt="MixaKids" border="0" style="display: block;height:100px"></font></a>
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--><!--[if gte mso 10]>
									</td>
									<td align="right">
								<![endif]--><!--

								Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;">
									<table width="88" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
										<tr><td align="right" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="right">
													<!--social -->
													<div class="mob_center_bl" style="width: 88px;">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr><td width="30" align="center" style="line-height: 19px;">
															<a href="https://www.facebook.com/opmuganda/" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/facebook.gif" width="10" height="19" alt="Follow us on Facebook" border="0" style="display: block;"></font></a>
														</td><td width="39" align="center" style="line-height: 19px;">
															<a href="https://www.twitter.com/mixakids" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/twitter.gif" width="19" height="16" alt="Follow us on Twitter" border="0" style="display: block;"></font></a>
														</td><!--<td width="29" align="right" style="line-height: 19px;">
															<a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#596167">
															<img src="http://artloglab.com/metromail3/images/dribbble.gif" width="19" height="19" alt="Dribbble" border="0" style="display: block;"></font></a>
														</td>--></tr>
													</table>
													</div>
													<!--social END-->
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--></td>
							</tr>
						</table>
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--header END-->

					<!--content 1 -->
					<tr><td align="center" bgcolor="#f8f8f8">
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left">
								<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
								<font face="Arial, Helvetica, sans-serif" size="4" color="#333" style="font-size: 15px;">
									<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #333;">
										' . $body . '
									</span>
								</font>
								<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
							</td></tr>
						</table>
					</td></tr>
					<!--content 1 END-->
					<tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left">
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
						<div style="line-height: 22px;">
							<font face="Arial, Helvetica, sans-serif" size="5" color="#6b6b6b" style="font-size: 20px;">
								<span style="font-family: Arial, Helvetica, sans-serif; font-size: 20px; color: #6b6b6b;">
									Thank you for Subscribing with <strong>Governmnent evaluation Facility</strong>
								</span>
							</font>
						</div>
					<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
							</td></tr>
						</table>
					</td></tr>

					<!--footer -->
					<tr><td align="center" bgcolor="#ffffff">
						<!-- padding --><div style="height: 15px; line-height: 15px; font-size: 10px;">&nbsp;</div>
						<table width="90%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="left"><!--

								Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
									<table class="mob_center" width="180" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
										<tr><td align="left" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="115" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="left" valign="top" class="mob_center">
													<a href="http://www.opm.go.ug" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
													<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
													<img src="src="' . $logo . '"" alt="Government Evaluation Facility" border="0" style="display: block;width:100%" /><br>
													Support Team</font></a>
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--><!--[if gte mso 10]>
									</td>
									<td align="right">
								<![endif]--><!--

								Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 150px;">
									<table class="mob_center" width="150" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
										<tr><td align="right" valign="middle">
											<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;">&nbsp;</div>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr><td align="right">
													<!--social -->
													<div class="mob_center_bl">
													<table border="0" cellspacing="0" cellpadding="0">
														<tr><td align="center" style="line-height: 19px; padding-right: 20px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															FAQ</font></a>
														</td><td align="center" style="line-height: 19px; padding-right: 20px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															Blog</font></a>
														</td><td align="right" style="line-height: 19px;">
															<a href="http://opm.go.ug/" target="_blank" style="color: #9c9c9c; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-decoration: none;">
															<font face="Arial, Helvetica, sans-serif" size="2" color="#9c9c9c">
															Contact</font></a>
														</td></tr>
													</table>
													</div>
													<!--social END-->
												</td></tr>
											</table>
										</td></tr>
									</table></div><!-- Item END--></td>
							</tr>
						</table>
						<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
					</td></tr>
					<!--footer END-->
					<tr>
						<td>
							<p>&nbsp;</p>
							<p style="background:#e9e9e9;color:#333"><small>This email was sent automatically by <a href="http://opm.go.ug/">opm.go.ug/</a>. Please, do not reply</small></p>
						</td>
					</tr>
					<tr><td>
					<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
					</td></tr>
				</table>
				<!--[if gte mso 10]>
				</td></tr>
				</table>
				<![endif]-->

				</td></tr>
				</table>

				</div>
				</body>
		</html>';
            return $html;
        }


    function sendHTMLEmail($to, $subject, $message)
        {
            $headers = sprintf("From: Government Evaluation Facility<noreply@opm.go.ug>\r\n");
            $headers .= sprintf("MIME-Version: 1.0\r\n");
            $headers .= "Reply-To: " . $to . "\r\n";
            $headers .= "Bcc: tamaledns@gmail.com\r\n";
            $headers .= sprintf("Content-type: text/html; charset=utf-8\r\n");
            $send = @mail($to, $subject, $message, $headers);
            if ($send) {
                return true;
            }
            else {
                return false;
            }
        }




    function sendMail($to, $subject, $message,$attachment=null,$cc=null,$bcc=null){

        //$this->CI->load->library('email');
        //$this->CI->load->helper('email');

        $config['protocol'] = 'smtp';
        //$config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['useragent'] = $this->CI->site_options->title('site_name');
        $config['smtp_host'] = 'www.wealthmoneytransfer.com';
        $config['smtp_user'] = 'no-reply@wealthmoneytransfer.com';
        $config['smtp_pass'] = 'svuN82^6';
        $this->CI->email->initialize($config);

        if(valid_email($to)) {

            $this->CI->email->clear(TRUE);
            $this->CI->email->from('no-reply@wealthmoneytransfer.com',  $this->CI->site_options->title('site_name'));
            $this->CI->email->to($to);
            isset($cc) && valid_email($cc)?$this->CI->email->cc($cc):'';
            isset($bcc) && valid_email($bcc)?$this->CI->email->bcc($bcc):'';

            $this->CI->email->subject($subject);
//            $this->email->message($message);
            $data['message']=$message;
            isset($attachment)?$this->CI->email->attach($attachment):'';
            $this->CI->email->message($this->CI->load->view('email_templates/email', $data, true));
            $this->CI->email->set_mailtype('html');




            if($this->CI->email->send())
            {
                $this->CI->db->insert('outbox', array('to_user' => $to, 'subject' => $subject, 'm_type' => 'email', 'message' => $message, 'created_on' => time(),));

                echo 'message sent';
                // return true;
            }else{
                echo 'message failed';
//                false;
            }

        }else{
            echo 'Message Failed firsttime';
            //return false;
        }
    }




    function role_exist($value,$type='title'){


        if(isset($this->CI->session->permission)) {

            $array = $this->CI->session->permission;

            $key = array_search(strtolower($value), array_column($array, $type));

//             var_dump($key);



            return strlen($key)==0 ? false : true;
        }else{
            return false;
        }


    }


    function any_role()
        {
           return count($this->CI->session->permission) > 0 ? true : false;


        }




    function house_holds($form_num=null)
        {

            $this->CI->db->select()->from('registration');
            isset($form_num)?$this->CI->db->where(array('form_num' => $form_num)):'';
                $forms = $this->CI->db->where(array('confirm'=>1))
                ->order_by('id', 'desc')->get()->result();

            if (count($forms) > 0) {
                return $forms;

            }
            else {
                return false;
            }

        }




    function people_in_household($data_id)
        {
            $people = $this->CI->db->select()->from('reg_detail')->where(array('data_id' => $data_id))->get()->result();

            if (count($people) > 0) {
                return $people;

            }
            else {
                return false;
            }
        }


    function entry_variation($form_num)
        {


            $entries = $this->CI->db->where(array('form_num' => $form_num,'entry_times !='=>3))->from('registration')->count_all_results();




            $variance = 0;
            $resolved ='';
            $entrant = '';
            $validation = '';
            $records = $entries;
            if ($entries == 2) {


                $entered_forms = $this->CI->db->select('a.*,b.form_num')
                    ->from('reg_detail a')
                    ->join('registration b', 'a.data_id=b.id')
                    ->where(array('b.form_num' => $form_num,'b.entry_times'=>1))->get()->result();


                $form_row=0;
                foreach ($entered_forms as $f1) {

                    $f2 = $this->CI->db->select('a.*,b.form_num')
                        ->from('reg_detail a')
                        ->join('registration b', 'a.data_id=b.id')
                        ->where(array('b.form_num' => $form_num,'b.entry_times'=>2,'row_no'=>$form_row))->get()->row();
                    $form_row++;

                    trim(strtolower($f1->last_name))!= trim(strtolower($f2->last_name))?$variance++:'';
                    trim(strtolower($f1->first_name))!= trim(strtolower($f2->first_name))?$variance++:'';
                    trim(strtolower($f1->tel))!= trim(strtolower($f2->tel))?$variance++:'';
                    trim(strtolower($f1->national_id))!= trim(strtolower($f2->national_id))?$variance++:'';
                    trim(strtolower($f1->person_vht))!= trim(strtolower($f2->person_vht))?$variance++:'';
                    trim(strtolower($f1->person_sc))!= trim(strtolower($f2->person_sc))?$variance++:'';
                    trim(strtolower($f1->person_final))!= trim(strtolower($f2->person_final))?$variance++:'';


                    $entrant = $this->get_user_full_name($f1->created_by);
                    $validation = 'Parish Chief';
                }

            }
            elseif ($entries == 3) {

                $variance = 0;
                $resolved = 'Resolved';
            }
            elseif ($entries == 1) {

                $variance = 'N/A';
            }
            elseif ($entries == 0) {
                $variance = 'N/A';
            }
            else {
                $variance = 'error getting variations';
            }





                    $output = array(

                'records' => $records,
                'resolved'=>$resolved,
                'variance' => $variance,
                'entrant' => $entrant,
                'validation' => $validation
            );

            return $output;


        }

    function entry_variations($form_num)
        {


            $entries = $this->CI->db->select()->where(array('form_num' => $form_num))->from('registration')->get()->result();



            $variance = 0;
            $resolved ='';
            $entrant = '';
            $validation = '';
            $records = count($entries);
            $form_worked_on=0;
            $form_resolved=0;
            $households=0;
            $households_resolved=0;


            if(count($entries)>1){

               // if($entries[0]->confirm==1){

                   if(count($entries)>=2){



                       $entered_forms = $this->CI->db->select('a.*,b.form_num')
                           ->from('reg_detail a')
                           ->join('registration b', 'a.data_id=b.id')
                           ->where(array('b.form_num' => $form_num,'b.entry_times'=>1))->get()->result();



                       foreach ($entered_forms as $f1) {



                           $f2 = $this->CI->db->select('a.*,b.form_num')
                               ->from('reg_detail a')
                               ->join('registration b', 'a.data_id=b.id')
                               ->where(array('b.form_num' => $form_num,'b.entry_times'=>2,'row_no'=>$f1->row_no))->get()->row();


                           if(count($f2)==1) {

                               $form_worked_on++;

//                           print_r($f1);
//                           echo '<hr/>';
                               //print_r($f2);

                               $v1 = trim(strtolower($f1->first_name)) != trim(strtolower($f2->first_name)) ? 1 : 0;
                               $v1 = trim(strtolower($f1->last_name)) != trim(strtolower($f2->last_name)) ? 1 : 0;
                               $v2 = trim(strtolower($f1->tel)) != trim(strtolower($f2->tel)) ? 1 : 0;
                               $v3 = trim(strtolower($f1->national_id)) != trim(strtolower($f2->national_id)) ? 1 : 0;
                               $v4 = trim(strtolower($f1->chalk_id)) != trim(strtolower($f2->chalk_id)) ? 1 : 0;
                               $v5 = trim(strtolower($f1->person_vht)) != trim(strtolower($f2->person_vht)) ? 1 : 0;
                               $v6 = trim(strtolower($f1->person_sc)) != trim(strtolower($f2->person_sc)) ? 1 : 0;
                               $v7 = trim(strtolower($f1->person_final)) != trim(strtolower($f2->person_final)) ? 1 : 0;


                               $var = $v1 + $v2 + $v3 + $v4 + $v5 + $v6 + $v7;

                               $variance = $variance + $var;

                               $variance>0?$households++:'';



                               $validation = $this->get_user_full_name($f2->created_by);
                               //$validation = 'Parish Chief';
                           }

                       }



                       if(count($entries)==3){


                           $f2 = $this->CI->db->select('b.created_by')->from('registration b')
                               ->where(array('b.form_num' => $form_num,'b.entry_times'=>3))->get()->row();

                           $entrant = isset($f2->created_by)?$this->get_user_full_name($f2->created_by):'N/A';
                           $resolved= ($entries[2]->confirm==1)?'Resolved':'';

                           $form_resolved++;

                       }elseif(count($entries)==2){
                          // $resolved= ($entries[1]->confirm==1)?'Resolved':'';
                       }



                   //}


                }


            }




            $output = array(
                'households' => $households,
                'forms_worked_on' => $form_worked_on,
                'forms_resolved'=>$form_resolved,
                'records' => $records,
                'resolved'=>$resolved,
                'variance' => $variance,
                'entrant' => $entrant,
                'validation' => $validation
            );

            return $output;
        }



    function collection_variations($form_num)
        {


            $form_entry = $this->CI->db->select('a.lc1,b.person_vht,b.person_sc,b.person_final,a.form_num,a.created_by')->from('registration a')->join('reg_detail b', 'a.id=b.data_id')->where(array('a.form_num' => $form_num, 'a.confirm' => 1))->get()->result();

            $variance = 0;
            $resolved = 0;
            $entrant = count($form_entry) > 0 ? $this->get_user_full_name($form_entry[0]->created_by) : '';
            $validation = '';



            foreach ($form_entry as $fe) {

                strcasecmp($fe->person_vht, $fe->person_sc) != 0 ? $variance++ : '';
                //checking if the record has been resolved
                $validation= $this->get_user_full_name($fe->created_by);
                if (strcasecmp($fe->person_vht, $fe->person_sc) != 0) {
                    $fe->person_final != 0 ? $resolved++ : '';
                }
            }


            $output = array(

                'resolved' => $resolved,
                'records' => count($form_entry),
                'variance' => $variance,
                'entrant' => $entrant,
                'validation' => $validation
            );

            return $output;


        }





    function getting_sub_county_in_district($district)
        {
            $this->CI->db->select('b.sub_county')
                ->from('registration a')->join('villages b', 'a.village=b.village')->where(array('a.confirm' => 1, 'a.district' => $district));
            $this->CI->db->group_by('b.sub_county');
            $sub_county = $this->CI->db->order_by('a.id', 'desc')->get()->result();


            $sub_county_no=count($sub_county);
            $parish_no=0;
            $village_no=0;
            $house_hold = 0;
            $population = 0;
            $house_hold_nets = 0;
            $bails = 0;
            $extra_bails = 0;
            $picked=0;


            foreach ($sub_county as $q) {

               $np=$this->getting_parish_in_sub_county($q->sub_county);

                $parish_no =$parish_no + $np['parish_no'];
                $village_no =$village_no + $np['village_no'];
                $house_hold =$house_hold + $np['house_holds'];
                $population =$population + $np['population'];
                $house_hold_nets =$house_hold_nets + $np['house_hold_nets'];
                $bails = $bails + $np['bails'];
                $extra_bails = $extra_bails+ $np['extra_bails'];
                $picked=$picked+$np['picked'];
            }

            return array(
                'sub_county_no' =>  $sub_county_no,
                'parish_no' =>  $parish_no,
                'village_no' =>  $village_no,
                'house_holds' => $house_hold,
                'population' => $population,
                'house_hold_nets' => $house_hold_nets,
                'bails' => $bails,
                'extra_bails' => $extra_bails,
                'picked'=>$picked

            );
        }





    function getting_parish_in_sub_county($sub_county)
        {
            $this->CI->db->select('b.parish')
                ->from('registration a')->join('villages b', 'a.village=b.village')->where(array('confirm' => 1, 'b.sub_county' => $sub_county));
            $this->CI->db->group_by('b.parish');
            $parish = $this->CI->db->order_by('a.id', 'desc')->get()->result();


            $parish_no=count($parish);
            $village_no=0;
            $house_hold = 0;
            $population = 0;
            $house_hold_nets = 0;
            $bails = 0;
            $extra_bails = 0;
            $picked=0;


            foreach ($parish as $q) {

               $np=$this->getting_villages_in_parish($q->parish);

                $village_no =$village_no + $np['village_no'];
                $house_hold =$house_hold + $np['house_holds'];
                $population =$population + $np['population'];
                $house_hold_nets =$house_hold_nets + $np['house_hold_nets'];
                $bails = $bails + $np['bails'];
                $extra_bails = $extra_bails+ $np['extra_bails'];
                $picked=$picked+$np['picked'];



            }

            return array(
                'parish_no' =>  $parish_no,
                'village_no' =>  $village_no,
                'house_holds' => $house_hold,
                'population' => $population,
                'house_hold_nets' => $house_hold_nets,
                'bails' => $bails,
                'extra_bails' => $extra_bails,
                'picked'=>$picked

            );
        }



    function getting_villages_in_parish($parish)
        {
            $this->CI->db->select('distinct(a.village) as village')
                ->from('registration a')->join('villages b', 'a.village=b.village')->where(array('confirm' => 1, 'b.parish' => $parish));
            $villages = $this->CI->db->order_by('a.id', 'desc')->get()->result();



            $village_no=count($villages);
            $house_hold=0;
            $population=0;
            $house_hold_nets=0;
            $bails=0;
            $extra_bails=0;
            $picked=0;


            foreach ($villages as $q) {

                $for_det = $this->CI->db->select('b.person_vht,b.person_final,b.no_picked')
                    ->from('registration a')
                    ->join('reg_detail b', 'b.data_id=a.id')
                    ->where(array('a.village' => $q->village, 'a.confirm' => 1))
                    ->get()->result();

                $np = nets_popn_agg($for_det);
                $house_hold=$house_hold+count($for_det);
                $population=$population+$np['population'];
                $house_hold_nets=$house_hold_nets+$np['house_hold_nets'];
                $bails=$bails+$np['bails'];
                $extra_bails=$extra_bails+$np['extra_bails'];
                $picked=$picked+$np['picked'];
            }

            return array(
                'village_no'=>$village_no,
                'house_holds'=>$house_hold,
                'population'=>$population,
                'house_hold_nets'=>$house_hold_nets,
                'bails'=>$bails,
                'extra_bails'=>$extra_bails,
                'picked'=>$picked

            );
        }




    function getting_distribution_points($id)
        {
            $this->CI->db->select('a.village')
                ->from('registration a')->join('distribution_point_villages b', 'a.village=b.village')->where(array('confirm' => 1, 'b.distribution_point' => $id));
            $villages = $this->CI->db->group_by('b.village')->order_by('a.id', 'desc')->get()->result();



            $village_no=count($villages);
            $house_hold=0;
            $population=0;
            $house_hold_nets=0;
            $bails=0;
            $extra_bails=0;
            $picked=0;


            foreach ($villages as $q) {

                $for_det = $this->CI->db->select('b.person_vht,b.person_final,b.no_picked')
                    ->from('registration a')
                    ->join('reg_detail b', 'b.data_id=a.id')
                    ->where(array('a.village' => $q->village, 'a.confirm' => 1))
                    ->get()->result();

                $np = nets_popn_agg($for_det);
                $house_hold=$house_hold+count($for_det);
                $population=$population+$np['population'];
                $house_hold_nets=$house_hold_nets+$np['house_hold_nets'];
                $bails=$bails+$np['bails'];
                $extra_bails=$extra_bails+$np['extra_bails'];
                $picked=$picked+$np['picked'];
            }

            return array(
                'village_no'=>$village_no,
                'house_holds'=>$house_hold,
                'population'=>$population,
                'house_hold_nets'=>$house_hold_nets,
                'bails'=>$bails,
                'extra_bails'=>$extra_bails,
                'picked'=>$picked

            );
        }




}


?>
