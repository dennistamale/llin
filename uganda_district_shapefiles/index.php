<?php


// Register autoloader
require_once('../php-shapefile/src/ShapeFileAutoloader.php');
\ShapeFile\ShapeFileAutoloader::register();

// Import classes
use \ShapeFile\ShapeFile;
use \ShapeFile\ShapeFileException;

echo "<pre>";
try {
    // Open shapefile
//    $ShapeFile = new ShapeFile('../UGA_adm_shp/UGA_adm4.shp');
    $ShapeFile = new ShapeFile('Uganda_admin_2014_WGS84.shp');


    // Get Shape Type
    echo "Shape Type : ";
    echo $ShapeFile->getShapeType()." - ".$ShapeFile->getShapeType(ShapeFile::FORMAT_STR);
    echo "\n\n";

    // Get number of Records
    echo "Records : ";
    echo $ShapeFile->getTotRecords();
    echo "\n\n";

    // Get Bounding Box
    echo "Bounding Box : ";
    print_r($ShapeFile->getBoundingBox());
    echo "\n\n";

    // Get DBF Fields
    echo "DBF Fields : ";
    print_r($ShapeFile->getDBFFields());
    echo "\n\n";

} catch (ShapeFileException $e) {
    // Print detailed error information
    exit('Error '.$e->getCode().' ('.$e->getErrorType().'): '.$e->getMessage());
}
echo "</pre>";

?>

<hr/>
<?php

echo "<pre>";
try {
    // Open shapefile
//    $ShapeFile = new ShapeFile('../UGA_adm_shp/UGA_adm4.shp');
    $ShapeFile = new ShapeFile('Uganda_admin_2014_WGS84.shp');


    // Read all the records using a foreach loop
    foreach ($ShapeFile as $i => $record) {
        if ($record['dbf']['_deleted']) continue;
        // Record number
        echo "Record number: $i\n";
        // Geometry
        print_r($record['shp']);
        // DBF Data
        print_r($record['dbf']);
    }

} catch (ShapeFileException $e) {
    // Print detailed error information
    exit('Error '.$e->getCode().' ('.$e->getErrorType().'): '.$e->getMessage());
}
echo "</pre>";

?>

<?php

//try {
//    // Open shapefile
//    $ShapeFile = new ShapeFile('UGANDA_DISTRICTS_116-wgs84.shp');
//
//    // Read all the records
//    while ($record = $ShapeFile->getRecord(ShapeFile::GEOMETRY_BOTH)) {
//        if ($record['dbf']['_deleted']) continue;
//        // Geometry
//        print_r($record['shp']);
//        // DBF Data
//        print_r($record['dbf']);
//    }
//
//} catch (ShapeFileException $e) {
//    // Print detailed error information
//    exit('Error '.$e->getCode().' ('.$e->getErrorType().'): '.$e->getMessage());
//}

?>
