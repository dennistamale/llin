



<html>
<head>
    <title>A Leaflet map!</title>
    <link rel="stylesheet" href="lib/leaflet/leaflet.css"/>
    <link rel="stylesheet" href="lib/leaflet/leaflet.label.css"/>
    <script src="lib/leaflet/leaflet.js"></script>
    <script src="lib/leaflet/leaflet.label.js"></script>
    <script src="data/subcounties.geojson"></script>
    <!--    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>-->
    <style>
        #map {
            height: 100%
        }
    </style>
</head>
<body>

<div id="map"></div>

<script>
    var statesLayer;

    function highlightFeature(e) {
        var layer = e.target;
        layer.setStyle(
            {
                weight: 5,
                color: 'black',
                fillColor: 'white',
                fillOpacity: '0.2'
            }
        );
        if (!L.Browser.ie && !L.Browser.opera) {
            layer.bringToFront();


        }

    }

    function resetHighlight(e) {
        statesLayer.resetStyle(e.target);
    }

    function zoomToFeature(e) {

        map.fitBounds(e.target.getBounds());

    }


    function statesOnEachFeature(feature, layer) {

        //  layer.bindLabel('A sweet static label!', { noHide: true });

        layer.bindLabel(feature.properties.Adm_Region.toString(), {noHide: true});
        layer.on(
            {
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: zoomToFeature
            }
        );
    }

    function getStateColor(region) {

        if (region == 'Northern') {
            return 'green';
        } else if (region == 'Central') {

            return 'blue';
        } else if (region == 'Western') {

            return 'red';
        } else if (region == 'Eastern') {

            return 'purple';
        } else {
            return 'black';
        }

    }

    function stateStyle(feature) {
        return {
            fillColor: getStateColor(feature.properties.Adm_Region),
            weight: 1,
            opacity: 1,
            color: 'white',
            dashArray: 3,
            fillOpacity: 0.9
        }
    }

    // initialize the map
    var map = L.map('map').setView([0.9, 32.1], 7);
    // load GeoJSON from an external file
    statesLayer = L.geoJson(
        states,
        {
            style: stateStyle,
            onEachFeature: statesOnEachFeature
        }
    ).addTo(map);

    map.fitBounds(statesLayer.getBounds());

    var legend = L.control({position: 'bottomright'});


</script>
</body>
</html>
